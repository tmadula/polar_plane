#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage
import os

from rw_utils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--limit-input", dest="limit_input", default="",
                  help="Limit input file")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")
parser.add_option("-o", "--output-dir", dest="output_dir", default="",
                   help="Output directory")
parser.add_option("--NNT_tag", dest="NNT_tag", default="",
                  help="NNT tag")

(options, args) = parser.parse_args()

input_file = options.limit_input
label = options.label
NNT_tag = options.NNT_tag
output_dir = options.output_dir

if input_file.find('HCrescale') != -1:
    label+="_HCrescale"

if label and label[0] != '_':
    label = '_'+label

from rootpy.plotting import *
from rootpy.io import root_open
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)

methods = []
if input_file.find('BDT') != -1:
    methods.append('BDT')
if input_file.find('NN') != -1:
    methods.append('NN')
if input_file.find('spline') != -1:
    methods.append('spline')

if input_file.find('16') != -1:
    year = "2016"
    yr_short = "16"
elif input_file.find('17') != -1:
    year = "2017"
    yr_short = "17"
elif input_file.find('18') != -1:
    year = "2018"
    yr_short = "18"
else:
    year = "2015"
    yr_short = "15"

if input_file.find('rel20') != -1:
    rel_label = ", rel 20"
elif input_file.find('rel21') != -1:
    rel_label = ", rel 21"
else:
    rel_label = ""

f = root_open(input_file)

for method in methods:
    if method == 'BDT':
        hist_label = 'BDT_hh'
        title_str = 'BDT'
    elif method == 'NN':
        hist_label = 'NN_hh'
        title_str = 'NN'
    else:
        hist_label = 'qcd_hh'
        title_str = 'Multijet'

    for reg in ["LowHt", "HighHt"]:
        if reg == "LowHt":
            title_label = "Low"
        else:
            title_label = "High"

        hist_nom = f.Get(hist_label)

        hist_VRw = f.Get(hist_label+"_"+reg+"VRw")
        hist_VRi = f.Get(hist_label+"_"+reg+"VRi")

        hist_nom.SetFillStyle(0)
        hist_nom.SetLineColor(ROOT.kBlack)
        hist_nom.SetLineWidth(2)
        hist_nom.SetTitle("Nominal " + title_str)

        hist_VRw.SetFillStyle(0)
        hist_VRw.SetLineColor(ROOT.kRed)
        hist_VRw.SetLineWidth(2)
        hist_VRw.SetTitle(title_label+" H_{T} VR Weighted")

        hist_VRi.SetFillStyle(0)
        hist_VRi.SetLineColor(ROOT.kBlue)
        hist_VRi.SetLineWidth(2)
        hist_VRi.SetTitle(title_label+" H_{T} VR Inverted")
 
        canvas = Canvas()
        canvas.cd()
        ROOT.gStyle.SetOptStat(0)
        pad1 = Pad(0.0,0.33,1.0,1.0)
        pad2 = Pad(0.0,0.0,1.0,0.33)
        pad1.SetBottomMargin(0.015)
        pad1.SetTopMargin(0.05)
        pad2.SetTopMargin(0.05)
        pad2.SetBottomMargin(0.3)
        pad1.Draw()
        pad2.Draw()
        pad1.SetTicks(1, 1)

        pad1.cd() 
        hist_nom.legendstyle = 'L'
        hist_VRw.legendstyle = 'L'
        hist_VRi.legendstyle = 'L'
        legend = Legend([hist_nom, hist_VRw, hist_VRi], leftmargin=0.33, margin=0.3, entrysep=0.05, textsize=0.07)

        #legend.SetHeader(year+" Signal Region"+rel_label)
    
        if input_file.find('HCrescale') != -1:
            x_axis_title = 'm_{HH} (corrected) [GeV]'
        else:
            x_axis_title = 'm_{HH} [GeV]'

        hist_nom.SetTitle("")
        hist_VRw.SetTitle("")
        hist_VRi.SetTitle("")

       # if label.find("norm") != -1:
       #     print(count_VR, count_nom, count_VR/count_nom)
       #     hist_nom.Scale(hist_VRw.Integral()/hist_nom.Integral())

        #if reg == "HighHt":
        #    max_y = 400
        #else:
        max_y = hist_nom.GetMaximum()*1.6 #np.maximum(hist_VRw.GetMaximum(), hist_VRi.GetMaximum())*1.4#100*(year == "2015") + 2000*(year == "2016") + 3000*(year=="2017") + 3000*(year=="2018") 
        hist_nom.GetYaxis().SetRangeUser(0, max_y)
        hist_nom.Draw("hist")
        hist_VRw.Draw("hist same")
        hist_VRi.Draw("hist same")

        hist_nom.GetXaxis().SetLabelSize(0)
        hist_nom.GetYaxis().SetLabelSize(0.038)

        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
        legend.Draw()


        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextColor(ROOT.kBlack)
        l.SetTextSize(0.08)
        l.DrawLatex(0.15, 0.84, "#it{ATLAS} #bf{Internal}")
        l.Draw()

        l2 = ROOT.TLatex()
        l2.SetNDC()
        l2.SetTextColor(ROOT.kBlack)
        l2.SetTextSize(0.05)

        lumi={}
        lumi['15'] = 3.2
        lumi['16'] = 24.6
        lumi['17'] = 43.65
        lumi['18'] = 58.45

        l2.DrawLatex(0.15, 0.775, ("#bf{#sqrt{s} = 13 TeV, "+year+" %.1f fb^{-1}}") % (lumi[yr_short]))
        l2.Draw()

        l3 = ROOT.TLatex()
        l3.SetNDC()
        l3.SetTextColor(ROOT.kBlack)
        l3.SetTextSize(0.05)

        l3.DrawLatex(0.15, 0.71, "#bf{Resolved Signal Region}")
        l3.Draw()

        tag = ROOT.TLatex()
        tag.SetNDC()
        tag.SetTextColor(ROOT.kBlack)
        tag.SetTextSize(0.04)
        tag.DrawLatex(0.75, 0.96, "#bf{"+NNT_tag+"}")
        tag.Draw()




        pad2.cd()
        ROOT.gStyle.SetOptStat(0)
        rat_VRwnom = hist_VRw.Clone("rat_VRwnom")
        rat_VRwnom.Divide(hist_nom)

        rat_VRinom = hist_VRi.Clone("rat_VRinom")
        rat_VRinom.Divide(hist_nom)

        if reg == 'LowHt':
            ratrange=[0.75,1.25]
        else:
            ratrange=[0.5,1.5]
        rat_VRwnom.GetYaxis().SetRangeUser(ratrange[0], ratrange[1])
        rat_VRwnom.GetYaxis().SetNdivisions(503)
        rat_VRwnom.GetXaxis().SetLabelSize(0.1)
        rat_VRwnom.GetYaxis().SetLabelSize(0.1)
        rat_VRwnom.GetYaxis().SetLabelOffset(0.03)
        rat_VRwnom.GetYaxis().SetTitle("Var / Nom ")
        rat_VRwnom.GetYaxis().CenterTitle();
        rat_VRwnom.GetXaxis().SetTitle(x_axis_title)
        rat_VRwnom.SetTitle('')

        rat_VRwnom.GetYaxis().SetTitleSize(0.12)
        rat_VRwnom.GetXaxis().SetTitleSize(0.1)
        rat_VRwnom.GetYaxis().SetTitleOffset(0.35)
        rat_VRwnom.GetXaxis().SetTitleOffset(1.1)

        rat_VRwnom.SetMarkerStyle(20)
        rat_VRwnom.SetMarkerSize(0.6)
        rat_VRwnom.SetMarkerColor(ROOT.kRed)
        rat_VRwnom.SetLineColor(ROOT.kRed)
        rat_VRwnom.SetLineStyle(1)
        rat_VRwnom.SetLineWidth(1)
        rat_VRwnom.Draw("pe")
   
        rat_VRinom.SetMarkerStyle(20)
        rat_VRinom.SetMarkerSize(0.6)
        rat_VRinom.SetMarkerColor(ROOT.kBlue)
        rat_VRinom.SetLineColor(ROOT.kBlue)
        rat_VRinom.SetLineStyle(1)
        rat_VRinom.SetLineWidth(1)
        rat_VRinom.Draw("pe same")

        line = ROOT.TLine(rat_VRwnom.GetXaxis().GetXmin(), 1, rat_VRwnom.GetXaxis().GetXmax(), 1)
        line.SetLineColor(1)
        line.SetLineWidth(1)
        line.SetLineStyle(1)
        line.Draw()

        canvas.Draw()

        out_name = NNT_tag+'_'+os.path.basename(input_file)[:-5]+label+"_mhh_shapeVar_"+reg+".pdf"
        canvas.SaveAs(output_dir+out_name.replace('_', '-'))

        pad1.cd()
        ROOT.gPad.SetLogy()
        hist_nom.GetYaxis().SetRangeUser(1e-1,max_y*30)
        out_name_log = NNT_tag+'_'+os.path.basename(input_file)[:-5]+label+"_mhh_shapeVar_"+reg+"_logscale.pdf"
        canvas.SaveAs(output_dir+out_name_log.replace('_', '-'))
