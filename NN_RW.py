#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

import os

from sklearn.preprocessing import StandardScaler
import pickle

import pandas as pd
from pandas import DataFrame

from rw_utils import *
from rw_model import *
from rw_common import *

from keras.callbacks import EarlyStopping, ModelCheckpoint

import yaml
import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import uproot
from ROOT import TFile, TParameter

from tqdm import tqdm

def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0-phi1) - np.pi)

def dR_hh(arr):
    return np.sqrt(dPhi(arr['phi_h1'], arr['phi_h2'])**2+(arr['eta_h1']-arr['eta_h2'])**2)

def theta_c(x,x_c,y,y_c):
    x1 = x - x_c
    x2 = y - y_c
    return np.arctan2(x2,x1)

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    output_dir = inputs["output_dir"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    rw_tree = inputs["rw_tree"]
    kinematic_region = inputs["kinematic_region"]
    doBootstrap = inputs["doBootstrap"]
    doNominal = inputs["doNominal"]
    n_resamples_here = inputs["n_resamples_here"]
    start = inputs["start"]
    job = inputs["job"]
    n_jobs = inputs["n_jobs"]
    full = inputs["full"]
    all_trees = inputs["all_trees"]
    param_dict = inputs["param_dict"]
    param_label = inputs["param_label"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    doLog = inputs["doLog"]
    weight_label = inputs["weight_label"]
    config = inputs["config"]
    use_old_names = inputs["use_old_names"]
    out_tree_list = inputs["out_tree_list"]
    doVBF = inputs["doVBF"]

    if not config:
        print("Please specify a configuration with --config. Available options are baseline (spline+log(pt_hh)) or dPhi_HC_505050 (current \"best\")")
        return 0

    flabel=[]
    for fname in data_file:
        yr_idx = fname.find(yr_short)
        ext_idx = fname.find(".root")
        flabel.append(fname[yr_idx+2:ext_idx])

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
               'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt', 'pt_hh', 'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2',
               'phi_h1_j1', 'phi_h1_j2', 'phi_h2_j1', 'phi_h2_j2', 'phi_h1', 'phi_h2', 'eta_h1', 'eta_h2']

    vbf_exists= (b'pass_vbf_sel' in uproot.open(data_file[0])[rw_tree].keys())
    if vbf_exists: 
        columns_load+=['pass_vbf_sel']

    if kinematic_region:
        columns_load+= ['kinematic_region']

    if 'columns' in param_dict.keys():
        for column in param_dict['columns']:
            if column not in columns_load:
                columns_load+=[column]

    columns_mc_load = columns_load + ['mc_sf']

    print("About to load in data...")
    print(data_file, rw_tree, year)

    files_in = []
    for fidx in range(len(data_file)):
        print(os.path.basename(data_file[fidx]))
        files_in.append(uproot.open(data_file[fidx]))
        count = 0
        for cluster in tqdm(list(files_in[fidx][rw_tree].clusters())):
            if count == 0 and fidx == 0:
                full_data = files_in[fidx][rw_tree].pandas.df(columns_load, 
                                                    entrystart=cluster[0], entrystop=cluster[1])
            else:
                full_data = full_data.append(files_in[fidx][rw_tree].pandas.df(columns_load, 
                                              entrystart=cluster[0], entrystop=cluster[1]), ignore_index=True)
            count+=1
    #full_data = f_in[rw_tree].pandas.df(columns_load)

    if kinematic_region:
        if use_old_names:
            kr_dict = {"control" : 1, "sideband" : 2}
        else:
            kr_dict = {"validation" : 1, "control" : 2}

        full_data = full_data[(full_data["kinematic_region"] == kr_dict[kinematic_region])]

    if vbf_exists:
        if doVBF:
            print("Running VBF background")
            full_data = full_data[full_data["pass_vbf_sel"]]
        else:
            print("Running ggF background - removing events passing VBF sel")
            full_data = full_data[~full_data["pass_vbf_sel"]]
    else:
        print("pass_vbf_sel flag not in NNT")

    if do_tth:
        tth_in = uproot.open(tth_file)
        full_tth = tth_in[rw_tree].pandas.df(columns_mc_load)

    if do_tnh:
        tnh_in = uproot.open(tnh_file)
        full_ttnh = tnh_in[rw_tree].pandas.df(columns_mc_load)

    print("Making dPhi")
    full_data.insert(0, 'dPhi_h1', dPhi(full_data['phi_h1_j1'], full_data['phi_h1_j2']), False)
    full_data.insert(0, 'dPhi_h2', dPhi(full_data['phi_h2_j1'], full_data['phi_h2_j2']), False)
    full_data.insert(0, 'dR_hh', dR_hh(full_data), False)
    
    print("Making theta")
    full_data.insert(0,'theta_c',theta_c(full_data['m_h1'],120,full_data['m_h2'],110),False)

    if config == "dPhi_HC_505050":
        print("Running dPhi_HC_505050")
        model_size = 50
        rw_columns =  ['theta_c','pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log', 'dRjj_2_log',
                       'njets', 'pt_hh_log', 'X_wt_log', 'dR_hh', 'dPhi_h1', 'dPhi_h2']
    else:
        model_size = 20
        rw_columns =  ['pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log', 'dRjj_2_log',
                       'njets', 'pt_hh_log']

    if 'columns' in param_dict.keys():
        rw_columns = param_dict['columns']

    for col in rw_columns:
        log_idx = col.find('_log')
        col_base = col[:log_idx]
        if log_idx != -1 and col_base not in doLog:
            doLog+=[col_base]

    for col in doLog:
        if col+'_log' not in rw_columns:
            if col in rw_columns:
                print("Need to explicitly put " + col + "_log in rw_columns!")
                return 0
            else:
                rw_columns += [col+'_log']

        

    print("Loaded! Setting up for training...")
    full_data.insert(0, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_data)), False)
    if do_tth: full_tth.insert(0, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_tth)), False)
    if do_tnh: full_ttnh.insert(0, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_ttnh)), False)


    if doLog:
        print("Taking logs of", doLog)
        for col in doLog:
            full_data.insert(0, col+'_log', np.log(full_data[col]), False)

    #Seed for reproducibility
    np.random.seed(1000)

    #Run random so that the call here corresponds to the start'th call  
    if doBootstrap:
        for run in range(start):
            _ = np.random.poisson(1, len(full_data))

        for count in range(n_resamples_here+doNominal):
            if doNominal and count == 0:
                resamp_id = ""
            else:
                resamp_id = "%d" % (start+count-doNominal)
                poisson_weight = np.random.poisson(1, len(full_data))
                full_data.insert(0, 'poissonWeight_resamp_'+resamp_id, poisson_weight, False)


    arrs = {}
    arrs['dat2'], arrs['dat4'] = driver.preprocess(full_data, year)
    if do_tth:
        arrs['th2'], arrs['th4'] = driver.preprocess(full_tth, year)
    if do_tnh:
        arrs['tnh2'], arrs['tnh4'] = driver.preprocess(full_ttnh, year)


    print("2 tag:", len(arrs['dat2']), "4 tag:", len(arrs['dat4']))
    original = DataFrame(arrs['dat2'][rw_columns])
    target = DataFrame(arrs['dat4'][rw_columns])

    X_all = pd.concat((original, target), ignore_index=True).values

    Y_all = []
    for _df, ID in [(original,1), (target, 0)]:
        Y_all.extend([ID] * _df.shape[0])
    Y_all = np.array(Y_all)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_all)
    Y_train = Y_all.copy()

    pickle.dump(scaler, open(output_dir+"StandardScaler_"+yr_short+reg_label+".pkl", "wb"))

    #If not splitting, need to shuffle
    print("Total training events:", len(X_train))
    idxs = np.random.permutation(len(X_train))
    X_train = X_train[idxs]
    Y_train = Y_train[idxs]

    #Defaults
    print("About to train...")
    print("Training on columns:", rw_columns)


    reweighter_dict = {}
    for count in range(n_resamples_here+doNominal):
        if doNominal and count == 0:
            resamp_id = ""
        else:
            resamp_id = "%d" % (start+count-doNominal)

        reweighter_dict[resamp_id] = model(X_train.shape, size=model_size)
        reweighter_dict[resamp_id].compile(loss=louppe_loss, metrics=['accuracy'], optimizer="adam")
        reweighter_dict[resamp_id].summary()

        if resamp_id:
            print("Resampling id:", resamp_id)
            original_weights = arrs['dat2']['poissonWeight_resamp_'+resamp_id]
            target_weights = arrs['dat4']['poissonWeight_resamp_'+resamp_id]
        else:
            original_weights = np.ones(len(arrs['dat2']))
            target_weights = np.ones(len(arrs['dat4']))

        poisson_weights = np.concatenate((original_weights, target_weights))[idxs]

        history = reweighter_dict[resamp_id].fit(X_train, Y_train, sample_weight = poisson_weights,
                            callbacks = [
                                EarlyStopping(monitor='val_loss', patience=10, verbose=True),
                                    ModelCheckpoint(output_dir+'NN_'+yr_short+reg_label+label+'_'+resamp_id+'.h5', monitor='val_loss', verbose=True, save_best_only=True) ],
                                    epochs=80,
                                    validation_split = 0.4,
                                    batch_size=8192,
                           )

        #Setting weights to checkpointed best
        reweighter_dict[resamp_id].load_weights(output_dir+'NN_'+yr_short+reg_label+label+'_'+resamp_id+'.h5')

    if out_tree_list:
        treeNames = out_tree_list
    elif all_trees:
        f = TFile(data_file[0], "read")
        treeNames= [key.GetName() for key in f.GetListOfKeys() if key.GetClassName() == "TTree" and key.GetName()]
        f.Close()
    else:
        treeNames=[rw_tree]

    save_list = ['event_number', 'run_number']
    if full:
        save_list = columns_load.copy()
        save_list_mc = columns_mc_load.copy()

    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            save_list += ['NN_d24_weight'+reg_label+weight_label+resamp_label]
        else:
            save_list += ['NN_d24_weight'+reg_label+weight_label]
    save_list_mc = save_list.copy()

    for fidx in range(len(data_file)):
        with uproot.recreate(output_dir+'dat_NN_d24_'+yr_short+flabel[fidx]+reg_label+label+'.root') as f:
            print("About to write weights for trees: ", treeNames, "in file", os.path.basename(data_file[fidx]))
            for treeCount in range(len(treeNames)):
                treeName = treeNames[treeCount]   

                tree_dict = {}
                for col in save_list:
                    if col.find('d24_weight') == -1:
                        tree_dict[col] = str(full_data[col].dtype) 
                for resamp_id in reweighter_dict.keys():
                    if resamp_id:
                        resamp_label = "_resampling_"+resamp_id
                    else:
                        resamp_label = ""

                    tree_dict['NN_d24_weight'+reg_label+weight_label+resamp_label] = 'float64'

                f[treeName] = uproot.newtree(tree_dict) 
     
                print("Writing tree", treeName, "in clusters")
                for cluster in tqdm(list(files_in[fidx][treeName].clusters())):
                    data_out = files_in[fidx][treeName].pandas.df(columns_load, entrystart=cluster[0], entrystop=cluster[1])
                    data_out.insert(0, 'dPhi_h1', dPhi(data_out['phi_h1_j1'], data_out['phi_h1_j2']), False)
                    data_out.insert(0, 'dPhi_h2', dPhi(data_out['phi_h2_j1'], data_out['phi_h2_j2']), False)
                    data_out.insert(0, 'dR_hh', dR_hh(data_out), False)
                    data_out.insert(0, 'theta_c',theta_c(data_out['m_h1'],120,data_out['m_h2'],110),False)

                    if vbf_exists:
                        if doVBF:
                            data_out = data_out[data_out["pass_vbf_sel"]]
                        else:
                            data_out = data_out[~data_out["pass_vbf_sel"]]

                    if doLog:
                        #print("Taking logs for output")
                        for col in doLog:
                            data_out.insert(0, col+'_log', np.log(data_out[col]), False)

                    for resamp_id in reweighter_dict.keys():
                        if resamp_id:
                            resamp_label = "_resampling_"+resamp_id
                        else:
                            resamp_label = ""

                        data_out.insert(0, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(data_out)), False)
                        isdat2 = (data_out['ntag'] == 2)
                        if np.sum(isdat2) == 0:
                            continue
                        pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(data_out[isdat2][rw_columns]).values), batch_size=8192)
                        data_out.loc[isdat2, 'NN_d24_weight'+reg_label+weight_label+resamp_label] = np.exp(pred)[:,0]

                    f[treeName].extend(data_out[save_list].to_dict(orient='list'))

        if do_tth:
            with uproot.recreate(output_dir+'tth_NN_d24_'+yr_short+reg_label+label+'.root') as f:
                print("About to write weights for trees: ", treeNames)
                for treeCount in range(len(treeNames)):
                    treeName = treeNames[treeCount]

                    tree_dict = {}
                    for col in save_list:
                        if col.find('d24_weight') == -1:
                            tree_dict[col] = str(full_tth[col].dtype)    
                    for resamp_id in reweighter_dict.keys():
                        if resamp_id:
                            resamp_label = "_resampling_"+resamp_id
                        else:
                            resamp_label = ""

                        tree_dict['NN_d24_weight'+reg_label+weight_label+resamp_label] = 'float64'    

                    f[treeName] = uproot.newtree(tree_dict)

                    print("Writing tree", treeName, "in clusters")
                    for cluster in tqdm(list(tth_in[treeName].clusters())):
                        tth_out = tth_in[treeName].pandas.df(columns_load, entrystart=cluster[0], entrystop=cluster[1])
                        #print("Making dPhi ")
                        tth_out.insert(0, 'dPhi_h1', dPhi(tth_out['phi_h1_j1'], tth_out['phi_h1_j2']), False)
                        tth_out.insert(0, 'dPhi_h2', dPhi(tth_out['phi_h2_j1'], tth_out['phi_h2_j2']), False)
                        tth_out.insert(0, 'dR_hh', dR_hh(tth_out), False)

                        if doLog:
                            #print("Taking logs for output")
                            for col in doLog:
                                tth_out.insert(0, col+'_log', np.log(tth_out[col]), False)

                        for resamp_id in reweighter_dict.keys():
                            if resamp_id:
                                resamp_label = "_resampling_"+resamp_id
                            else:
                                resamp_label = ""

                            tth_out.insert(0, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(tth_out)), False)
                            istth2 = (tth_out['ntag'] == 2)
                            if np.sum(istth2) == 0:
                                continue
                            pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(tth_out[istth2][rw_columns]).values), batch_size=8192)
                            tth_out.loc[istth2, 'NN_d24_weight'+reg_label+weight_label+resamp_label] = np.exp(pred)[:,0]

                        f[treeName].extend(tth_out[save_list].to_dict(orient='list'))

        if do_tnh:
            with uproot.recreate(output_dir+'ttnh_NN_d24_'+yr_short+reg_label+label+'.root') as f:
                print("About to write weights for trees: ", treeNames)
                for treeCount in range(len(treeNames)):
                    treeName = treeNames[treeCount]

                    tree_dict = {}
                    for col in save_list:
                        if col.find('d24_weight') == -1:
                            tree_dict[col] = str(full_ttnh[col].dtype)    
                    for resamp_id in reweighter_dict.keys():
                        if resamp_id:
                            resamp_label = "_resampling_"+resamp_id
                        else:
                            resamp_label = ""

                        tree_dict['NN_d24_weight'+reg_label+weight_label+resamp_label] = 'float64'    

                    f[treeName] = uproot.newtree(tree_dict)

                    print("Writing tree", treeName, "in clusters")
                    for cluster in tqdm(list(tnh_in[treeName].clusters())):
                        tnh_out = tnh_in[treeName].pandas.df(columns_load, entrystart=cluster[0], entrystop=cluster[1])
                        #print("Making dPhi ")
                        tnh_out.insert(0, 'dPhi_h1', dPhi(tnh_out['phi_h1_j1'], tnh_out['phi_h1_j2']), False)
                        tnh_out.insert(0, 'dPhi_h2', dPhi(tnh_out['phi_h2_j1'], tnh_out['phi_h2_j2']), False)
                        tnh_out.insert(0, 'dR_hh', dR_hh(tnh_out), False)

                        if doLog:
                            #print("Taking logs for output")
                            for col in doLog:
                                tnh_out.insert(0, col+'_log', np.log(tnh_out[col]), False)

                        for resamp_id in reweighter_dict.keys():
                            if resamp_id:
                                resamp_label = "_resampling_"+resamp_id
                            else:
                                resamp_label = ""

                            tnh_out.insert(0, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(tnh_out)), False)
                            istnh2 = (tnh_out['ntag'] == 2)
                            if np.sum(istnh2) == 0:
                                continue  
                            pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(tnh_out[istnh2][rw_columns]).values), batch_size=8192)
                            tnh_out.loc[istnh2, 'NN_d24_weight'+reg_label+weight_label+resamp_label] = np.exp(pred)[:,0]


                        f[treeName].extend(tnh_out[save_list].to_dict(orient='list'))


    isdat2 = full_data['ntag']==2
    f_root = TFile(output_dir+'dat_NN_d24_'+yr_short+flabel[0]+reg_label+label+'.root', "update")
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            original_weights = full_data['poissonWeight_resamp_'+resamp_id]
            full_data.insert(0, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(full_data)), False)
        else:
            resamp_label = ""
            original_weights = np.ones(len(full_data))

        pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(full_data[isdat2][rw_columns]).values), batch_size=8192)
        full_data.loc[isdat2, 'NN_d24_weight'+reg_label+weight_label+resamp_label] = np.exp(pred)[:, 0]
        norm_factor = (1.*np.sum(original_weights[full_data['ntag']>=4])/
                       np.sum(original_weights[full_data['ntag']==2]*full_data[full_data['ntag']==2]['NN_d24_weight'+reg_label+weight_label+resamp_label]))

        norm = TParameter("double")("NN_norm"+reg_label+weight_label+resamp_label, norm_factor)
        norm.Write()

    f_root.Close()

if __name__ == '__main__':
    driver = common("NN_RW.py")

    parser = driver.makeParser()
    parser.add_argument("--out-tree-list", dest="out_tree_list", default=[], nargs="+",
                        help="List of trees to write out. By default, does everything in input.")
    parser.add_argument("--vbf",
                        action="store_true", dest="doVBF", default= False,
                        help="Use for VBF selection. If not passed, will veto VBF events if flag in NNT")
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    inputs['out_tree_list'] = args.out_tree_list
    inputs['doVBF'] = args.doVBF

    main(inputs)
