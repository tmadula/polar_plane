import argparse

class common:
    def __init__(self, filename):
        self.filename = filename
    def preprocess(self, full_array, year):
        #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
        run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                       '2017' : [324320, 348197], '2018' : [348197, 364486] }
        year_run_range = run_bounds[year]

        full_array = full_array[(full_array['run_number'] > year_run_range[0]) & (full_array['run_number'] < year_run_range[1])]

        sel2= full_array[full_array['ntag'] == 2]
        sel4= full_array[full_array['ntag'] >= 4]

        return sel2, sel4

    def makeParser(self):
        filename = self.filename
        isBDT_RW = (filename == "BDT_RW.py")
        isNN_RW = (filename == "NN_RW.py")
        isSpline_RW = (filename == "reweight.py")
        isCombine = (filename == "combine_weights.py")
        isPlotting = (filename == "make_rw_plots.py")

        parser = argparse.ArgumentParser()
        parser.add_argument("-i", "--input_dir", dest="input_dir", default="",
                              help="Input directory")
        parser.add_argument("-d", "--data", dest="data_file", default=[], nargs="+",
                            help="Input data filename")
        parser.add_argument("-t", "--tth", dest="tth_file", default="",
                            help="Input all had ttbar filename")
        parser.add_argument("-n", "--tnh", dest="tnh_file", default="",
                            help="Input non all-had ttbar filename")
        parser.add_argument("-y", "--year", dest="year", default="2015",
                             help="Year")
        parser.add_argument("-l", "--label", dest="label", default="",
                             help="Label for output file (e.g., pflow)")
        parser.add_argument("--weight-label", dest="weight_label", default="",
                            help="Label for alternate weight derivation (e.g. logpT)")
        parser.add_argument("--use-old-names",
                            action="store_true", dest="use_old_names", default=False,
                            help="Use SB/CR/SR naming scheme")
        parser.add_argument("-o", "--output_dir", dest="output_dir", default="",
                            help="Output directory")

        if isPlotting:
            parser.add_argument("--tree", dest="tree", default="",
                              help="Tree on which to make plots")
            parser.add_argument("--do-CRw", action="store_true", dest="doCRw", default=False,
                              help="Use CR derived weights")
            parser.add_argument("--do-VRw", action="store_true", dest="doVRw", default=False,
                              help="Use VR derived weights")
            parser.add_argument("--HCrescale",
                              action="store_true", dest="HCrescale", default=False,
                              help="Rescale HC 4 vecs to have m=125 GeV")
            parser.add_argument("--bstrap_ave",
                              action="store_true", dest="bstrap_ave", default=False,
                              help="Use average of bstrap as nominal")
            parser.add_argument("--bstrap_med",
                              action="store_true", dest="bstrap_med", default=False,
                              help="Use median of bstrap as nominal")
            parser.add_argument("--run-shape-systs",
                              action="store_true", dest="doShapeSysts", default=False,
                              help="Turn on plotting with shape syst")
            parser.add_argument("--pulls",
                              action="store_true", dest="doPulls", default=False,
                              help="Plot with pulls in bottom panel and make dists")
            parser.add_argument("--NNT_tag", dest="NNT_tag", default="NNT_FEB20_pflow_bdt",
                              help="NNT production tag")

        if isPlotting or isBDT_RW or isNN_RW or isSpline_RW:
            parser.add_argument("--kinematic-region", dest="kinematic_region", default="",
                                help="Kinematic region to run reweighting. Put in control or validation")

        if isBDT_RW or isNN_RW or isSpline_RW:
            parser.add_argument("--reweight-tree", dest="reweight_tree", default="",
                                help="Tree on which to run reweighting.")
            parser.add_argument("-f", "--full",
                                action="store_true", dest="full", default=False,
                                help="Turn on write variables - otherwise, just weights/event_number")

            parser.add_argument("--not-all-trees",
                                action="store_true", dest="not_all_trees", default=False,
                                help="Turn off calculating weight for all trees")


        if isPlotting:
            parser.add_argument("-b", "--BDT",
                                action="store_true", dest="BDT", default=False,
                                help="Turn on BDT")
            parser.add_argument("--nn",
                                action="store_true", dest="NN", default=False,
                                help="Turn on NN")
            parser.add_argument("-s", "--spline",
                                dest="spline", default=-1,
                                help="Turn on spline, give number of iters. If default (=-1), won't turn on")

        if isCombine:
            parser.add_argument("-w", "--weight_dir", dest="weight_dir", default="",
                                 help="Directory with weight files")
            parser.add_argument("--comb-label", dest="comb_label", default="with_weights",
                                help="Writes tree to file with argument as suffix.")

        if isBDT_RW or isNN_RW or isCombine or isPlotting:
            parser.add_argument("--bootstrap", dest="n_resamples", default="-1",
                                help="Turn on bootstrapping. Put in number of resamples.")

        if isBDT_RW or isNN_RW or isCombine:
            parser.add_argument("--n_jobs", dest="n_jobs", default="",
                                help="Total number of jobs (to be used for bootstrap only)")

        if isBDT_RW or isNN_RW:
            parser.add_argument("-p", "--parameters", dest="params", default="", nargs='+',
                                help = "Read in parameters for BDT optimization. Feed in columns or use a YAML file (recommended)")

            parser.add_argument("--job", dest="job", default ="",
                                help="Condor job number (to be used for bootstrap only)")

            parser.add_argument("--doLog", dest="doLog", default=[], nargs='+',
                                 help = "List of columns to take the log of for training") 

        if isBDT_RW:
            parser.add_argument("-s", "--save",
                                action="store_true", dest="save", default=False,
                                help="Turn on save BDT (pickle)")

        if isNN_RW:
            parser.add_argument("--config", dest="config", default="",
                                help="Choose baseline or dPhi_HC_505050.")

        if isSpline_RW:
            parser.add_argument("--no-fit",
                              action="store_false", dest="doFit", default=True,
                              help="Turn off fit")
            parser.add_argument("--useOldLumi",
                              action="store_false", dest="useUpdatedLumi", default=True,
                              help="Use old lumi values (incomplete 2018 dataset)")

        return parser

    def interpretArgs(self, args):
        filename=self.filename
        isBDT_RW = (filename == "BDT_RW.py")
        isNN_RW = (filename == "NN_RW.py")
        isSpline_RW = (filename == "reweight.py")
        isCombine = (filename == "combine_weights.py")
        isPlotting = (filename == "make_rw_plots.py")

        input_dir = args.input_dir
        output_dir = args.output_dir
        data_file = args.data_file
        tth_file = args.tth_file
        tnh_file = args.tnh_file
        year_in = args.year
        label = args.label
        weight_label = args.weight_label
        use_old_names = args.use_old_names

        if isPlotting:
            tree = args.tree
            doCRw = args.doCRw
            doVRw = args.doVRw
            HCrescale=args.HCrescale
            bstrap_ave = args.bstrap_ave
            bstrap_med = args.bstrap_med
            doShapeSysts = args.doShapeSysts
            doPulls = args.doPulls
            NNT_tag = args.NNT_tag

        if isPlotting:
            BDT = args.BDT
            NN = args.NN
            n_it = int(args.spline)
            spline = (n_it != -1)

        if isCombine:
            weight_dir = args.weight_dir
            comb_label = args.comb_label

        if isPlotting or isBDT_RW or isNN_RW or isSpline_RW:
            kinematic_region = args.kinematic_region

        if isBDT_RW or isNN_RW or isSpline_RW:
            rw_tree = args.reweight_tree
            full = args.full
            all_trees= not args.not_all_trees

        if isBDT_RW or isNN_RW or isCombine or isPlotting:
            n_resamples = int(args.n_resamples)

        if isBDT_RW or isNN_RW or isCombine:
            n_jobs=args.n_jobs

        if isBDT_RW or isNN_RW:
            params = args.params
            job=args.job
            doLog = args.doLog

        if isBDT_RW:
            save = args.save

        if isNN_RW:
            config = args.config

        if isSpline_RW:
            doFit = args.doFit
            useUpdatedLumi = args.useUpdatedLumi

        #Processing for year input
        year =''
        yr_short=''
        if len(year_in) == 4:
            year=year_in
            yr_short=year_in[2:]
        elif len(year_in) == 2:
            year='20'+year_in
            yr_short=year_in
        else:
            print("Invalid year format, defaulting to 2015")
            year='2015'
            yr_short='15'

        #Make sure uniform input and output dir format
        if input_dir:
            if input_dir[-1] != '/': input_dir+='/'
        if output_dir:
            if output_dir[-1] != '/': output_dir+='/'
        if isCombine:
            if weight_dir:
                if weight_dir[-1] != '/': weight_dir+='/'
            else: 
                weight_dir = input_dir

        if data_file:
            for fidx in range(len(data_file)):
                input_dir_here = input_dir
                if data_file[fidx][0] == '/':
                    input_dir_here = ''
                data_file[fidx] = input_dir_here+data_file[fidx]
                if data_file[fidx][-4:] != 'root':
                    print("Warning! Data file not a root file")
                if data_file[fidx].find(yr_short) == -1:
                    print("Warning! Input data file name may not match year.")

        #Check if tth and ttnh are supplied
        if tth_file:
            do_tth = True
            if tth_file[0] == '/':
                input_dir = ''
            tth_file = input_dir+tth_file
            if tth_file[-4:] != 'root':
                print("Warning! All had ttbar file not a root file")
        else:
            do_tth = False

        if tnh_file:
            do_tnh = True
            if tnh_file[0] == '/':
                input_dir = ''
            tnh_file = input_dir+tnh_file
            if tnh_file[-4:] != 'root':
                print("Warning! Non all had ttbar file not a root file")
        else:
            do_tnh = False

        #Check that reweighting tree is sensible
        if isBDT_RW or isNN_RW or isSpline_RW:
            if rw_tree:
                if rw_tree not in [ "sideband", "control", "validation"] and not kinematic_region:
                    print("Warning! Reweighting tree not recognized. Continuing, but check inputs.")
            else:
                if use_old_names:
                    rw_tree = "sideband"
                else:
                    rw_tree = "control"

        #Label for CR derived weights
        reg_label=''

        if isPlotting:
            reg_label=''
            if doCRw:
                reg_label='_CRderiv'
                if not use_old_names:
                    print("Naming mismatch!!")
            if doVRw:
                reg_label='_VRderiv'

        if isBDT_RW or isNN_RW or isSpline_RW:
            if (rw_tree == "control" or kinematic_region == "control") and use_old_names: 
                reg_label="_CRderiv"
            elif rw_tree == "validation" or kinematic_region == "validation":
                reg_label="_VRderiv"

        #Proper formatting of other label
        if label:
            if label[0] != '_':
                label='_'+label

        if weight_label:
            if weight_label[0] != '_':
                weight_label='_'+weight_label

        if isPlotting:
            doBootstrap = (n_resamples != -1)

            if HCrescale:
                label+= "_HCrescale"

            if bstrap_ave:
                print("Using ave of bootstrap weights")
                label+='_bstrap_ave'
            if bstrap_med:
                print("Using med of bootstrap weights")
                label+='_bstrap_med'

            if doBootstrap:
                label+= "_bootstrap"

            if doShapeSysts:
                label += "_shapesyst"

            if doPulls:
                label += "_pulls"

        if isCombine:
            doBootstrap = False
            if n_resamples != -1:
                print("Combining bootstrapping from %d total resamples" % n_resamples)
                doBootstrap = True
                label+='_bootstrap'

            if n_jobs and not doBootstrap:
                print("You should only use n_jobs for bootstrap! Check inputs")

            if not n_jobs:
                n_jobs = 0

            n_jobs=int(n_jobs)

            if comb_label != "":
                if comb_label[-5:] == '.root':
                    comb_label = comb_label[:-5]
                if comb_label[0] == '_':
                    comb_label = comb_label[1:]
                                        

        if isBDT_RW or isNN_RW:
            #Set up bootstrapping
            doBootstrap = False
            if n_resamples != -1:
                print("Running bootstrapping with %d total resamples" % n_resamples)
                doBootstrap = True
                label+='_bootstrap'

            if doBootstrap and ((job and not n_jobs) or (n_jobs and not job)):
                print("Invalid condor info - running everything here")
                job = ""
                n_jobs = ""

            #Handle splitting into jobs
            n_resamples_here = 0
            doNominal=True
            start=0
            if doBootstrap:
                if job and n_jobs:
                    print("Job number", job, "(%d out of %d)" % (int(job)+1, int(n_jobs)))
                    label+=('_'+job)
                    job = int(job)
                    n_jobs = int(n_jobs)
                    n_per_job = int(n_resamples/n_jobs)
                    remainder = n_resamples % n_jobs

                    print("Splitting", n_resamples, "resamples into", n_jobs)

                    start = job * (n_per_job + 1)* (job < remainder) + (remainder + job * n_per_job)*(job >= remainder)
                    end = (job+1) * (n_per_job + 1)* (job < remainder) + (remainder + (job+1)* n_per_job)*(job >= remainder)

                    n_resamples_here = end-start
                    print("Will resample here", n_resamples_here, "times")
                    if job == 0:
                        print("Running nominal here as well")
                    else:
                        doNominal=False
                else:
                    start=0
                    end=n_resamples
                    n_resamples_here=end-start

                    print("Running nominal and", n_resamples_here, "boostrap resamples here")
     
            #Setup params
            param_dict = {}
            param_label = ''
            if params:
                if len(params) == 1:
                    params = params[0]

                if params.find('.yml') != -1 or params.find('.yaml') != -1:
                    print("Loading in BDT parameters from file", params)
                    param_label = params[:params.find('.y')]
                    with open(params, 'r') as stream:
                        param_dict = yaml.load(stream)
                    print("Loaded in params:")
                    for key in param_dict.keys():
                        print(key+':', param_dict[key])
                else:
                    print("Interpreting input as list of columns for training")
                    param_label = 'col_list'
                    param_dict['columns'] = params


        print("Input data file (s):", data_file)
        if do_tth:
            print("Input all-had ttbar file:", tth_file)

        if do_tnh:
            print("Input non all-had ttbar file:", tnh_file)

        if isBDT_RW or isNN_RW or isSpline_RW:
            print("Output directory:", output_dir)
            print("Training reweighting model on tree", rw_tree)
            if kinematic_region:
                print("Training in region", kinematic_region)

        print("Year:", year)


        inputs = {}
        inputs["year"] = year
        inputs["yr_short"] = yr_short
        inputs["do_tth"] = do_tth
        inputs["do_tnh"] = do_tnh
        inputs["data_file"] = data_file
        inputs["tth_file"] = tth_file
        inputs["tnh_file"] = tnh_file
        inputs["label"] = label
        inputs["reg_label"] = reg_label
        inputs["weight_label"] = weight_label
        inputs["output_dir"] = output_dir
        inputs["use_old_names"] = use_old_names

        if isPlotting:
            inputs["tree"] = tree
            inputs["doCRw"] = doCRw
            inputs["doVRw"] = doVRw
            inputs["HCrescale"] = HCrescale
            inputs["bstrap_ave"] = bstrap_ave
            inputs["bstrap_med"] = bstrap_med
            inputs["doShapeSysts"] = doShapeSysts
            inputs["doPulls"] = doPulls
            inputs["NNT_tag"] = NNT_tag
            inputs["BDT"] = BDT
            inputs["NN"] = NN
            inputs["n_it"] = n_it
            inputs["spline"] = spline

        if isPlotting or isBDT_RW or isNN_RW or isSpline_RW:
            inputs["kinematic_region"] = kinematic_region

        if isBDT_RW or isNN_RW or isSpline_RW:
            inputs["rw_tree"] = rw_tree
            inputs["full"] = full
            inputs["all_trees"] = all_trees

        if isCombine:
            inputs["weight_dir"] = weight_dir
            inputs["comb_label"] = comb_label

        if isCombine or isPlotting:
            inputs["n_resamples"] = n_resamples

        if isCombine or isBDT_RW or isNN_RW or isPlotting:
            inputs["doBootstrap"] = doBootstrap

        if isCombine or isBDT_RW or isNN_RW:
            inputs["n_jobs"] = n_jobs

        if isBDT_RW or isNN_RW:
            inputs["param_dict"] = param_dict
            inputs["param_label"] = param_label
            inputs["doNominal"]   = doNominal
            inputs["n_resamples_here"] = n_resamples_here
            inputs["start"] = start
            inputs["job"] = job
            inputs["doLog"] = doLog
        
        if isBDT_RW:
            inputs["save"] = save

        if isNN_RW:
            inputs["config"] = config
 
        if isSpline_RW:
            inputs["doFit"] = doFit
            inputs["useUpdatedLumi"] = useUpdatedLumi
    
        return inputs

