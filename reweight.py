#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *
from rw_common import *

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import root_numpy
from ROOT import TH1D, TFile, TParameter

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    output_dir = inputs["output_dir"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    rw_tree = inputs["rw_tree"]
    full = inputs["full"]
    all_trees = inputs["all_trees"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    doFit = inputs["doFit"]
    useUpdatedLumi = inputs["useUpdatedLumi"]
    use_old_names = inputs["use_old_names"]
    
    doCRw=(rw_tree=="control") and (use_old_names)
    doVRw=(rw_tree=="validation")

    if not do_tth or not do_tnh:
        print("ttbar files are missing. Exiting now")
        return

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
                    'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt']

    columns_mc_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
                       'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'event_number', 'X_wt']

    print("Initial load in of data...")

    #Load in data using root_numpy (for ROOT independence, can also update to uproot)
    full_data = root_numpy.root2array(data_file,
                                 treename=rw_tree, branches=columns_load)
    full_tth = root_numpy.root2array(tth_file,
                                 treename=rw_tree, branches=columns_mc_load)
    full_ttnh = root_numpy.root2array(tnh_file,
                                 treename=rw_tree, branches=columns_mc_load)

    #leptop_muon is region with >=1 muon of pT >= 25 GeV (no isolation yet, no Xwt cut)
    #leptop_hadron is region with >=1 electron of pT >= 25 GeV (no isolation yet, no Xwt cut)
    #hadtop is region with Xwt < 0.75
    #top_multijet is region with Xwt > 0.75
    if doFit:
        fit_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 
                       'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'mc_sf', 'kinematic_region']

        in_keys = ['dat', 'th', 'tnh']
        regions = ['qcd', 'tnh', 'th']

        fit_inputs = {}

        fit_inputs['dat'] = {}
        fit_inputs['dat']['tnh'] = root_numpy.root2array(data_file,
                                         treename='leptop_muon', branches=fit_columns)
        fit_inputs['dat']['th'] = root_numpy.root2array(data_file,
                                         treename='hadtop', branches=fit_columns)
        fit_inputs['dat']['qcd'] = root_numpy.root2array(data_file,
                                         treename='top_multijet', branches=fit_columns)

        fit_inputs['th'] = {}
        fit_inputs['th']['tnh'] = root_numpy.root2array(tth_file,
                                         treename='leptop_muon', branches=fit_columns)
        fit_inputs['th']['th'] = root_numpy.root2array(tth_file,
                                         treename='hadtop', branches=fit_columns)
        fit_inputs['th']['qcd'] = root_numpy.root2array(tth_file,
                                         treename='top_multijet', branches=fit_columns)

        fit_inputs['tnh'] = {}
        fit_inputs['tnh']['tnh'] = root_numpy.root2array(tnh_file,
                                         treename='leptop_muon', branches=fit_columns)
        fit_inputs['tnh']['th'] = root_numpy.root2array(tnh_file,
                                         treename='hadtop', branches=fit_columns)
        fit_inputs['tnh']['qcd'] = root_numpy.root2array(tnh_file,
                                         treename='top_multijet', branches=fit_columns)

        #Ranges from resolved-recon trigger-list.dat
        run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320], 
                       '2017' : [324320, 348197], '2018' : [348197, 364486] } 
        year_run_range = run_bounds[year]
     
        fit_arrs = {}
        for key in in_keys:
            fit_arrs[key+'2'] = {}
            fit_arrs[key+'4'] = {}
            for region in regions:
                arr = fit_inputs[key][region]

                #rel 21 resolved-recon doesn't include a rw_weight or njet_weight branch - add this in, init to ones
                arr = append_fields(arr, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [np.ones(len(arr)), np.ones(len(arr))], usemask=False)

                #From enum class region : char { Signal, Control, Sideband, Other }; 
                if rw_tree == "control":
                    arr = arr[arr['kinematic_region'] == 1]
                else: #sideband
                    arr = arr[arr['kinematic_region'] == 2]
                arr = arr[(arr['run_number'] > year_run_range[0]) & (arr['run_number'] < year_run_range[1])]
                fit_arrs[key+'2'][region] = arr[arr['ntag'] == 2]
                fit_arrs[key+'4'][region] = arr[arr['ntag'] >= 4]



    print("Data loaded in. About to start iteration loop")

    rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2']

    #Set up rw hist bins. To be optimized (has a significant effect)
    bins = {}

    #pT_4
    bins[rw_columns[0]] = np.arange(36, 700, (700-36)/20.)
    #pT_2
    bins[rw_columns[1]] = np.arange(36, 700, (700-36)/20.)
    #ave_eta
    bins[rw_columns[2]] = np.arange(0,4, 4./60)
    #dRjj close
    bins[rw_columns[3]] = np.arange(0,6, 6./50.)
    #dRjj not close
    bins[rw_columns[4]]= np.arange(0,6, 6./50.)

    #Include a rw_weight/njet_weight branch, init to ones
    full_data = append_fields(full_data, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [np.ones(len(full_data)), np.ones(len(full_data))], usemask=False)
    full_tth = append_fields(full_tth, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [np.ones(len(full_tth)), np.ones(len(full_tth))], usemask=False)
    full_ttnh = append_fields(full_ttnh, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [np.ones(len(full_ttnh)), np.ones(len(full_ttnh))], usemask=False)

    #Preselect
    arrs = {}
    arrs['dat2'], arrs['dat4'] = driver.preprocess(full_data, year)
    arrs['th2'], arrs['th4'] = driver.preprocess(full_tth, year)
    arrs['tnh2'], arrs['tnh4'] = driver.preprocess(full_ttnh, year)

    #Set up identifiers for data/fit regions
    keys = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']
    regions = ['qcd', 'tnh', 'th']

    dat_rw_weights = {}
    tth_rw_weights = {}
    ttnh_rw_weights = {}

    #Start iteration loop
    for it in range(7):

        #Set up minimizer class for pseudo-tag optimization
        minimizer = f_min(arrs, keys, year, doCRw=doCRw, doVRw=doVRw)
     
        #Initialize relevant containers
        hists = {}
        weights = {}
        fit_weights = {}
        fit_counts = {}
        count_errs = {}

        #Make histograms for reweighting, fit counts, and weights
        for key in keys:
                weights[key] = makeWeights(arrs, key, it, int(year), doCRw=doCRw, doVRw=doVRw, useUpdatedLumi=useUpdatedLumi)
                hists[key] = {}
                fit_counts[key] = {}
                count_errs[key] = {}
                fit_weights[key] = {}
                
                arr_Xwt_cut = arrs[key][arrs[key]['X_wt'] > 1.5]
                weights_Xwt_cut = weights[key][arrs[key]['X_wt'] > 1.5]

                for var in rw_columns:
                    hists[key][var], _ = np.histogram(arr_Xwt_cut[var], bins=bins[var], weights=weights_Xwt_cut)

                if doFit:
                    if key == 'th4_real':
                        continue
                    for region in regions:
                        fit_weights[key][region] = makeWeights(fit_arrs, key, it, int(year), region=region, doCRw=doCRw, doVRw=doVRw, useUpdatedLumi=useUpdatedLumi)
                        
                        fit_counts[key][region], count_errs[key][region] = getFitCounts(fit_arrs, fit_weights, key, region)

        sftnh = 1.
        sfth = 1.
        sftnh2 = 1.

        #Fill normalization to be used if skipping fit
        sfqcd = 1.*(np.sum(weights['dat4'])-np.sum(weights['th4'])-np.sum(weights['tnh4']))/(
                 np.sum(weights['dat2'])-np.sum(weights['th2'])-np.sum(weights['tnh2']))

        sftnh2_err = 0

        if doFit:
            #Non all-hadronic 2 tag is just scaled up to match data    
            sftnh2 = fit_counts['dat2']['tnh']/fit_counts['tnh2']['tnh']
            sftnh2_err = sftnh2*np.sqrt(((count_errs['dat2']['tnh'])/(fit_counts['dat2']['tnh']))**2 
                                       +((count_errs['tnh2']['tnh'])/(fit_counts['tnh2']['tnh']))**2)

            #Subtract ttbar to get qcd values for fits
            fit_counts['qcd'] = {}
            for region in regions:
                fit_counts['qcd'][region] = fit_counts['dat2'][region] - fit_counts['th2'][region] - fit_counts['tnh2'][region]*sftnh2

            print(fit_counts)
            #Run fits
            print("About to do fit for iteration", it)
            y=[0,0,0]
            y[0] = fit_counts['dat4']['tnh']
            y[1] = fit_counts['dat4']['th']
            y[2] = fit_counts['dat4']['qcd']

            popt, pcov = opt.curve_fit(lambda x, p0,p1,p2: f(x, p0,p1,p2,fit_counts), [0,1,2], y, sigma=np.sqrt(y), 
                                   absolute_sigma=True)

            sftnh = popt[0]
            sfth = popt[1]
            sfqcd = popt[2]
            sf_errs = np.sqrt(pcov.diagonal())

        #Pass fit scale factors to mimimizer
        sf_vals={'tnh':sftnh,'th':sfth, 'qcd':sfqcd, 'tnh2':sftnh2}
        minimizer.set_sfs(sf_vals)

        sftnh_err = 0
        sfth_err = 0
        sfqcd_err = 0
     
        if doFit:
            sftnh_err = sf_errs[0]
            sfth_err = sf_errs[1]
            sfqcd_err = sf_errs[2]

            print("tnh: %.3f +/- %.3f, th: %.3f +/- %.3f, qcd: %.3f +/- %.3f" % (popt[0],sf_errs[0], popt[1], sf_errs[1], popt[2], sf_errs[2]))
            print("tnh2: %.3f +/- %.3f" % (sftnh2, sftnh2_err))

        else:
            print("Not doing fit, setting ttbar scale factors at the nominal (i.e. 1)")
            print("Scale QCD up, sfqcd = %.3f" % sfqcd)

        print("About to make splines for iteration", it)
        #Make splines
        ratio = {}
        splines = {}
        spl_hists ={}
        spl_hists['qcd'] = {}
        spl_hists['dat4'] = {}
     
        #Controls the granularity of smoothing
        sigmas = [0.5,0.5,1,1,1]
        sigma = {}
        for i in range(len(rw_columns)):
            sigma[rw_columns[i]] = sigmas[i]

        for var in rw_columns:
                spl_hists['qcd'][var] = hists['dat2'][var] - hists['th2'][var] - hists['tnh2'][var]*sftnh2
                spl_hists['dat4'][var] = hists['dat4'][var] - hists['th4'][var]*sfth - hists['tnh4'][var]*sftnh
                      
                spl_hists['qcd'][var] = spl_hists['qcd'][var]/np.sum(spl_hists['qcd'][var])
                spl_hists['dat4'][var] = spl_hists['dat4'][var]/np.sum(spl_hists['dat4'][var])
                
                ratio[var] = spl_hists['dat4'][var]/(spl_hists['qcd'][var]+0.000001)
                for i in range(len(ratio[var])):
                        if ratio[var][i] <= 0: 
                                ratio[var][i] = 1.
                    
                sm_bin = ndimage.gaussian_filter1d(mid(bins[var]), sigma[var])
                sm_rat = ndimage.gaussian_filter1d(ratio[var], sigma[var])
                splines[var] = interp1d(sm_bin, sm_rat, kind='cubic',fill_value=1., bounds_error=False)


        print("Optimizing f for iteration", it)
        f_res = opt.minimize_scalar(minimizer.to_min, bounds=[0, 1], method='bounded')
        print("Best f =", f_res.x)

        print("Saving, prepping for next iteration")
        full_data['rw_weight'+reg_label] = calc_Weight(splines, full_data, rw_columns, it, doCRw=doCRw, doVRw=doVRw)
        full_tth['rw_weight'+reg_label] = calc_Weight(splines, full_tth, rw_columns, it, doCRw=doCRw, doVRw=doVRw)
        full_ttnh['rw_weight'+reg_label] = calc_Weight(splines, full_ttnh, rw_columns, it, doCRw=doCRw, doVRw=doVRw)

        full_data['njet_weight'+reg_label] = nJetWeight(full_data, f_res.x)
        full_tth['njet_weight'+reg_label] = nJetWeight(full_tth, f_res.x)
        full_ttnh['njet_weight'+reg_label] = nJetWeight(full_ttnh, f_res.x)

        arrs['dat2']['rw_weight'+reg_label] = calc_Weight(splines, arrs['dat2'], rw_columns, it, doCRw=doCRw, doVRw=doVRw)
        arrs['th2']['rw_weight'+reg_label] = calc_Weight(splines, arrs['th2'], rw_columns, it, doCRw=doCRw, doVRw=doVRw)
        arrs['th4']['rw_weight'+reg_label] = calc_Weight(splines, arrs['th4'], rw_columns, it, doCRw=doCRw, doVRw=doVRw)
        arrs['tnh2']['rw_weight'+reg_label] = calc_Weight(splines, arrs['tnh2'], rw_columns, it, doCRw=doCRw, doVRw=doVRw)

        arrs['dat2']['njet_weight'+reg_label] = nJetWeight(arrs['dat2'], f_res.x)
        arrs['th2']['njet_weight'+reg_label] = nJetWeight(arrs['th2'], f_res.x)
        arrs['tnh2']['njet_weight'+reg_label] = nJetWeight(arrs['tnh2'], f_res.x)

        if doFit:
            for key in keys:
                for region in regions:
                    fit_arrs[key][region]['njet_weight'+reg_label] = nJetWeight(fit_arrs[key][region], f_res.x)
                    fit_arrs[key][region]['rw_weight'+reg_label] = calc_Weight(splines, fit_arrs[key][region], rw_columns, it, doCRw=doCRw, doVRw=doVRw)


        file = open((output_dir+'info_it_%d_' + yr_short +reg_label+label+'.txt') % (it),"w") 
     
        file.write("sftnh : %.4f +/- %.4f\n" % (sftnh, sftnh_err)) 
        file.write("sfth  : %.4f +/- %.4f\n" % (sfth, sfth_err)) 
        file.write("sfqcd : %.4f +/- %.4f\n" % (sfqcd, sfqcd_err)) 
        file.write("sftnh2: %.4f +/- %.4f\n" % (sftnh2, sftnh2_err))
        file.write("f     : %.4f\n" % (f_res.x))
        file.close() 

        save_list = ['event_number', 'run_number', 'rw_weight'+reg_label, 'njet_weight'+reg_label]
        save_list_mc = save_list.copy()

        if full:
            save_list = columns.copy()
            save_list_mc = columns_mc.copy()

        dat_treeNames=[rw_tree]
        tth_treeNames=[rw_tree]
        tnh_treeNames=[rw_tree]
        if all_trees:
            dat_file_for_trees = TFile(data_file, "read")
            dat_treeNames= [key.GetName() for key in dat_file_for_trees.GetListOfKeys() if key.GetClassName() == "TTree"]
            dat_file_for_trees.Close()
     
            tth_file_for_trees = TFile(tth_file, "read")
            tth_treeNames= [key.GetName() for key in tth_file_for_trees.GetListOfKeys() if key.GetClassName() == "TTree"]
            tth_file_for_trees.Close()
      
            tnh_file_for_trees = TFile(tnh_file, "read")
            tnh_treeNames= [key.GetName() for key in tnh_file_for_trees.GetListOfKeys() if key.GetClassName() == "TTree"]
            tnh_file_for_trees.Close()


        print("About to write data weights for trees: ", dat_treeNames)
        for treeCount in range(len(dat_treeNames)):
            if treeCount == 0:
                treemode = 'recreate'
            else:
                treemode = 'update'

            treeName = dat_treeNames[treeCount]

            print("Writing tree", treeName)
            data_out = root_numpy.root2array(data_file, treename=treeName, branches=columns_load)
            if it == 0:
                dat_rw_weights[treeName] = np.ones(len(data_out))

            data_out = append_fields(data_out, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [dat_rw_weights[treeName], np.ones(len(data_out))], usemask=False)
            data_out['rw_weight'+reg_label] = calc_Weight(splines, data_out, rw_columns, it, doCRw=doCRw, doVRw=doVRw)
            dat_rw_weights[treeName] = data_out['rw_weight'+reg_label].copy()
            data_out['njet_weight'+reg_label] = nJetWeight(data_out, f_res.x)
            root_numpy.array2root(data_out[save_list], (output_dir+'dat_it_%d_'+yr_short+reg_label+label+'.root') % (it),
                                  treename=treeName, mode=treemode)

        print("About to write tth weights for trees: ", tth_treeNames)
        for treeCount in range(len(tth_treeNames)):
            if treeCount == 0:
                treemode = 'recreate'
            else:
                treemode = 'update'

            treeName = tth_treeNames[treeCount]

            print("Writing tree", treeName)
            tth_out = root_numpy.root2array(tth_file, treename=treeName, branches=columns_mc_load)

            if it == 0:
                tth_rw_weights[treeName] = np.ones(len(tth_out))

            tth_out = append_fields(tth_out, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [tth_rw_weights[treeName], np.ones(len(tth_out))], usemask=False)
            tth_out['rw_weight'+reg_label] = calc_Weight(splines, tth_out, rw_columns, it, doCRw=doCRw, doVRw=doVRw)
            tth_rw_weights[treeName] = tth_out['rw_weight'+reg_label].copy()
            tth_out['njet_weight'+reg_label] = nJetWeight(tth_out, f_res.x)
            root_numpy.array2root(tth_out[save_list_mc], (output_dir+'tth_it_%d_'+yr_short+reg_label+label+'.root') % (it),
                                  treename=treeName, mode=treemode)

        print("About to write tnh weights for trees: ", tnh_treeNames)
        for treeCount in range(len(tnh_treeNames)):
            if treeCount == 0:
                treemode = 'recreate'
            else:
                treemode = 'update'

            treeName = tnh_treeNames[treeCount]

            print("Writing tree", treeName)
            ttnh_out = root_numpy.root2array(tnh_file, treename=treeName, branches=columns_mc_load)

            if it == 0:
                ttnh_rw_weights[treeName] = np.ones(len(ttnh_out))

            ttnh_out = append_fields(ttnh_out, ['rw_weight'+reg_label, 'njet_weight'+reg_label], [ttnh_rw_weights[treeName], np.ones(len(ttnh_out))], usemask=False)
            ttnh_out['rw_weight'+reg_label] = calc_Weight(splines, ttnh_out, rw_columns, it, doCRw=doCRw, doVRw=doVRw)
            ttnh_rw_weights[treeName] = ttnh_out['rw_weight'+reg_label].copy()
            ttnh_out['njet_weight'+reg_label] = nJetWeight(ttnh_out, f_res.x)
            root_numpy.array2root(ttnh_out[save_list_mc], (output_dir+'ttnh_it_%d_'+yr_short+reg_label+label+'.root') % (it),
                                  treename=treeName, mode=treemode)

        sf_hist_file = TFile((output_dir+'info_it_%d_' + yr_short + reg_label+label+'.root') % (it),"recreate")
        sf_hist = TH1D("sf_hist_it_%d" % it, "", 4, 0, 4)
        sf_hist.GetXaxis().SetBinLabel(1, "sftnh")
        sf_hist.SetBinContent(1, sftnh)
        sf_hist.SetBinError(1, sftnh_err)

        sf_hist.GetXaxis().SetBinLabel(2, "sfth")
        sf_hist.SetBinContent(2, sfth)
        sf_hist.SetBinError(2, sfth_err)

        sf_hist.GetXaxis().SetBinLabel(3, "sfqcd")
        sf_hist.SetBinContent(3, sfqcd)
        sf_hist.SetBinError(3, sfqcd_err)

        sf_hist.GetXaxis().SetBinLabel(4, "sftnh2")
        sf_hist.SetBinContent(4, sftnh2)
        sf_hist.SetBinError(4, sftnh2_err)

        f_value = TParameter("double")("f_it_%d" % it, f_res.x)

        sf_hist.Write()
        f_value.Write()

        sf_hist_file.Close()

if __name__ == '__main__':
    driver = common("reweight.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)     
