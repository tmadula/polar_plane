def dsid_to_mass_dict():
    dsid_m = {}
  
    #scalar
    dsid_m['450248'] = 251
    dsid_m['450249'] = 260
    dsid_m['450250'] = 280
    dsid_m['450251'] = 300
    dsid_m['450252'] = 350
    dsid_m['450253'] = 400
    dsid_m['450254'] = 500
    dsid_m['450255'] = 600
    dsid_m['450256'] = 700
    dsid_m['450257'] = 800
    dsid_m['450258'] = 900

    return dsid_m
