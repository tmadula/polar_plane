# BDT Optimization Tools

We provide here a few scripts useful for optimizing. The rough procedure is to create various yaml files for configuration of the
reweighting BDT. The reweighting BDT is trained with each configuration, and a classifier BDT is used to distinguish between reweighted
and target data (here, reweighted 2 tag and 4 tag data respectively). We can thus extract a ROC area under curve (AUC) for each configuration.
Since we want the reweighted and target datasets to look the same, an AUC closer to 0.5 means that the configuration is better. We can thus
rank configurations.

An example of making a set of yaml files for optimization is shown in `make_opt_configs.py`, currently set up for randomly choosing input
variable sets. This will be expanded in future work.

The optimization is run with `runBDT_opt.py` as, e.g.
``` console
$ python runBDT_opt.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21.root -r 21 -y 16 -p /afs/cern.ch/user/s/sgasioro/private/hh4b-background-estimation/Resolved/BDTopt/configs/210519/ -j $2 -n 100 -o aucs_210519_$2.txt
```

with options given by

``` console
$ ./runBDT_opt.py -h
Usage: runBDT_opt.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUT_NAME, --out_name=OUT_NAME
                        Output AUC file name
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -y YEAR, --year=YEAR  Year
  -j JOB, --job=JOB     Condor job number
  -n N_JOBS, --n_jobs=N_JOBS
                        Total number of jobs
  -r REL, --release=REL
                        Release 20 or 21
  -p PARAM_DIR, --param_dir=PARAM_DIR
                        Read in parameters for BDT optimization. Feed in
                        directory
  -s, --save            Turn on save BDT (pickle)
```

Currently both training and classification is done in the sideband region. This is sub-ideal, and we will be cleaner about train/test sets in the future.

Playing around with these results is probably most convenient in, e.g., a notebook, but we provide an example script as a guideline, `analyze_opt_results.py`.
This is run as:

``` console
$ ./analyze_opt_results.py -i aucs/weighted_aucs_130519.txt -p configs/130519/
```

with options
``` console
$ ./analyze_opt_results.py -h
Usage: analyze_opt_results.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_AUCS, --input_aucs=INPUT_AUCS
                        File with config id's and AUCs
  -p PARAM_DIR, --param_dir=PARAM_DIR
                        Directory with config files
```


Plots comparing different configurations can be made with `compare_models.py`, run as
``` console
$ ./compare_models.py -d /eos/user/s/sgasioro/public/nano_tuples/rel21/data15_rel21.root -y 2015 -r 21 -p configs/configs_compare/
```

with options
``` console
$ ./compare_models.py -h
Usage: compare_models.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -y YEAR, --year=YEAR  Year
  -r REL, --release=REL
                        Release 20 or 21
  -p PARAM_DIR, --param_dir=PARAM_DIR
                        Directory containing param files to compare
  -s, --save            Turn on save BDT (pickle)

```
