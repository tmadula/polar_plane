#!/usr/bin/env python3

import yaml
import io

#best min 15
columns = ['njets', 'E_h1', 'eta_h2', 'E_h1_j2', 'E_h2_j1', 'eta_h2_j1', 'phi_h2_j1', 
           'm_h2_j2', 'E_h2_j2', 'phi_h2_j2', 'dRjj_2']
# Define data
data = {'columns': columns,
        'n_estimators': 50,
        'learning_rate': 0.1,
        'max_depth': 3, 
        'min_samples_leaf': 125, 
        'gb_args': {'subsample': 0.4}}

# Write YAML file
with io.open('best_min_vars.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
