#!/usr/bin/env python3

in_file_names = ["opt.2944041.%d.out" % i for i in range(100)] 

labels = []
aucs = []

for in_file_name in in_file_names:
    with open(in_file_name, "r") as f:
        for line in f:
            if line.find("sweep_210519") != -1:
                label = line[line.find("sweep_210519"):].strip()
                labels.append(label[:-5])
            if line.find("gb_weights") != -1:
                auc_str = line[line.find("gb_weights")+len("gb_weights "):]
                if auc_str:
                    aucs.append(float(auc_str))
    if len(labels) > len(aucs):
        labels = labels[:-1]     

print(labels[0], aucs[0])
print(len(labels), len(aucs))

labels.append('template')
aucs.append(0.5645)
with open("weighted_aucs_210519_16.txt", "w+") as fout:
    for i in range(len(labels)):
        fout.write(('{:>17}'.format(labels[i])+'{:>17}'.format('%.4f' % aucs[i])))
        fout.write('\n')
