from keras.models import Model
from keras.layers import Dense, Dropout, Input, BatchNormalization
import keras.backend as K

def model(in_shape, size=20, batch_norm = False):
    inputs = Input(shape=(in_shape[1],))
    hidden = Dense(size, activation='relu')(inputs)
    if batch_norm: 
        hidden = BatchNormalization()(hidden)
    hidden = Dense(size, activation='relu')(hidden)
    if batch_norm: 
        hidden = BatchNormalization()(hidden)
    hidden = Dense(size, activation='relu')(hidden)
    if batch_norm: 
        hidden = BatchNormalization()(hidden)
    outputs = Dense(1, activation='linear')(hidden)

    model = Model(inputs, outputs)

    return model

def louppe_loss(y_true, y_pred):
    # Our loss
    return (y_true * (K.sqrt(K.exp(y_pred))) + 
            (1.0 - y_true) * (1.0 / K.sqrt(K.exp(y_pred))))
