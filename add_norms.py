#!/usr/bin/env python3

import uproot
import numpy as np
import os

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-i", "--input_dir", dest="input_dir", default="",
                    help="Input directory")
parser.add_argument("-d", "--data", dest="data_file", default=[], nargs="+",
                    help="Input data filename")
parser.add_argument("--output-file", dest="output_file", default="",
                     help="Output filename")
parser.add_argument("-y", "--year", dest="year_in", default="",
                    help="Year")
parser.add_argument("--use-old-names",
                    action="store_true", dest="use_old_names", default=False,
                    help="Use SB/CR/SR naming scheme")

args = parser.parse_args()

input_dir = args.input_dir
output_file = args.output_file
data_file = args.data_file
year_in = args.year_in
use_old_names = args.use_old_names

VRlabel = '_VRderiv'
VRtree = 'validation'
CRtree = 'control'
if use_old_names:
    VRlabel = '_CRderiv'
    VRtree='control'
    CRtree='sideband'

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'
if data_file:
    for fidx in range(len(data_file)):
        input_dir_here = input_dir
        if data_file[fidx][0] == '/':
            input_dir_here = ''
        data_file[fidx] = input_dir_here+data_file[fidx]
        if data_file[fidx][-4:] != 'root':
            print("Warning! Data file not a root file")
        if data_file[fidx].find(yr_short) == -1:
            print("Warning! Input data file name may not match year.")
else:
    print("No input file given!")


for fidx in range(len(data_file)):
    print(os.path.basename(data_file[fidx]))
    in_file= uproot.open(data_file[fidx])
    if fidx == 0:
        CR = in_file[CRtree].pandas.df(['NN_d24_weight_bstrap_med_'+yr_short,'ntag'])
        VR = in_file[VRtree].pandas.df(['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short,'ntag'])
    else:
        CR = CR.append(in_file[CRtree].pandas.df(['NN_d24_weight_bstrap_med_'+yr_short,'ntag']), ignore_index=True)
        VR = VR.append(in_file[VRtree].pandas.df(['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short,'ntag']), ignore_index=True)

sf = np.sum(CR['ntag']>=4)/np.sum(CR['NN_d24_weight_bstrap_med_'+yr_short][CR['ntag']==2])
sf_CRderiv = np.sum(VR['ntag']>=4)/np.sum(VR['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short][VR['ntag']==2])
print(sf, sf_CRderiv)

from ROOT import TFile, TParameter, TObject

if output_file:
    f = TFile(output_file, "update")
else:
    f = TFile(data_file[0], "update")
norm = TParameter("double")("NN_norm_bstrap_med_"+yr_short, sf)
norm_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_med_"+yr_short, sf_CRderiv)

norm.Write("NN_norm_bstrap_med_"+yr_short, TObject.kOverwrite)
norm_VR.Write("NN_norm"+VRlabel+"_bstrap_med_"+yr_short, TObject.kOverwrite)
f.Close()
