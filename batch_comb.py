#!/usr/bin/env python3
import pandas as pd
import uproot

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

from root_numpy import root2array, array2tree
from ROOT import TFile, TParameter, TTree, TObject

import numpy as np
from functools import reduce

from tqdm import tqdm

from rw_common import *

def main(inputs):
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    data_file = inputs["data_file"]
    do_data = bool(data_file)
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    doBootstrap = inputs["doBootstrap"]
    n_resamples = inputs["n_resamples"]
    n_jobs = inputs["n_jobs"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    BDT = inputs["BDT"]
    NN = inputs["NN"]
    append = inputs["append"]
    n_it = inputs["n_it"]
    spline = inputs["spline"]
    weight_dir = inputs["weight_dir"]
    comb_label = inputs["comb_label"]
    isCRderiv = inputs["isCRderiv"]
    doMLttbar = inputs["doMLttbar"]
    weight_label = inputs["weight_label"]
    use_old_names = inputs["use_old_names"]

    VRlabel = '_VRderiv'
    VRtree = 'validation'
    CRtree = 'control'
    if use_old_names:
        VRlabel = '_CRderiv'
        VRtree = 'control'
        CRtree = 'sideband'

    if len(data_file) > 1:
        print("Combine one file at a time!")
        return 0

    data_file = data_file[0]

    yr_idx = data_file.find(yr_short)
    ext_idx = data_file.find(".root")
    flabel = data_file[yr_idx+2:ext_idx]

    d_file = uproot.open(data_file)
    w_files = []
    if BDT or NN:
        if BDT: method_label = "BDT"
        else: method_label = "NN"
        for job in range(n_jobs+(n_jobs==0)):
            n_job_label=""
            if n_jobs != 0:
                n_job_label= "_%d" % job 

            w_files.append(weight_dir+"dat_"+method_label+"_d24_"+yr_short+flabel+label+n_job_label+".root")
            if reg_label:
                w_files.append(weight_dir+"dat_"+method_label+"_d24_"+yr_short+flabel+reg_label+label+n_job_label+".root")

    treeNames = d_file.allkeys(filtername=lambda name: name, filterclass=lambda cls: issubclass(cls, uproot.tree.TTreeMethods)) 

    cols = d_file[treeNames[0]].keys()
    test = d_file[treeNames[0]].pandas.df(cols, entrystart=0,  entrystop=1)

    tree_dict = {}
    for col in cols:
        col = col.decode('utf-8')
        tree_dict[col] = str(test[col].dtype)

    tree_dict['NN_d24_weight_bstrap_med_'+yr_short] = 'float64'
    tree_dict['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short] = 'float64'
        
    norm_dict = {}
    norm_dict_VR = {}
    for w_file in w_files:
        cols_w = []
        f_for_bnames = TFile(w_file, "read")
        tree = f_for_bnames.Get(treeNames[0].decode('utf-8'))
        for branch in tree.GetListOfBranches():
            bname = branch.GetName()
            cols_w.append(bname)

        for key in f_for_bnames.GetListOfKeys():
            if key.GetName() and key.GetClassName().find('TParameter') != -1:
                name = key.GetName()
                if name.find("CR") != -1 or name.find("VR") != -1:
                    norm_dict_VR[name] = (f_for_bnames.Get(name)).GetVal()
                else:
                    norm_dict[name] = (f_for_bnames.Get(name)).GetVal()

        test_w = root2array(w_file, treename=treeNames[0], branches=cols_w, start=0,  stop=1)

        for col in cols_w:
            col_new = col
            if col.find('weight') != -1:
                col_new = col+'_'+yr_short
            tree_dict[col_new] = str(test_w[col].dtype)

 
    norms = np.array([norm_dict['NN_norm_resampling_%d'%bstrap] for bstrap in range(n_resamples)])
    med_columns = ["NN_d24_weight_resampling_%d" %bstrap for bstrap in range(n_resamples)]

    norms_VR = np.array([norm_dict_VR['NN_norm'+VRlabel+'_resampling_%d'%bstrap] for bstrap in range(n_resamples)])
    med_columns_VR = ["NN_d24_weight"+VRlabel+"_resampling_%d" %bstrap for bstrap in range(n_resamples)]


    if output_dir:
        out_name = output_dir + os.path.basename(data_file)[:-5]+'_'+comb_label+'.root'
    else:
        out_name = data_file[:-5]+'_'+comb_label+'.root'

    with uproot.recreate(out_name) as f:
        for treeName in treeNames:
            treeName = treeName.decode('utf-8')
            f[treeName] = uproot.newtree(tree_dict)
            print("Merging tree", treeName)
            tree_in_d = d_file[treeName]

            clusters = list(tree_in_d.clusters())

            for cluster in tqdm(clusters):
                data_in = tree_in_d.pandas.df(entrystart=cluster[0], entrystop=cluster[1])
                
                w_ins = [pd.DataFrame(root2array(w_file, treename=treeName, start=cluster[0], stop=cluster[1]))
                         for w_file in w_files]
                full_w_in = reduce(lambda left,right: 
                                   pd.merge(left,right,how="left", 
                                            on=["event_number", "run_number"],
                                            validate="one_to_one"), w_ins)

                full_w_in.insert(0, 'NN_d24_weight_bstrap_med', np.median(full_w_in[med_columns]*norms, axis=1))
                full_w_in.insert(0, 'NN_d24_weight'+VRlabel+'_bstrap_med', np.median(full_w_in[med_columns_VR]*norms_VR, axis=1))

                merged=pd.merge(data_in,full_w_in,how="left",on=["event_number", "run_number"],
                               validate="one_to_one")

                rename_dict = {}
                for col in list(merged.keys()):
                    if col.find('d24_weight') != -1:
                        rename_dict[col] = col+'_'+yr_short
                merged=merged.rename(columns=rename_dict)
                if merged.isna().values.any():
                    print("Warning - NaN in dataframe!!")
                f[treeName].extend(merged.to_dict(orient='list'))

    print("About to add norms")
    f_out = TFile(out_name, "update")
    for w_file in w_files:
        f_extra = TFile(w_file, "read")
        for key in f_extra.GetListOfKeys():
            if key.GetName() and key.GetClassName().find('TTree') == -1:
                extra = f_extra.Get(key.GetName())
                extra_out = extra.Clone()
                if key.GetClassName().find('TParameter') != -1:
                    outname = extra_out.GetName()+"_"+yr_short
                else:
                    outname = extra_out.GetName()
                f_out.cd()
                extra_out.Write(outname, TObject.kOverwrite)

    print("About to calculate med norm")
    out_file = uproot.open(out_name)
    CR = out_file[CRtree].arrays(['NN_d24_weight_bstrap_med_'+yr_short,'ntag'],
                                      namedecode='utf-8')
    VR = out_file[VRtree].arrays(['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short,'ntag'],
                                     namedecode='utf-8')

    sf = np.sum(CR['ntag']>=4)/np.sum(CR['NN_d24_weight_bstrap_med_'+yr_short][CR['ntag']==2])
    sf_VRderiv = np.sum(VR['ntag']>=4)/np.sum(VR['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short][VR['ntag']==2])
    print(sf, sf_VRderiv)

    norm = TParameter("double")("NN_norm_bstrap_med_"+yr_short, sf)
    norm_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_med_"+yr_short, sf_VRderiv)
    f_out.cd()
    norm.Write()
    norm_VR.Write()

if __name__ == '__main__':
    driver = common("combine_weights.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)
