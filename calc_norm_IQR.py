#!/usr/bin/env python3
import pandas as pd
import uproot
import numpy as np
from functools import reduce
import os

from scipy.stats import iqr

from tqdm import tqdm

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import TFile, TParameter, TObject

from rw_common import *

def main(inputs):
    year = inputs["year"]
    data_file = inputs["data_file"]
    yr_short = inputs["yr_short"]
    n_resamples = inputs["n_resamples"]
    n_jobs = inputs["n_jobs"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    weight_dir = inputs["weight_dir"]
    comb_label = inputs["comb_label"]
    weight_label = inputs["weight_label"]
    output_dir = inputs["output_dir"]
    use_old_names = inputs["use_old_names"]
    norm_label = inputs["norm_label"]
    output_file = inputs["output_file"]

    VRlabel = '_VRderiv'
    VRtree = 'validation'
    CRtree = 'control'
    if use_old_names:
        VRlabel = '_CRderiv'
        VRtree = 'control'
        CRtree = 'sideband'

    w_fnames_for_norm = []
    for job in range(n_jobs+(n_jobs==0)):
        n_job_label=""
        if n_jobs != 0:
            n_job_label= "_%d" % job

        w_fnames_for_norm.append(weight_dir+"dat_NN_d24_"+yr_short+norm_label+reg_label+label+n_job_label+".root")

    if not w_fnames_for_norm:
        w_fnames_for_norm = w_fnames.copy()


    print("About to look at norms")
    norms = []
    for w_fname in w_fnames_for_norm:
        print(w_fname)
        f_extra = TFile(w_fname, "read")
        for key in f_extra.GetListOfKeys():
            if key.GetName() and key.GetClassName().find('TParameter') != -1 and key.GetName().find('resampling') != -1:
                norms.append(f_extra.Get(key.GetName()).GetVal())
        f_extra.Close()

    norms_iqr = iqr(norms)
    norms_perc_25 = np.percentile(norms, 25)
    norms_perc_75 = np.percentile(norms, 75)
    norms_perc_50 = np.median(norms)
    print(norms_perc_50, norms_perc_25, norms_perc_75, norms_iqr)

    iqr_param = TParameter("double")("NN_norm_bstrap_IQR_"+yr_short, norms_iqr)
    perc25_param = TParameter("double")("NN_norm_bstrap_perc_25_"+yr_short, norms_perc_25)
    perc75_param = TParameter("double")("NN_norm_bstrap_perc_75_"+yr_short, norms_perc_75)
    perc50_param = TParameter("double")("NN_norm_bstrap_perc_50_"+yr_short, norms_perc_50)

    if output_file:
        f = TFile(output_file, "update")
    else:
        f = TFile(data_file[0], "update")
    iqr_param.Write("NN_norm_bstrap_IQR_"+yr_short, TObject.kOverwrite)
    perc25_param.Write("NN_norm_bstrap_perc_25_"+yr_short, TObject.kOverwrite)
    perc75_param.Write("NN_norm_bstrap_perc_75_"+yr_short, TObject.kOverwrite)
    perc50_param.Write("NN_norm_bstrap_perc_50_"+yr_short, TObject.kOverwrite)
    f.Close()

if __name__ == '__main__':
    driver = common("combine_weights.py")

    parser = driver.makeParser()
    parser.add_argument("--norm-label", dest="norm_label", default="",
                        help="For split data files, the label used for file with norms (e.g. a, b, c)")
    parser.add_argument("--output-file", dest="output_file", default="",
                         help="Output filename")
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    inputs["norm_label"] = args.norm_label
    inputs["output_file"] = args.output_file
    main(inputs)
