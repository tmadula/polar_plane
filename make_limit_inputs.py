#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *
from rw_common import *
from error_handling import *
from xsbook import xsLookup
from dsid_to_mass import dsid_to_mass_dict 
import uproot

import logging
mpl_logger = logging.getLogger("matplotlib")
mpl_logger.setLevel(logging.ERROR)

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_argument("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_argument("-d", "--data", dest="data_file", default=[], nargs="+",
                  help="Input data filename")
parser.add_argument("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_argument("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_argument("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_argument("--graviton", dest="graviton", default="",
                  help="Input graviton signal file template. Should have mass as mXXX")
parser.add_argument("--smnr", dest="smnr", default="",
                  help="Input SMNR signal file ")
parser.add_argument("--signal-list", dest="signal_list", default="",
                  help="Input text file with signal samples")
parser.add_argument("--nn",
                  action="store_true", dest="NN", default=False,
                  help="Do plots with NN")
parser.add_argument("--HCrescale",
                  action="store_true", dest="HCrescale", default=False,
                  help="Rescale HC 4 vecs to have m=125 GeV")
parser.add_argument("-s", "--spline",
                  dest="spline", default=-1,
                  help="Turn on spline plot, give number of the iteration. If default (=-1) don't plot.")
parser.add_argument("-l", "--label", dest="label", default="",
                  help="Label for output")
parser.add_argument("--run-shape-systs", 
                  action="store_true", dest="doShapeSysts", default=False,
                  help="Include multijet shape variations in output")
parser.add_argument("--bootstrap", dest="n_resamples", default="-1",
                    help="Turn on bootstrapping. Put in number of resamples.")
parser.add_argument("--append-sig-to-file", dest="lim_file_name", default="",
                  help="Add extra signals to file")
parser.add_argument("--bstrap_ave",
                  action="store_true", dest="bstrap_ave", default=False,
                  help="Use ave of bootstraps as nominal")
parser.add_argument("--bstrap_med",
                   action="store_true", dest="bstrap_med", default=False,
                   help="Use median of bootstraps as nominal")
parser.add_argument("--use-old-names",
                    action="store_true", dest="use_old_names", default=False,
                    help="Use SB/CR/SR naming scheme")

args = parser.parse_args()

import root_numpy
import ROOT
from ROOT import TLorentzVector, TFile, TParameter

NN = args.NN
it = int(args.spline)
spline = (it != -1)
data_file = args.data_file
tth_file = args.tth_file
tnh_file = args.tnh_file
year_in = args.year
input_dir = args.input_dir
output_dir = args.output_dir
label= args.label
grav_template = args.graviton
signal_list_file = args.signal_list
smnr_file = args.smnr
HCrescale = args.HCrescale
doShapeSysts = args.doShapeSysts
n_resamples = int(args.n_resamples)
doBootstrap = (n_resamples != -1)
lim_file_name = args.lim_file_name
bstrap_ave = args.bstrap_ave
bstrap_med = args.bstrap_med
use_old_names = args.use_old_names

skip_data=False
if lim_file_name:
    skip_data = True

if HCrescale:
    label+= "_HCrescale"
if doBootstrap:
    label+= "_bootstrap"
if bstrap_ave:
    label+="_ave"
if bstrap_med:
    label+="_med"

if label:
   if label[0] != '_':
       label = '_'+label
else:
   print("Please use a label!")


year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if smnr_file:
    if smnr_file[0] != '/':
        smnr_file = input_dir+smnr_file
    if smnr_file[-4:] != 'root':
        print("Warning! SMNR file not a root file")

grav_files = []
masses = [400, 500, 600, 700, 800, 900, 1000]
if grav_template:
    if grav_template.find('M') != -1:
        pos = grav_template.find('M')
    elif grav_template.find('m') != -1:
        pos = grav_template.find('m')
    else:
        print("Not able to find mass in grav template!")
    
    front = grav_template[:pos+1]
    back = grav_template[pos+1:]
    back = back[back.find('_'):]

    if grav_template[0] != '/':
        front=input_dir+front

    for mass in masses:
        grav_files.append(front+str(mass)+back)
else:
    masses=[]

scalar_files = []
scalar_masses = []
if signal_list_file:
    mass_dict = dsid_to_mass_dict()
    period_dict = { '2015': 'mc16a', '2016': 'mc16a', '2017': 'mc16d', '2018': 'mc16e'}
    with open(signal_list_file) as f:
        for line in f:
            if line.find("#") != -1:
                continue
            if line.find(period_dict[year]) != -1:
                if line.find("450000") != -1:
                    smnr_file=line.rstrip()

                s_idx = line.find("4502")
                if s_idx != -1:
                    scalar_files.append(line.rstrip())
                    scalar_masses.append(mass_dict[line[s_idx:s_idx+6]])

print(smnr_file)
for fidx in range(len(data_file)):
    if data_file[fidx][0] != '/':
        data_file[fidx] = input_dir+data_file[fidx]
    if data_file[fidx][-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file[fidx].find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")

if not data_file:
    if NN and not skip_data:
        print("Warning! Need to specify input data name for NN.")
        NN = False

#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['ntag', 'run_number', 'kinematic_region']

columns_mc = ['ntag', 'mc_sf', 'run_number', 'kinematic_region']

syst_vars = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

if HCrescale:
    columns+=['m_hh_cor']
    columns_mc+=['m_hh_cor']
else:
    columns+=['m_hh']
    columns_mc+=['m_hh']

if doShapeSysts:
    for var in syst_vars:
        if var not in columns:
            columns += [var]
        if var not in columns_mc:
            columns_mc += [var]

if smnr_file:
   it_smnr = root_numpy.root2array(smnr_file,
                                 treename='fullmassplane', branches=columns_mc)
   it_smnr = it_smnr[it_smnr['kinematic_region'] == 0]
   print("Loaded SMNR")

it_grav = {}
for i in range(len(grav_files)):
    it_grav[masses[i]] = root_numpy.root2array(grav_files[i],
                                 treename='fullmassplane', branches=columns_mc)

    it_grav[masses[i]] = it_grav[masses[i]][it_grav[masses[i]]['kinematic_region'] == 0]
    print("Loaded graviton, mass", masses[i])

it_scalar = {}
for i in range(len(scalar_files)):
    it_scalar[scalar_masses[i]] = root_numpy.root2array(scalar_files[i],
                                 treename='fullmassplane', branches=columns_mc)

    it_scalar[scalar_masses[i]] = it_scalar[scalar_masses[i]][it_scalar[scalar_masses[i]]['kinematic_region']==0]
    print("Loaded scalar, mass", scalar_masses[i])


if NN:
    if bstrap_ave:
        columns += ['NN_d24_weight_bstrap_ave_'+yr_short]
        if doShapeSysts:
            if use_old_names:
                columns+=['NN_d24_weight_CRderiv_bstrap_ave_'+yr_short]
            else:
                columns+=['NN_d24_weight_VRderiv_bstrap_ave_'+yr_short]
    elif bstrap_med:
         columns += ['NN_d24_weight_bstrap_med_'+yr_short]
         if doShapeSysts:
             if use_old_names:
                 columns+=['NN_d24_weight_CRderiv_bstrap_med_'+yr_short]
             else:
                 columns+=['NN_d24_weight_VRderiv_bstrap_med_'+yr_short]
    else:                       
        columns += ['NN_d24_weight_'+yr_short]
        if doShapeSysts:
            if use_old_names:
                columns+=['NN_d24_weight_CRderiv_'+yr_short]
            else:
                columns+=['NN_d24_weight_VRderiv_'+yr_short]


#Set up x labels and plotting variables
x_label_dict = {'pT_4': 'HC Jet 4 p_{T} [GeV]', 'pT_2': 'HC Jet 2 p_{T} [GeV]', 'eta_i': '<|HC #eta|>', 
                'dRjj_1': '#Delta R_{jj} Close', 'dRjj_2': '#Delta R_{jj} Not Close', 'njets' : '# of additional jets', 'm_hh' : 'm_{4j} [GeV]'}


sfs = {'nom':{}}
if doShapeSysts:
    sfs['VRw'] = {}

#Adjust verbose branchnames (with it, year) to be base ones
#columns = [vanillize_name(key) for key in it_data.dtype.names]
#it_data.dtype.names = tuple(columns)

if not skip_data:
    file_for_norm = TFile(data_file[0], "read")
    if bstrap_med:
        sfs['nom']['NN_norm']= (file_for_norm.Get("NN_norm_bstrap_med_"+yr_short)).GetVal()
        if doShapeSysts:
            if use_old_names:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_CRderiv_bstrap_med_"+yr_short)).GetVal()
            else:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_VRderiv_bstrap_med_"+yr_short)).GetVal()
    elif bstrap_ave:
        sfs['nom']['NN_norm']= (file_for_norm.Get("NN_norm_bstrap_ave_"+yr_short)).GetVal()
        if doShapeSysts:
            if use_old_names:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_CRderiv_bstrap_ave_"+yr_short)).GetVal()
            else:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_VRderiv_bstrap_ave_"+yr_short)).GetVal()
    else:
        sfs['nom']['NN_norm']= (file_for_norm.Get("NN_norm_"+yr_short)).GetVal()
        if doShapeSysts:
            if use_old_names:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_CRderiv_"+yr_short)).GetVal()
            else:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_VRderiv_"+yr_short)).GetVal()

    if doBootstrap:
        bstrap_norms = {}
        bstrap_norms['nom'] = {}
        bstrap_norms['VR'] = {}
        for bstrap in range(n_resamples):
            columns+=[('NN_d24_weight_resampling_%d_'+yr_short)%bstrap]
            bstrap_norms['nom'][bstrap]=(file_for_norm.Get(("NN_norm_resampling_%d_"+yr_short) % bstrap)).GetVal()
            if doShapeSysts:
                if use_old_names:
                    columns+=[('NN_d24_weight_CRderiv_resampling_%d_'+yr_short)%bstrap]
                    bstrap_norms['CR'][bstrap]=(file_for_norm.Get(("NN_norm_CRderiv_resampling_%d_"+yr_short) % bstrap)).GetVal()
                else:
                    columns+=[('NN_d24_weight_VRderiv_resampling_%d_'+yr_short)%bstrap]
                    bstrap_norms['VR'][bstrap]=(file_for_norm.Get(("NN_norm_VRderiv_resampling_%d_"+yr_short) % bstrap)).GetVal()
                                        
             
        sfs['bootstrap'] = bstrap_norms

    file_for_norm.Close()

    print(columns)
    for fidx in range(len(data_file)):
        file_in = uproot.open(data_file[fidx])
        if np.sum([key.decode('utf-8').find("sig") != -1 for key in file_in.keys()])>0:
            tree = "sig"
        else:
            tree = "fullmassplane"

        if fidx == 0:
            it_data = file_in[tree].pandas.df(columns)
        else:
            it_data = it_data.append(file_in[tree].pandas.df(columns), ignore_index=True)

    it_data = it_data[it_data['kinematic_region']==0]

    print("Loaded data")
 
driver = common("NN_RW.py")
arrs = {}

if not skip_data:
    arrs['dat2'], arrs['dat4'] = driver.preprocess(it_data, year)

if smnr_file:
    arrs['smnr2'], arrs['smnr4'] = driver.preprocess(it_smnr, year)

for mass in masses:
    arrs['g10_M%d_2'%mass], arrs['g10_M%d_4'%mass] = driver.preprocess(it_grav[mass], year)

for mass in scalar_masses:
    arrs['scalar_M%d_2'%mass], arrs['scalar_M%d_4'%mass] = driver.preprocess(it_scalar[mass], year)


lumis={}
lumis['15'] = 3.2
lumis['16'] = 24.6
lumis['17'] = 43.65
lumis['18'] = 58.45
lumi = (year=='2015')*lumis['15'] + (year=='2016')*lumis['16'] + (year=='2017')*lumis['17'] + (year=='2018')*lumis['18']

#For rel21 15+16, overall normalization is to combined 15+16 lumi
if year == '2015' or year == '2016':
    lumi = lumis['15']+lumis['16']

if not lumi:
    print('Warning! No lumi specified!!')

weights = {}
if smnr_file:
    weights['smnr4'] = arrs['smnr4']['mc_sf']*lumi

for mass in masses:
    arrs['g10_M%d_4'%mass]= grav[grav['ntag'] >= 4]

    weights['g10_M%d_4'%mass] = arrs['g10_M%d_4'%mass]['mc_sf']*lumi

for mass in scalar_masses:
    scalar = it_scalar[mass][(it_scalar[mass]['X_wt'] > 1.5)]
    arrs['scalar_M%d_4'%mass]= scalar[scalar['ntag'] >= 4]

    weights['scalar_M%d_4'%mass] = arrs['scalar_M%d_4'%mass]['mc_sf']*lumi

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)

if skip_data:
    all_keys = []
else:
    all_keys=['dat2']

if smnr_file:
    all_keys+= ['smnr4']
all_keys += [("g10_M%d_4" % mass) for mass in masses]
all_keys += [("scalar_M%d_4" % mass) for mass in scalar_masses]

m_hh_str = 'm_hh'
if HCrescale:
    m_hh_str = 'm_hh_cor'

if skip_data:
    lim_file = ROOT.TFile(lim_file_name, "update")
else:
    lim_file = ROOT.TFile(output_dir+"resolved_4bSR_NN"+label+"_"+year+".root", "recreate")
    
bins = np.linspace(200,1200,31)

if not skip_data:
    NNerrs=errors(arrs['dat2'], year, bins, col=m_hh_str)

    if doBootstrap and not doShapeSysts:
        hist_NN = NNerrs.runBootstrap(sfs['bootstrap']['nom'], sfs['nom']['NN_norm'], ave=bstrap_ave, med=bstrap_med)
        hist_NN.Write('NN_hh')

    if doShapeSysts:

        if doBootstrap:
            shape_systs = NNerrs.runShapeSystematics(sfs['nom']['NN_norm'], sfs['VRw']['NN_norm'],ave=bstrap_ave, med=bstrap_med, bstrap_norms=bstrap_norms, old_names=use_old_names)
        else:
            shape_systs = NNerrs.runShapeSystematics(sfs['nom']['NN_norm'], sfs['VRw']['NN_norm'],ave=bstrap_ave, med=bstrap_med, old_names=use_old_names)

        shape_systs[''].Write('NN_hh')
        shape_systs['LowHtVRw'].Write('NN_hh_LowHtVRw')
        shape_systs['HighHtVRw'].Write('NN_hh_HighHtVRw')
        shape_systs['LowHtVRi'].Write('NN_hh_LowHtVRi')
        shape_systs['HighHtVRi'].Write('NN_hh_HighHtVRi')
        shape_systs['HighHt'].Write('NN_hh_HighHt')
        shape_systs['LowHt'].Write('NN_hh_LowHt')

if smnr_file: 
    hist_smnr = Hist(bins, title='sm_hh', legendstyle='F',drawstyle='hist')
    hist_smnr.fill_array(arrs['smnr4'][m_hh_str], weights=weights['smnr4'])
    hist_smnr = makePositive_hist(hist_smnr)
    hist_smnr.Write("sm_hh", ROOT.TObject.kOverwrite)

for mass in masses:
    g_label = "g_hh_m%d_c10_4" % mass
    g_title = "g_hh_m%d_c10" % mass 
    hist_grav = Hist(bins, title=g_label, legendstyle='F',drawstyle='hist')
    hist_grav.fill_array(arrs[g_label][m_hh_str], weights=weights[g_label])
    hist_grav = makePositive_hist(hist_grav)
    hist_grav.Write(g_title, ROOT.TObject.kOverwrite)

for mass in scalar_masses:
    s_label = "scalar_M%d_4" % mass
    s_title = "s_hh_m%d" % mass
    hist_scalar = Hist(bins, title=s_label, legendstyle='F',drawstyle='hist')
    hist_scalar.fill_array(arrs[s_label][m_hh_str], weights=weights[s_label])
    hist_scalar = makePositive_hist(hist_scalar)
    hist_scalar.Write(s_title, ROOT.TObject.kOverwrite)


lim_file.Close()
