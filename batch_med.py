#!/usr/bin/env python3

import numpy as np
import os
import sys
from rw_common import *

import uproot

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--data", dest="data_file", default="",
                          help="Input data filename")
parser.add_option("-o", "--output-dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-y", "--year", dest="year_in", default="",
                  help="Year")
parser.add_option("--n-batches", dest="n_batches", default="-1",
                  help="Number of batches to split comb into")
parser.add_option("--start", dest="start", default="0",
                  help="Batch to start on")
parser.add_option("--end", dest="end", default="0",
                  help="Batch to end on")

(options, args) = parser.parse_args()

data_file = options.data_file
output_dir = options.output_dir
year_in = options.year_in
n_batches = int(options.n_batches)
start = int(options.start)
end = int(options.end)

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'
if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

from ROOT import TFile, TParameter
norms=[]
norms_CR = []
columns = []
n_bootstraps = 100
f=TFile(data_file, "read")
for bstrap in range(n_bootstraps):
    columns+=[('NN_d24_weight_resampling_%d_'+yr_short)%bstrap]
    norms.append(f.Get(('NN_norm_resampling_%d_'+yr_short)%bstrap).GetVal())
    
    columns+=[('NN_d24_weight_CRderiv_resampling_%d_'+yr_short)%bstrap]
    norms_CR.append(f.Get(('NN_norm_CRderiv_resampling_%d_'+yr_short)%bstrap).GetVal())
    
f.Close()

f = TFile(data_file,"read")
treeNames = []
for key in f.GetListOfKeys():
    if key.GetName():
        if key.GetClassName() == "TTree":
            treeNames.append(key.GetName())


in_file = uproot.open(data_file)

cols = in_file["fullmassplane"].keys()
test = in_file["fullmassplane"].pandas.df(cols, entrystart=0,  entrystop=1)

tree_dict = {}
for col in cols:
    col = col.decode('utf-8')
    tree_dict[col] = str(test[col].dtype)

tree_dict['NN_d24_weight_bstrap_med_'+yr_short] = 'float64'
tree_dict['NN_d24_weight_CRderiv_bstrap_med_'+yr_short] = 'float64'

all_resamp = [('NN_d24_weight_resampling_%d_'+yr_short)%i for i in range(100)]
all_resamp_CR = [('NN_d24_weight_CRderiv_resampling_%d_'+yr_short)%i for i in range(100)]

from tqdm import tqdm

for j in tqdm(range(start, end)):
    with uproot.recreate((output_dir+"comb"+yr_short+"_%d.root")%j) as f:
        for name in treeNames:
            nentries = in_file[name].numentries
            estart = j*nentries/n_batches*1.
            estop = (j+1)*nentries/n_batches*1.
            
            f[name] = uproot.newtree(tree_dict)
            out_dict = in_file[name].arrays(cols, namedecode='utf-8',
                                         entrystart=estart, 
                                         entrystop=estop)
            
            w_name = 'NN_d24_weight_bstrap_med_'+yr_short
            w_name_CR = 'NN_d24_weight_CRderiv_bstrap_med_'+yr_short
            
            weights = in_file[name].pandas.df(all_resamp, entrystart=estart, 
                                            entrystop=estop)*norms
            out_dict[w_name] = np.median(weights, overwrite_input =True, axis=1)
            del weights
            
            weights_CR = in_file[name].pandas.df(all_resamp_CR, entrystart=estart, 
                                            entrystop=estop)*norms_CR
            out_dict[w_name_CR] = np.median(weights_CR, overwrite_input=True, axis=1)
            del weights_CR
            
            f[name].extend(out_dict)
