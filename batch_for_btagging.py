#!/usr/bin/env python3
import subprocess
import os


filelist = os.listdir("/eos/user/s/sgasioro/private/btagging_opt_copy/")

for fname in filelist:
    if fname.find("data16") == -1:
        continue

    label = fname[fname.find('data16')+7:fname.find('.root')]
    if label in ["bdt_mv2c10_60", "bdt_mv2c10_70", "bdt_dl1r_60", "bdt_mv2c10_77", "bdt_dl1r_77"]:
        continue
    if label.find("weight") != -1:
        continue

    command = "./NN_RW.py -i /eos/user/s/sgasioro/private/btagging_opt_copy/ -d "+fname+" -y 2016 --doLog pT_2 pT_4 dRjj_1 dRjj_2 pt_hh -l "+ label + " -o /eos/user/s/sgasioro/private/btagging_opt_weights/"
    print("Running", command)
    os.system(command)

    command_cr = "./NN_RW.py -i /eos/user/s/sgasioro/private/btagging_opt_copy/ -d "+fname+" -y 2016 --doLog pT_2 pT_4 dRjj_1 dRjj_2 pt_hh -l "+ label + " --reweight-tree control -o /eos/user/s/sgasioro/private/btagging_opt_weights/"
    print("Running", command_cr)
    os.system(command_cr)

    #print(subprocess.check_output(command).decode("utf-8"))
    #subprocess.Popen(command, 
    #                 stdout=subprocess.PIPE, 
    #                 stderr=subprocess.STDOUT)
    #stdout,stderr = out.communicate()
    #print(stdout)
    #print(stderr)
