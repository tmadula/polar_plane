#!/usr/bin/env python3
import numpy as np
import os

from rw_common import *

import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

from root_numpy import root2array, array2tree
from ROOT import TFile, TParameter, TTree, TObject

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    data_file = inputs["data_file"]
    do_data = bool(data_file)
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    doBootstrap = inputs["doBootstrap"]
    n_jobs = inputs["n_jobs"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    BDT = inputs["BDT"]
    NN = inputs["NN"]
    append = inputs["append"]
    n_it = inputs["n_it"]
    spline = inputs["spline"]
    weight_dir = inputs["weight_dir"]
    comb_label = inputs["comb_label"]
    doMLttbar = inputs["doMLttbar"]
    weight_label = inputs["weight_label"]
    output_dir = inputs["output_dir"]

    if output_dir and append:
        print("Appending to file, output_dir will not be used")

    if len(data_file) > 1:
        print("Combine one file at a time!")
        return 0

    data_file = data_file[0]

    str_to_add = []
    str_target = []
    file_to_add = []
    in_file = []

    n_ftypes = do_data+do_tth+do_tnh

    data_dir = os.path.dirname(data_file)
    if data_dir:
        if data_dir[-1] != '/':
            data_dir+='/'

    if BDT or NN:
        if BDT: method_label = "BDT"
        else: method_label = "NN"
        for job in range(n_jobs+(n_jobs==0)):
            n_job_label=""
            if n_jobs != 0:
                n_job_label= "_%d" % job

            if doMLttbar:
                str_to_add+=[[method_label+"_d24_weight"+reg_label+weight_label] for _ in range(n_ftypes)]
                str_target+=[[method_label+"_d24_weight"+reg_label+weight_label] for _ in range(n_ftypes)]

            else:
                str_to_add+=[[method_label+"_d24_weight"+reg_label+weight_label]]
                str_target+=[[method_label+"_d24_weight"+reg_label+weight_label]]

            if do_data:
                file_to_add.append(weight_dir+"dat_"+method_label+"_d24_"+yr_short+reg_label+label+n_job_label+".root")
                in_file.append(data_file)
            if do_tth and doMLttbar: 
                file_to_add.append(weight_dir+"tth_"+method_label+"_d24_"+yr_short+reg_label+label+n_job_label+".root")
                in_file.append(tth_file)
            if do_tnh and doMLttbar: 
                file_to_add.append(weight_dir+"ttnh_"+method_label+"_d24_"+yr_short+reg_label+label+n_job_label+".root")
                in_file.append(tnh_file)

    if spline:
        info_outfile=TFile(data_dir+"spline_info_all"+reg_label+".root", "update")
        for it in range(n_it):
            str_to_add+=[["rw_weight"+reg_label, "njet_weight"+reg_label] for _ in range(n_ftypes)]
            str_target+=[[("rw_weight_it_%d"+reg_label) % it, ("njet_weight_it_%d"+reg_label) % it] for _ in range(n_ftypes)]

            if do_data:
                    file_to_add.append((weight_dir+"dat_it_%d_"+yr_short+reg_label+label+".root") % (it))
                    in_file.append(data_file)
            if do_tth: 
                file_to_add.append((weight_dir+"tth_it_%d_"+yr_short+reg_label+label+".root") % (it))
                in_file.append(tth_file)
            if do_tnh: 
                file_to_add.append((weight_dir+"ttnh_it_%d_"+yr_short+reg_label+label+".root") % (it))
                in_file.append(tnh_file)

            info_in_file=TFile((weight_dir+"info_it_%d_"+yr_short+reg_label+".root") % (it), "read")
            for key in info_in_file.GetListOfKeys():
                info_in = info_in_file.Get(key.GetName())
                info_out = info_in.Clone()
                info_outfile.cd()
                info_out.Write(key.GetName()+"_"+yr, TObject.kOverwrite)
            del info_in_file

    print(file_to_add)
    for i in range(len(file_to_add)):
        add_file = file_to_add[i]

        f = TFile(in_file[i],"update")
       
        treeNames = []
        objNames = []
        for key in f.GetListOfKeys():
            if key.GetName():
                objNames.append(key.GetName())
                if key.GetClassName() == "TTree":
                    treeNames.append(key.GetName())

        if doBootstrap:
            new_str_to_add = []
            f_for_bnames = TFile(add_file, "read")
            print(add_file)
            for key in f_for_bnames.GetListOfKeys():
                if key.GetName():
                    if key.GetClassName() == "TTree":
                        tree = f_for_bnames.Get(key.GetName())
                        for branch in tree.GetListOfBranches():
                            bname = branch.GetName()
                            if (bname.find("BDT_d24_weight") != -1 and BDT) or (bname.find("NN_d24_weight") != -1 and NN):
                                new_str_to_add.append(bname)
                        break

            str_to_add[i] = new_str_to_add.copy()
            str_target[i] = new_str_to_add.copy()

        out_bnames = []
        bnames_to_check = []
        out_bnames = [t+'_'+yr_short for t in str_target[i]]
        bnames_to_check = [t+'_'+yr_short for t in str_target[i]]


        for treeCount in range(len(treeNames)):
            treeName=treeNames[treeCount]
            T = f.Get(treeName)
            print(in_file[i])
            branch_list = T.GetListOfBranches()
            duplicate = False
            dup_names = []
            for obj in branch_list:
                for name in bnames_to_check:
                    if obj.GetName() == name:
                        duplicate = True
                        dup_names.append(name)
            if duplicate:
               print("Branches", dup_names, "already found in tree "+treeName+". Skipping to avoid issues")
               continue

            print("Loading", add_file)

            weights = root2array(add_file, treename=treeName, 
                                 branches=str_to_add[i]+['event_number', 'run_number'])

            print(out_bnames+['event_number', 'run_number'])
            weights.dtype.names = out_bnames+['event_number','run_number']

            print("Tree with", len(weights))

            e_run_nums = root2array(in_file[i], treename=treeName,
                                branches=['event_number','run_number'])
            e_nums = list(zip(e_run_nums['event_number'], e_run_nums['run_number']))        

            merge = np.array(np.ones(len(e_nums)),
                             dtype=weights[out_bnames].dtype) 

            idxs = np.arange(len(e_nums))
            idx_dict = {}
            for j in range(len(e_nums)):
                idx_dict[e_nums[j]] = idxs[j]

            weights_to_merge =  weights[out_bnames]
            for l in range(len(weights)):
                event_num = (weights['event_number'][l], weights['run_number'][l])
                idx = idx_dict[event_num]
                merge[idx] = weights_to_merge[l]

            array2tree(merge, tree=T)

            T.Print()
            if not append and comb_label != '':
                if in_file[i].find((comb_label+'.root')) != -1:
                    f.cd()
                    T.Write(treeName, TObject.kOverwrite)
                    if treeCount == len(treeNames)-1:
                        f_add = TFile(add_file, "read")
                        for key in f_add.GetListOfKeys():
                            if key.GetName() not in objNames and key.GetName():
                                extra = f_add.Get(key.GetName())
                                extra_out = extra.Clone()
                                if key.GetClassName().find('TParameter') != -1:
                                    outname = extra_out.GetName()+"_"+yr_short
                                else:
                                    outname = extra_out.GetName()
                                f.cd()
                                extra_out.Write(outname, TObject.kOverwrite)
                else:
                    if output_dir:
                        out_name = output_dir + os.path.basename(in_file[i])[:-5]+'_'+comb_label+'.root'
                    else:
                        out_name = in_file[i][:-5]+'_'+comb_label+'.root'

                    if treeCount==0:
                        mode = "recreate"
                    else:
                        mode = "update"
                    f_out = TFile(out_name, mode)
                    print("About to clone tree", treeName, "- this might take a sec")
                    T_out = T.CloneTree()
                    T_out.Write(treeName, TObject.kOverwrite)

                    if treeCount == len(treeNames)-1: 
                        f.cd()
                        for key in f.GetListOfKeys():
                            if key.GetClassName().find('TTree') == -1:
                                extra_obj = f.Get(key.GetName())
                                f_out.cd()
                                extra_obj_out = extra_obj.Clone()
                                extra_obj_out.Write(key.GetName(), TObject.kOverwrite)
                                del extra_obj_out
                            f.cd()


                        f_add = TFile(add_file, "read")
                        for key in f_add.GetListOfKeys():
                            if key.GetClassName().find('TParameter') != -1:
                                par = f_add.Get(key.GetName())
                                par_out = par.Clone()
                                f_out.cd()
                                par_out.Write(par_out.GetName()+"_"+yr_short, TObject.kOverwrite)

                        new_in = []
                        for name in in_file:
                            if name == in_file[i]:
                                name = out_name
                            new_in.append(name)

                        in_file = new_in

                    del f_out
            else:
                f.cd()
                T.Write(treeName, TObject.kOverwrite)
                if treeCount == len(treeNames)-1:
                    f_add = TFile(add_file, "read")
                    for key in f_add.GetListOfKeys():
                        if key.GetName() not in objNames and key.GetName():
                            extra = f_add.Get(key.GetName())
                            extra_out = extra.Clone()
                            if key.GetClassName().find('TParameter') != -1:
                                outname = extra_out.GetName()+"_"+yr_short
                            else:
                                outname = extra_out.GetName()
                            f.cd()
                            extra_out.Write(outname, TObject.kOverwrite)

        del f
     
if __name__ == '__main__':
    driver = common("combine_weights.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)

