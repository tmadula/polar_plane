#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

import os

from sklearn.preprocessing import StandardScaler

import pandas as pd
from pandas import DataFrame

from rw_utils import *
from rw_model import *
from rw_common import *

from keras.callbacks import EarlyStopping, ModelCheckpoint

import yaml
import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import root_numpy
from ROOT import TFile, TParameter

def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0-phi1) - np.pi)

def dR_hh(arr):
    return np.sqrt(dPhi(arr['phi_h1'], arr['phi_h2'])**2+(arr['eta_h1']-arr['eta_h2'])**2)

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    output_dir = inputs["output_dir"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    rw_tree = inputs["rw_tree"]
    doBootstrap = inputs["doBootstrap"]
    doNominal = inputs["doNominal"]
    n_resamples_here = inputs["n_resamples_here"]
    start = inputs["start"]
    job = inputs["job"]
    n_jobs = inputs["n_jobs"]
    full = inputs["full"]
    all_trees = inputs["all_trees"]
    param_dict = inputs["param_dict"]
    param_label = inputs["param_label"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    doLog = inputs["doLog"]
    weight_label = inputs["weight_label"]
    config = inputs["config"]

    flabel=[]
    for fname in data_file:
        yr_idx = fname.find(yr_short)
        ext_idx = fname.find(".root")
        flabel.append(fname[yr_idx+2:ext_idx])

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
               'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt', 'pt_hh', 'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2',
               'phi_h1_j1', 'phi_h1_j2', 'phi_h2_j1', 'phi_h2_j2', 'phi_h1', 'phi_h2', 'eta_h1', 'eta_h2']

    columns_mc_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
                  'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'event_number', 'X_wt', 'pt_hh']

    if 'columns' in param_dict.keys():
        for column in param_dict['columns']:
            if column not in columns_load:
                columns_load+=[column]
            if column not in columns_mc_load:
                columns_mc_load+=[column] 


    print("About to load in data...")
    print(data_file, rw_tree, year)
    for fidx in range(len(data_file)):
        if fidx == 0:
            full_data = root_numpy.root2array(data_file[fidx],
                                              treename=rw_tree, branches=columns_load)
        else:
            full_data = np.concatenate((full_data, root_numpy.root2array(data_file[fidx],
                                                              treename=rw_tree, branches=columns_load)))

    print(len(full_data[full_data['ntag']==2]))
    if do_tth:
        full_tth = root_numpy.root2array(tth_file,
                                         treename=rw_tree, branches=columns_mc_load)

    if do_tnh:
        full_ttnh = root_numpy.root2array(tnh_file,
                                          treename=rw_tree, branches=columns_mc_load)

    print("Making dPhi")
    full_data = append_fields(full_data, 'dPhi_h1', dPhi(full_data['phi_h1_j1'], full_data['phi_h1_j2']), usemask=False)
    full_data = append_fields(full_data, 'dPhi_h2', dPhi(full_data['phi_h2_j1'], full_data['phi_h2_j2']), usemask=False)
    full_data = append_fields(full_data, 'dR_hh', dR_hh(full_data), usemask=False)

    if config == "dPhi_HC_505050":
        print("Running dPhi_HC_505050")
        model_size = 50
        rw_columns =  ['pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log', 'dRjj_2_log', 
                       'njets', 'pt_hh_log', 'X_wt_log', 'dR_hh', 'dPhi_h1', 'dPhi_h2'] 
    else:
        model_size = 20
        rw_columns =  ['pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log', 'dRjj_2_log',
                       'njets', 'pt_hh_log']

    if 'columns' in param_dict.keys():
        rw_columns = param_dict['columns']

    for col in rw_columns:
        log_idx = col.find('_log')
        col_base = col[:log_idx]
        if log_idx != -1 and col_base not in doLog:
            doLog+=[col_base]

    print("Loaded! Setting up for training...")
    full_data = append_fields(full_data, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_data)), usemask=False)
    if do_tth: full_tth = append_fields(full_tth, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_tth)), usemask=False)
    if do_tnh: full_ttnh = append_fields(full_ttnh, 'NN_d24_weight'+reg_label+weight_label, np.ones(len(full_ttnh)), usemask=False)


    if doLog:
        print("Taking logs of", doLog)
        for col in doLog:
            full_data = append_fields(full_data, col+'_log', np.log(full_data[col]), usemask=False)

    #Seed for reproducibility
    np.random.seed(1000)

    #Run random so that the call here corresponds to the start'th call  
    if doBootstrap:
        for run in range(start):
            _ = np.random.poisson(1, len(full_data))

        for count in range(n_resamples_here+doNominal):
            if doNominal and count == 0:
                resamp_id = ""
            else:
                resamp_id = "%d" % (start+count-doNominal)
                poisson_weight = np.random.poisson(1, len(full_data))
                full_data = append_fields(full_data, 'poissonWeight_resamp_'+resamp_id, poisson_weight, usemask=False)


    arrs = {}
    arrs['dat2'], arrs['dat4'] = driver.preprocess(full_data, year)
    if do_tth:
        arrs['th2'], arrs['th4'] = driver.preprocess(full_tth, year)
    if do_tnh:
        arrs['tnh2'], arrs['tnh4'] = driver.preprocess(full_ttnh, year)


    print(len(arrs['dat2']), len(arrs['dat4']))
    original = DataFrame(arrs['dat2'][rw_columns])
    target = DataFrame(arrs['dat4'][rw_columns])

    X_all = pd.concat((original, target), ignore_index=True).values

    Y_all = []
    for _df, ID in [(original,1), (target, 0)]:
        Y_all.extend([ID] * _df.shape[0])
    Y_all = np.array(Y_all)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_all)
    Y_train = Y_all.copy()

    #If not splitting, need to shuffle
    print(len(X_train))
    idxs = np.random.permutation(len(X_train))
    X_train = X_train[idxs]
    Y_train = Y_train[idxs]

    #Defaults
    print("About to train...")
    print("Training on columns:", rw_columns)


    reweighter_dict = {}
    for count in range(n_resamples_here+doNominal):
        if doNominal and count == 0:
            resamp_id = ""
        else:
            resamp_id = "%d" % (start+count-doNominal)

        reweighter_dict[resamp_id] = model(X_train.shape, size=model_size)
        reweighter_dict[resamp_id].compile(loss=louppe_loss, metrics=['accuracy'], optimizer="adam")
        reweighter_dict[resamp_id].summary()

        if resamp_id:
            print("Resampling id:", resamp_id)
            original_weights = arrs['dat2']['poissonWeight_resamp_'+resamp_id]
            target_weights = arrs['dat4']['poissonWeight_resamp_'+resamp_id]
        else:
            original_weights = np.ones(len(arrs['dat2']))
            target_weights = np.ones(len(arrs['dat4']))

        poisson_weights = np.concatenate((original_weights, target_weights))[idxs]

        history = reweighter_dict[resamp_id].fit(X_train, Y_train, sample_weight = poisson_weights,
                            callbacks = [
                                EarlyStopping(monitor='val_loss', patience=10, verbose=True),
                                    ModelCheckpoint('NN_'+resamp_id+'.h5', monitor='val_loss', verbose=True, save_best_only=True) ],
                                    epochs=80,
                                    validation_split = 0.4,
                                    batch_size=8192,
                           )

        #Setting weights to checkpointed best
        reweighter_dict[resamp_id].load_weights('NN_'+resamp_id+'.h5')

    treeNames=[rw_tree]
    if all_trees:
        f = TFile(data_file[0], "read")
        treeNames= [key.GetName() for key in f.GetListOfKeys() if key.GetClassName() == "TTree" and key.GetName()]
        f.Close()

    save_list = ['event_number', 'run_number']
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            save_list += ['NN_d24_weight'+reg_label+weight_label+resamp_label]
        else:
            save_list += ['NN_d24_weight'+reg_label+weight_label]
    save_list_mc = save_list.copy()

    if full:
        save_list = columns_load.copy()
        save_list_mc = columns_mc_load.copy()


    print("About to write weights for trees: ", treeNames)
    if len(data_file) > 1:
        print("Writing for each input data file separately")
        
    for treeCount in range(len(treeNames)):
        if treeCount == 0:
            treemode = 'recreate'
        else:
            treemode = 'update'
     
        treeName = treeNames[treeCount]

        for fidx in range(len(data_file)):
            print("Writing tree", treeName, "for file", os.path.basename(data_file[fidx]))
            data_out = root_numpy.root2array(data_file[fidx], treename=treeName, branches=columns_load)

            print("Making dPhi ")
            data_out = append_fields(data_out, 'dPhi_h1', dPhi(data_out['phi_h1_j1'], data_out['phi_h1_j2']), usemask=False)
            data_out = append_fields(data_out, 'dPhi_h2', dPhi(data_out['phi_h2_j1'], data_out['phi_h2_j2']), usemask=False)
            data_out = append_fields(data_out, 'dR_hh', dR_hh(data_out), usemask=False)

            if doLog:
                print("Taking logs for output")
                for col in doLog:
                    data_out = append_fields(data_out, col+'_log', np.log(data_out[col]), usemask=False)

            for resamp_id in reweighter_dict.keys():
                if resamp_id:
                    resamp_label = "_resampling_"+resamp_id
                else:
                    resamp_label = ""
                data_out = append_fields(data_out, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(data_out)), usemask=False)
                isdat2 = data_out['ntag'] == 2
                pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(data_out[isdat2][rw_columns]).values), batch_size=1024)
                data_out['NN_d24_weight'+reg_label+weight_label+resamp_label][isdat2] = np.exp(pred)[:,0]
            root_numpy.array2root(data_out[save_list], output_dir+'dat_NN_d24_'+yr_short+flabel[fidx]+reg_label+label+'.root',
                                  treename=treeName, mode=treemode)

        if do_tth:
            tth_out = root_numpy.root2array(tth_file, treename=treeName, branches=columns_mc_load)

            for resamp_id in reweighter_dict.keys():
                if resamp_id:
                    resamp_label = "_resampling_"+resamp_id
                else:
                    resamp_label = ""
                tth_out = append_fields(tth_out, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(tth_out)), usemask=False)
                istth2 = tth_out['ntag'] == 2
                pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(tth_out[istth2][rw_columns]).values), batch_size=1024)
                tth_out['NN_d24_weight'+reg_label+weight_label+resamp_label][istth2] = np.exp(pred)[:,0]
            root_numpy.array2root(tth_out[save_list_mc], output_dir+'tth_NN_d24_'+yr_short+reg_label+label+'.root', 
                                  treename=treeName, mode=treemode)

        if do_tnh:
            ttnh_out = root_numpy.root2array(tnh_file, treename=treeName, branches=columns_mc_load)

            for resamp_id in reweighter_dict.keys():
                if resamp_id:
                    resamp_label = "_resampling_"+resamp_id
                else:
                    resamp_label = ""
                ttnh_out = append_fields(ttnh_out, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(ttnh_out)), usemask=False)
                isttnh2 = ttnh_out['ntag'] == 2
                pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(ttnh_out[isttnh2][rw_columns]).values), batch_size=1024) 
                ttnh_out['NN_d24_weight'+reg_label+weight_label+resamp_label][isttnh2] = np.exp(pred)[:,0]
            root_numpy.array2root(ttnh_out[save_list_mc], output_dir+'ttnh_NN_d24_'+yr_short+reg_label+label+'.root', 
                                  treename=treeName, mode=treemode)

    isdat2 = full_data['ntag']==2
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            original_weights = full_data['poissonWeight_resamp_'+resamp_id]
            full_data = append_fields(full_data, 'NN_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(full_data)), usemask=False)
        else:
            resamp_label = ""
            original_weights = np.ones(len(full_data))

        pred = reweighter_dict[resamp_id].predict(scaler.transform(DataFrame(full_data[isdat2][rw_columns]).values), batch_size=1024)
        full_data['NN_d24_weight'+reg_label+weight_label+resamp_label][isdat2] = np.exp(pred)[:, 0]
        norm_factor = 1.*np.sum(original_weights[full_data['ntag']>=4])/np.sum(original_weights[full_data['ntag']==2]*full_data[full_data['ntag']==2]['NN_d24_weight'+reg_label+weight_label+resamp_label])

        norm = TParameter("double")("NN_norm"+reg_label+weight_label+resamp_label, norm_factor)
        f = TFile(output_dir+'dat_NN_d24_'+yr_short+flabel[0]+reg_label+label+'.root', "update")
        norm.Write()
        f.Close()

if __name__ == '__main__':
    driver = common("NN_RW.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)
