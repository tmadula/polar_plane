import numpy as np
from rootpy.plotting import Hist
from numpy.lib.recfunctions import append_fields

class errors:
    def __init__(self, bkgd_arr, year, bins, col='m_hh'):
        self.bkgd_arr = bkgd_arr
        self.year = year
        self.yr_short = year[-2:]
        self.bins = bins
        self.col = col

    def runBootstrapIQR(self, NN_norm_nom, IQRsf=1., ave=False, med=False, isCR=False, isVR=False, usearr=[]):
        if len(usearr) > 0:
            arr = usearr.copy()
        else:
            arr = self.bkgd_arr
        bins = self.bins
        yr_short = self.yr_short
        column = self.col

        if ave:
            ave_label='bstrap_ave_'
        elif med:
            ave_label='bstrap_med_'
        else:
            ave_label=''

        hist_qcd = Hist(bins, title='NN_hh', legendstyle='F',drawstyle='hist')
        hist_qcd_p_IQR = Hist(bins, title='NN_hh', legendstyle='F',drawstyle='hist')

        if isCR:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_CRderiv_'+ave_label+yr_short])
            hist_qcd_p_IQR.fill_array(arr[column], weights=(arr['NN_d24_weight_CRderiv_'+ave_label+yr_short]+
                                                            arr['NN_d24_weight_CRderiv_bstrap_IQR_'+yr_short]/2.))
        elif isVR:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_VRderiv_'+ave_label+yr_short])
            hist_qcd_p_IQR.fill_array(arr[column], weights=(arr['NN_d24_weight_VRderiv_'+ave_label+yr_short]+
                                                            arr['NN_d24_weight_VRderiv_bstrap_IQR_'+yr_short]/2.))
        else:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_'+ave_label+yr_short])
            hist_qcd_p_IQR.fill_array(arr[column], weights=(arr['NN_d24_weight_'+ave_label+yr_short]+
                                                            arr['NN_d24_weight_bstrap_IQR_'+yr_short]/2.))

        hist_qcd.Scale(NN_norm_nom)
        tot_nom = hist_qcd.Integral()
        tot_p = hist_qcd_p_IQR.Integral()

        hist_err = hist_qcd.Clone("hist_med")
        hist_qcd_p_IQR.Scale(tot_nom/tot_p)
        hist_err.Scale(0.5*IQRsf-1.)
        hist_err.Add(hist_qcd_p_IQR)

        for binx in range(1, hist_qcd.GetNbinsX()+1):
            poisson_err = hist_qcd.GetBinError(binx)
            bstrap_err = hist_err.GetBinContent(binx)
            tot_error=np.sqrt(bstrap_err**2+poisson_err**2)
            hist_qcd.SetBinError(binx, tot_error)
 
        return hist_qcd

    #Run bootstrap errors
    def runBootstrap(self, NN_norms, NN_norm_nom, n_bootstraps=-1, ave=False, med=False, isCR=False, isVR=False, usearr=[]):
        if len(usearr) > 0:
            arr = usearr.copy()
        else:
            arr = self.bkgd_arr
        bins = self.bins
        yr_short = self.yr_short
        column = self.col

        if ave:
            ave_label='bstrap_ave_'
        elif med:
            ave_label='bstrap_med_'
        else:
            ave_label=''

        if n_bootstraps == -1:
            n_bootstraps = 0
            if isinstance(arr, np.ndarray):
                colnames = arr.dtype.names
            else:
                colnames = arr.keys()

            for colname in colnames:
                if isCR:
                    if colname.find("resampling") != -1 and colname.find("CRderiv") != -1:
                        n_bootstraps+=1
                elif isVR:
                    if colname.find("resampling") != -1 and colname.find("VRderiv") != -1:
                        n_bootstraps+=1
                else:
                    if colname.find("resampling") != -1 and colname.find("CRderiv") == -1 and colname.find("VRderiv") == -1:
                        n_bootstraps+=1
                        
        hists_for_std = []
        for bstrap in range(n_bootstraps):
            hist_for_bstrap = Hist(bins)
            if isCR:
                hist_for_bstrap.fill_array(arr[column], weights=arr[('NN_d24_weight_CRderiv_resampling_%d_'+yr_short)%bstrap])
            elif isVR:
                hist_for_bstrap.fill_array(arr[column], weights=arr[('NN_d24_weight_VRderiv_resampling_%d_'+yr_short)%bstrap])
            else:
                hist_for_bstrap.fill_array(arr[column], weights=arr[('NN_d24_weight_resampling_%d_'+yr_short)%bstrap])

            hist_for_bstrap.Scale(NN_norms[bstrap])
            hists_for_std.append(hist_for_bstrap)

        bstrap_errs = []
        for binx in range(1, hists_for_std[0].GetNbinsX()+1):
            bin_vals = []
            for hist in hists_for_std:
                bin_vals.append(hist.GetBinContent(binx))
            bstrap_errs.append(np.std(bin_vals))


        hist_qcd = Hist(bins, title='NN_hh', legendstyle='F',drawstyle='hist')
        if isCR:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_CRderiv_'+ave_label+yr_short])
        elif isVR:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_VRderiv_'+ave_label+yr_short])
        else:
            hist_qcd.fill_array(arr[column], weights=arr['NN_d24_weight_'+ave_label+yr_short])

        hist_qcd.Scale(NN_norm_nom)

        for binx in range(1, hist_qcd.GetNbinsX()+1):
            poisson_err = hist_qcd.GetBinError(binx)
            bstrap_err = bstrap_errs[binx-1]
            tot_error=np.sqrt(bstrap_err**2+poisson_err**2)
            hist_qcd.SetBinError(binx, tot_error)

        return hist_qcd

    def runShapeSystematics(self, NN_norm, NN_norm_VRderiv, ave=False, med=False, bstrap_norms={}, old_names=False):
        arr = self.bkgd_arr
        bins = self.bins
        yr_short = self.yr_short

        if ave:
            ave_label='bstrap_ave_'
        elif med:
            ave_label='bstrap_med_'
        else:
            ave_label=''

        reg_arrs = {}

        dataHT = arr['pT_h1_j1'] + arr['pT_h1_j2'] + arr['pT_h2_j1'] + arr['pT_h2_j2']
        reg_list = ['', 'LowHt', 'HighHt', 'LowHtVRw', 'HighHtVRw']
 
        reg_arrs['']        = arr.copy() 
        reg_arrs['LowHt']   = arr[dataHT < 300]
        reg_arrs['HighHt']  = arr[dataHT >= 300]
        reg_arrs['LowHtVRw']   = reg_arrs['LowHt'].copy()
        reg_arrs['HighHtVRw']  = reg_arrs['HighHt'].copy()

        store_for_symm = {}
        store_for_add = {}
        for reg in reg_list:
            NN_weight_label = 'NN_d24_weight_'+ave_label+yr_short
            if reg.find('VRw') != -1:
                if old_names:
                    NN_weight_label = 'NN_d24_weight_CRderiv_'+ave_label+yr_short
                else:
                    NN_weight_label = 'NN_d24_weight_VRderiv_'+ave_label+yr_short

            if bstrap_norms:
                if reg.find("VRw") != -1:
                    hist_qcd = self.runBootstrap(bstrap_norms['VR'], NN_norm_VRderiv, ave=ave, med=med, isCR=old_names, isVR=(not old_names), usearr=reg_arrs[reg])
                else:
                    hist_qcd = self.runBootstrap(bstrap_norms['nom'], NN_norm, ave=ave, med=med, usearr=reg_arrs[reg])
 
            else:
                hist_qcd = Hist(bins, title='NN_hh', legendstyle='F',drawstyle='hist')
                hist_qcd.fill_array(reg_arrs[reg][self.col], weights=reg_arrs[reg][NN_weight_label])

                if reg.find("VRw") != -1:
                    hist_qcd.Scale(NN_norm_VRderiv)
                else:
                    hist_qcd.Scale(NN_norm)

            store_for_symm[reg] = hist_qcd.Clone()
            store_for_add[reg] = hist_qcd.Clone()


        store_for_symm['LowHt'].Scale(2.)
        store_for_symm['LowHt'].Add(store_for_symm['LowHtVRw'],-1)
        store_for_add['LowHtVRi'] = store_for_symm['LowHt'].Clone()

        store_for_symm['HighHt'].Scale(2.)
        store_for_symm['HighHt'].Add(store_for_symm['HighHtVRw'],-1)
        store_for_add['HighHtVRi'] = store_for_symm['HighHt'].Clone()

        store_for_add['LowHtVRw'].Add(store_for_add['HighHt'])
        store_for_add['HighHtVRw'].Add(store_for_add['LowHt'])

        store_for_add['LowHtVRi'].Add(store_for_add['HighHt'])
        store_for_add['HighHtVRi'].Add(store_for_add['LowHt']) 

        return store_for_add
