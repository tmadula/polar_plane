#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

from error_handling import *

from rw_utils import *
from rw_common import *

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import uproot
from ROOT import TFile, TLorentzVector
from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)

import logging
mpl_logger = logging.getLogger("matplotlib")
mpl_logger.setLevel(logging.ERROR)

def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0-phi1) - np.pi)

def dR_hh(arr):
    return np.sqrt(dPhi(arr['phi_h1'], arr['phi_h2'])**2+(arr['eta_h1']-arr['eta_h2'])**2)

def main(inputs):
    BDT = inputs["BDT"]
    NN = inputs["NN"]
    n_it = inputs["n_it"]
    data_file = inputs["data_file"]
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    output_dir = inputs["output_dir"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    tree = inputs["tree"]
    doCRw = inputs["doCRw"]
    doVRw = inputs["doVRw"]
    HCrescale = inputs["HCrescale"]
    bstrap_ave = inputs["bstrap_ave"]
    bstrap_med = inputs["bstrap_med"]
    n_resamples = inputs["n_resamples"]
    doBootstrap = inputs["doBootstrap"] 
    doShapeSysts = inputs["doShapeSysts"]
    doPulls = inputs["doPulls"]
    NNT_tag = inputs["NNT_tag"]
    use_old_names = inputs["use_old_names"]
    doIQRbstrap = inputs["doIQRbstrap"]
    kinematic_region = inputs["kinematic_region"]
    zoomed = inputs["zoomed"]
    overlay_norw = inputs["overlay_norw"]

    overlay_label = ""
    if overlay_norw:
        overlay_label = "_overlay"

    if (doCRw or doVRw) and doShapeSysts:
        print("Shape systs on VR derived model don't make sense!")
        return 0

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
               'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt', 'pt_hh',
               'pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2',
               'eta_h1_j1', 'eta_h1_j2', 'eta_h2_j1', 'eta_h2_j2',
               'phi_h1_j1', 'phi_h1_j2', 'phi_h2_j1', 'phi_h2_j2', 'phi_h1', 'phi_h2', 'eta_h1', 'eta_h2', 'pT_h1', 'pT_h2',
               'pairing_score_1', 'pairing_score_2', 'm_hh', 'm_hh_cor', 'dEta_hh']

    if kinematic_region:
        columns += ['kinematic_region']

    bkgd_weight_name = ""
    if BDT: 
        columns += ['BDT_d24_weight'+reg_label+'_'+yr_short]
        bkgd_weight_name = 'BDT_d24_weight'+reg_label+'_'+yr_short
    if NN:
        if bstrap_ave:
            columns+=['NN_d24_weight'+reg_label+'_bstrap_ave_'+yr_short]
            bkgd_weight_name = 'NN_d24_weight'+reg_label+'_bstrap_ave_'+yr_short
            if doShapeSysts:
                if use_old_names:
                    columns+=['NN_d24_weight_CRderiv_bstrap_ave_'+yr_short]
                else:
                    columns+=['NN_d24_weight_VRderiv_bstrap_ave_'+yr_short]
        elif bstrap_med:
            columns+=['NN_d24_weight'+reg_label+'_bstrap_med_'+yr_short]
            bkgd_weight_name = 'NN_d24_weight'+reg_label+'_bstrap_med_'+yr_short
            if doShapeSysts:
                if use_old_names:
                    columns+=['NN_d24_weight_CRderiv_bstrap_med_'+yr_short]
                else:
                    columns+=['NN_d24_weight_VRderiv_bstrap_med_'+yr_short]
            if doIQRbstrap:
                columns+=['NN_d24_weight_bstrap_IQR_'+yr_short]

        else:
            columns += ['NN_d24_weight'+reg_label+'_'+yr_short]
            bkgd_weight_name = 'NN_d24_weight'+reg_label+'_'+yr_short
            if doShapeSysts:
                if use_old_names:
                    columns+=['NN_d24_weight_CRderiv_'+yr_short]
                else:
                    columns+=['NN_d24_weight_VRderiv_'+yr_short]

    syst_vars = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

    if doShapeSysts:
        for var in syst_vars:
            if var not in columns:
                columns += [var]

    #Set up x labels and plotting variables
    x_label_dict = {'pT_1': 'HC Jet 1 p_{T} [GeV]', 'pT_2': 'HC Jet 2 p_{T} [GeV]', 'pT_3': 'HC Jet 3 p_{T} [GeV]', 'pT_4': 'HC Jet 4 p_{T} [GeV]',
                    'eta_1': 'HC Jet 1 #eta', 'eta_2': 'HC Jet 2 #eta', 'eta_3': 'HC Jet 3 #eta', 'eta_4': 'HC Jet 4 #eta',
                    'logpT_4': 'HC Jet 4 log(p_{T})', 'logpT_2': 'HC Jet 2 log(p_{T})', 'eta_i': '<|HC #eta|>', 
                    'dRjj_1': '#Delta R_{jj} Close', 'dRjj_2': '#Delta R_{jj} Not Close', 'njets' : 'Number of jets', 'm_hh' : 'm_{HH} [GeV]',
                    'pt_hh': 'HH p_{T} [GeV]', 'logpt_hh': 'HH log(p_{T})', 'm_h1' : 'm_{H}^{lead} [GeV]', 'm_h2' : 'm_{H}^{subl} [GeV]', 
                    'X_wt' : 'X_{Wt}', 'pT_h1' : 'Leading HC p_{T} [GeV]', 'pT_h2' : 'Subleading HC p_{T} [GeV]',
                    'dEta_hh': '#Delta #eta_{HH}', 'dR_hh' : '#Delta R_{HH}', 'dPhi_h1': 'Lead HC #Delta #phi_{jj}', 'dPhi_h2': 'Sublead HC #Delta #phi_{jj}',
                    'pairing_score_1': 'BDT Pairing Score 1', 'pairing_score_2' : 'BDT Pairing Score 2', 'm_hh_cor': 'm_{HH} (corrected) [GeV]'}




    sfs = {'nom':{}}
    if doShapeSysts:
        sfs['VRw'] = {}

    file_for_norm = TFile(data_file[0], "read")
    if bstrap_ave or bstrap_med:
        sfs['nom']['NN_norm']= (file_for_norm.Get("NN_norm_bstrap_med_"+yr_short)).GetVal()
        if doShapeSysts:
            if use_old_names:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_CRderiv_bstrap_med_"+yr_short)).GetVal()
            else:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_VRderiv_bstrap_med_"+yr_short)).GetVal()
        if doIQRbstrap:
            sfs['IQR'] = (file_for_norm.Get("NN_norm_bstrap_IQR_"+yr_short)).GetVal()
    else:
        sfs['nom']['NN_norm']= (file_for_norm.Get("NN_norm_"+yr_short)).GetVal()
        if doShapeSysts:
            if use_old_names:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_CRderiv_"+yr_short)).GetVal()
            else:
                sfs['VRw']['NN_norm'] = (file_for_norm.Get("NN_norm_VRderiv_"+yr_short)).GetVal()
    if doBootstrap:
        bstrap_norms = {}
        bstrap_norms['nom'] = {}
        bstrap_norms['VR'] = {}
        for bstrap in range(n_resamples):
            columns+=[('NN_d24_weight'+reg_label+'_resampling_%d_'+yr_short)%bstrap]
            bstrap_norms['nom'][bstrap]=(file_for_norm.Get(("NN_norm"+reg_label+"_resampling_%d_"+yr_short) % bstrap)).GetVal()
            if doShapeSysts:
                if use_old_names:
                    columns+=[('NN_d24_weight_CRderiv_resampling_%d_'+yr_short)%bstrap]
                    bstrap_norms['VR'][bstrap]=(file_for_norm.Get(("NN_norm_CRderiv_resampling_%d_"+yr_short) % bstrap)).GetVal()
                else:
                    columns+=[('NN_d24_weight_VRderiv_resampling_%d_'+yr_short)%bstrap]
                    bstrap_norms['VR'][bstrap]=(file_for_norm.Get(("NN_norm_VRderiv_resampling_%d_"+yr_short) % bstrap)).GetVal()

        sfs['bootstrap'] = bstrap_norms

    file_for_norm.Close()

    print(columns)
    for fidx in range(len(data_file)):
        file_in = uproot.open(data_file[fidx])
        if fidx == 0:
            it_data = file_in[tree].pandas.df(columns)
        else:
            it_data = it_data.append(file_in[tree].pandas.df(columns), ignore_index=True)

    arrs = {}
    hists = {}
    weights = {}
    fit_counts = {}

    it_data.insert(0, 'logpT_4', np.log(it_data['pT_4']), False)
    it_data.insert(0, 'logpT_2', np.log(it_data['pT_2']), False)
    it_data.insert(0, 'logpt_hh', np.log(it_data['pt_hh']), False)

    it_data.insert(0, 'dPhi_h1', dPhi(it_data['phi_h1_j1'], it_data['phi_h1_j2']), False)
    it_data.insert(0, 'dPhi_h2', dPhi(it_data['phi_h2_j1'], it_data['phi_h2_j2']), False)
    it_data.insert(0, 'dR_hh', dR_hh(it_data), False)

    pt_cols = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']
    eta_cols = ['eta_h1_j1', 'eta_h1_j2', 'eta_h2_j1', 'eta_h2_j2']
   # phi_cols = ['phi_h1_j1', 'phi_h1_j2', 'phi_h2_j1', 'phi_h2_j2']
   # m_cols = ['m_h1_j1', 'm_h1_j2', 'm_h2_j1', 'm_h2_j2']

    sorted_pt_cols = np.argsort(-it_data[pt_cols].values,axis=1)

    pTs = it_data[pt_cols].values
    etas = it_data[eta_cols].values
   # phis = it_data[eta_cols].values
   # ms = it_data[m_cols].values

    sorted_pTs = []
    sorted_etas = []
    #sorted_phis = []
    #sorted_ms = []
    for i in range(len(sorted_pt_cols)):
        sorted_pTs.append(pTs[i][sorted_pt_cols[i]])
        sorted_etas.append(etas[i][sorted_pt_cols[i]])
    #    sorted_phis.append(phis[i][sorted_pt_cols[i]])
    #    sorted_ms.append(ms[i][sorted_pt_cols[i]])

    sorted_pTs = np.array(sorted_pTs)
    sorted_etas = np.array(sorted_etas)
   # sorted_phis = np.array(sorted_phis)
   # sorted_ms = np.array(sorted_ms)

    it_data.insert(0, 'pT_1', sorted_pTs[:, 0], False)
    it_data.insert(0, 'pT_3', sorted_pTs[:, 2], False)
    it_data.insert(0, 'eta_1', sorted_etas[:,0], False)
    it_data.insert(0, 'eta_2', sorted_etas[:,1], False)
    it_data.insert(0, 'eta_3', sorted_etas[:,2], False)
    it_data.insert(0, 'eta_4', sorted_etas[:,3], False)

    if kinematic_region:
        if use_old_names:
            kr_dict = {"signal" : 0, "control" : 1, "sideband" : 2}
        else:
            kr_dict = {"signal" : 0, "validation" : 1, "control" : 2}

        it_data = it_data[(it_data["kinematic_region"] == kr_dict[kinematic_region])]

    #data = it_data[it_data['X_wt'] > 1.5]
    #it_data = it_data[it_data['dEta_hh'] < 1.5]
    arrs['dat2']= it_data[it_data['ntag'] == 2]
    arrs['dat4']= it_data[it_data['ntag'] >= 4]

    all_keys = ['dat2', 'dat4']
    m_hh_str = 'm_hh'
    if HCrescale:
        m_hh_str = 'm_hh_cor'

    columns_to_plot =  ['pT_1', 'pT_2', 'pT_3', 'pT_4', 'eta_1', 'eta_2', 'eta_3', 'eta_4', 
                        'eta_i', 'dRjj_1', 'dRjj_2', 'pt_hh', 'njets','m_hh', 
                        'm_hh_cor', 'm_h1', 'm_h2', 'pT_h1', 'pT_h2',
                        'logpT_4', 'logpT_2', 'logpt_hh', 'X_wt', 'dEta_hh', 'dPhi_h1', 
                        'dPhi_h2', 'dR_hh', 'pairing_score_1', 'pairing_score_2']
    #columns_to_plot = ['dEta_hh', 'X_wt']

    methods = []
    if BDT:
        methods.append('BDT')
        f_for_norm = TFile(data_file[0], "read")
        BDTnorm_par = f_for_norm.Get("BDT_norm"+reg_label+"_"+yr_short)
        BDTnorm = BDTnorm_par.GetVal()
        f_for_norm.Close()
    if NN:
        methods.append('NN')
        f_for_norm = TFile(data_file[0], "read")
        if bstrap_med:
            NNnorm_par = f_for_norm.Get("NN_norm"+reg_label+"_bstrap_med_"+yr_short)
            NNnorm = NNnorm_par.GetVal()
        elif bstrap_ave:
            NNnorm_par = f_for_norm.Get("NN_norm"+reg_label+"_bstrap_ave_"+yr_short)
            NNnorm = NNnorm_par.GetVal()
        else:
            NNnorm_par = f_for_norm.Get("NN_norm"+reg_label+"_"+yr_short)
            NNnorm = NNnorm_par.GetVal()
        f_for_norm.Close()

    for method in methods:
        for column in columns_to_plot:
            xlim = np.percentile(np.hstack([arrs['dat4'][column]]), [0.01, 99.99])
            width = xlim[1]-xlim[0]
            xlim[0] -= width*0.1
            xlim[1] += width*0.1

            n_bins = 50
            if column == 'njets':
                n_bins = 8
                xlim = [3.5,11.5]
            if column.find("m_hh") != -1:
                n_bins= 30
                xlim = [200,1200]
                #n_bins=66
                #xlim=[100,1400]
            if column == 'X_wt' and tree != "everything":
                xlim[0] = 1.5

            bins = np.linspace(xlim[0], xlim[1], n_bins+1)

            hist_dat_4tag = Hist(bins, title='4b Data', legendstyle='PE')
            hist_dat_4tag.fill_array(arrs['dat4'][column])

            if method == 'BDT':
                 hist_qcd = Hist(bins, title='QCD', legendstyle='F',drawstyle='hist')
                 if label.find('norw') != -1:
                     hist_qcd.fill_array(arrs['dat2'][column])
                 else:
                     hist_qcd.fill_array(arrs['dat2'][column], weights=arrs['dat2'][bkgd_weight_name])

                 hist_qcd.Scale(BDTnorm)
                # hist_qcd.Scale(hist_dat_4tag.Integral()/hist_qcd.Integral())

            if method == 'NN':
                 NNerrs=errors(arrs['dat2'], year, bins, col=column)

                 hist_qcd = Hist(bins, title='QCD', legendstyle='F',drawstyle='hist')
                 if label.find('norw') != -1:
                     print("Plotting with no reweighting!")
                     hist_qcd.fill_array(arrs['dat2'][column])
                     hist_qcd.Scale(hist_dat_4tag.Integral()/hist_qcd.Integral())
                 else:
                     hist_qcd.fill_array(arrs['dat2'][column], weights=arrs['dat2'][bkgd_weight_name])
                     hist_qcd.Scale(NNnorm)

                 if overlay_norw:
                     hist_qcd_norw = Hist(bins, title='QCD', legendstyle='F',drawstyle='hist')
                     print("Plotting overlay!")
                     hist_qcd_norw.fill_array(arrs['dat2'][column])
                     hist_qcd_norw.Scale(hist_dat_4tag.Integral()/hist_qcd_norw.Integral())

                 if doBootstrap: 
                     hist_qcd_for_err = NNerrs.runBootstrap(sfs['bootstrap']['nom'], sfs['nom']['NN_norm'], ave=bstrap_ave, med=bstrap_med, isCR=doCRw, isVR=doVRw)
                 elif doIQRbstrap:
                     #print(sfs['IQR'])
                     hist_qcd_for_err = NNerrs.runBootstrapIQR(sfs['nom']['NN_norm'], IQRsf=sfs['IQR'], ave=bstrap_ave, med=bstrap_med, isCR=doCRw, isVR=doVRw)

                 if doShapeSysts:
                     if doIQRbstrap:
                         bstrap_norms = {}
                     hist_shape = NNerrs.runShapeSystematics(sfs['nom']['NN_norm'], sfs['VRw']['NN_norm'],ave=bstrap_ave, med=bstrap_med, bstrap_norms=bstrap_norms, old_names=use_old_names)

            hist_qcd.SetFillColor(5)
            hist_qcd.SetFillStyle('solid')
            hist_qcd.SetLineColor(1)

            if method == 'BDT' or method == 'NN':
                 stack = HistStack(hists=[hist_qcd])

            stack.SetMaximum(hist_dat_4tag.GetMaximum()*1.4)
            hist_dat_4tag.SetMarkerStyle(20)
            hist_dat_4tag.SetMarkerSize(0.6)
            hist_dat_4tag.SetMarkerColor(1)
            hist_dat_4tag.SetLineColor(1)

            if overlay_norw:
                hist_qcd_norw.SetLineStyle(2)
                hist_qcd_norw.SetLineWidth(3)
                hist_qcd_norw.SetLineColor(ROOT.kBlue)


            if not doBootstrap and not doIQRbstrap:
                hist_qcd_for_err = stack.GetStack().Last().Clone("hist_qcd_for_err")

            if doShapeSysts:
                LowHt_for_err = hist_shape['LowHtVRw'].Clone("LowHt_for_err")
                HighHt_for_err = hist_shape['HighHtVRw'].Clone("HighHt_for_err")

                LowHt_for_err.Add(hist_qcd_for_err, -1)
                HighHt_for_err.Add(hist_qcd_for_err, -1)

                for binx in range(1, hist_qcd_for_err.GetNbinsX()+1):
                    low_err = abs(LowHt_for_err.GetBinContent(binx))
                    high_err = abs(HighHt_for_err.GetBinContent(binx))
                    syst_err = np.sqrt(low_err**2 + high_err**2)
                    stat_err = hist_qcd_for_err.GetBinError(binx)

                    hist_qcd_for_err.SetBinError(binx, np.sqrt(syst_err**2+stat_err**2))


            hist_qcd_for_err.SetFillColor(ROOT.kGray+2)
            hist_qcd_for_err.SetFillStyle(3245)
            hist_qcd_for_err.SetMarkerSize(0)
            hist_qcd_for_err.legendstyle = "FE2"

            canvas = Canvas()
            canvas.cd()
            pad1 = Pad(0.0,0.33,1.0,1.0)
            pad2 = Pad(0.0,0.0,1.0,0.33)
            pad1.SetBottomMargin(0.015)
            pad1.SetTopMargin(0.05)
            pad2.SetTopMargin(0.05)
            pad2.SetBottomMargin(0.3)
            pad1.Draw()
            pad2.Draw()
            pad1.SetTicks(1, 1)


            pad1.cd()
            if method == 'BDT':
                if label.find('norw') != -1:
                    hist_qcd.SetTitle("")
                else:
                    hist_qcd.SetTitle("BDT Reweighted 2b Data")

            if method == 'NN':
                if label.find('norw') !=-1:
                    hist_qcd.SetTitle("Normalized 2b Data")
                else:
                    if doCRw or doVRw:
                        hist_qcd.SetTitle("VR Reweighted 2b Data")
                    else:
                        hist_qcd.SetTitle("Reweighted 2b Data")

            if doShapeSysts:
                hist_qcd_for_err.SetTitle("Stat+Syst Error")
            else:
                hist_qcd_for_err.SetTitle("Stat Error")
            hist_qcd_for_err.legendstyle = "FE2"
            hist_qcd_for_err.drawstyle = "E2"
            hist_qcd_for_err.SetLineColor(ROOT.kWhite)

            if overlay_norw:
                hist_qcd_norw.SetTitle("Normalized 2b Data")
                legend = Legend([hist_qcd_norw, hist_qcd, hist_dat_4tag, hist_qcd_for_err], textsize=0.075, leftmargin=0.36, entrysep=0.1)
            else:
                legend = Legend([hist_qcd, hist_dat_4tag, hist_qcd_for_err], textsize=0.075, leftmargin=0.36, entrysep=0.1)

            stack.Draw()
            hist_qcd_for_err.Draw("same e2")
            hist_dat_4tag.Draw("same pe")
            if overlay_norw:
                hist_qcd_norw.Draw("hist same")

            stack.GetXaxis().SetLabelSize(0)
            stack.GetYaxis().SetLabelSize(0.038)

            legend.SetBorderSize(0)
            legend.SetFillStyle(0)
            legend.Draw()
           
            l = ROOT.TLatex()
            l.SetNDC()
            l.SetTextColor(ROOT.kBlack)
            l.SetTextSize(0.08)
            l.DrawLatex(0.15, 0.84, "#it{ATLAS} #bf{Internal}")
            l.Draw()

            l2 = ROOT.TLatex()
            l2.SetNDC()
            l2.SetTextColor(ROOT.kBlack)
            l2.SetTextSize(0.05)

            lumi={}
            lumi['15'] = 3.2
            lumi['16'] = 24.6
            lumi['17'] = 43.65
            lumi['18'] = 58.45

            l2.DrawLatex(0.15, 0.775, ("#bf{#sqrt{s} = 13 TeV, "+year+" %.1f fb^{-1}}") % (lumi[yr_short]))
            l2.Draw()

            l3 = ROOT.TLatex()
            l3.SetNDC()
            l3.SetTextColor(ROOT.kBlack)
            l3.SetTextSize(0.05)

            if use_old_names:
                reg_title = {"sideband" : "Control", "control" : "Validation", "everything": "Everything"}
            else:
                reg_title = {"control" : "Control", "validation" : "Validation", "everything": "Everything"}
            l3.DrawLatex(0.15, 0.71, "#bf{Resolved "+reg_title[tree]+" Region}")
            l3.Draw()

            tag = ROOT.TLatex()
            tag.SetNDC()
            tag.SetTextColor(ROOT.kBlack)
            tag.SetTextSize(0.04)
            tag.DrawLatex(0.75, 0.96, "#bf{"+NNT_tag+"}")
            tag.Draw()

            pad2.cd()
            ROOT.gStyle.SetOptStat(0)
            errorless_back = stack.GetStack().Last().Clone("errorless")
            for binx in range(1, errorless_back.GetNbinsX()):
                errorless_back.SetBinError(binx,0)

            if doPulls:
                rat_dat_pred = run_pulls(hist_dat_4tag, hist_qcd_for_err)
                rat_dat_pred.drawstyle = "PE"
                rat_dat_pred.GetYaxis().SetRangeUser(-3, 3)
                rat_dat_pred.GetYaxis().SetTitle("Significance")
                mark_pos = 0

            else:
                rat_dat_pred = hist_dat_4tag.Clone("rat_dat_pred")
                rat_dat_pred.Divide(errorless_back)
                #rat_dat_pred.Divide(stack.GetStack().Last())
                if zoomed:
                    rat_dat_pred.GetYaxis().SetRangeUser(0.75, 1.25)
                else:
                    rat_dat_pred.GetYaxis().SetRangeUser(0.5, 1.5)
                rat_dat_pred.GetYaxis().SetTitle("4b / 2b")
                mark_pos = 1

            rat_dat_pred.GetYaxis().SetNdivisions(503)
            rat_dat_pred.GetXaxis().SetLabelSize(0.1)
            rat_dat_pred.GetYaxis().SetLabelSize(0.1)
            rat_dat_pred.GetYaxis().SetLabelOffset(0.03)
            rat_dat_pred.GetYaxis().CenterTitle();
            rat_dat_pred.GetXaxis().SetTitle(x_label_dict[column])
            rat_dat_pred.SetTitle('')
        
            rat_dat_pred.GetYaxis().SetTitleSize(0.12)
            #rat_dat_pred.GetXaxis().SetTitleSize(0.1)
            rat_dat_pred.GetXaxis().SetTitleSize(0.15)
            rat_dat_pred.GetYaxis().SetTitleOffset(0.35)
            #rat_dat_pred.GetXaxis().SetTitleOffset(1.1)
            rat_dat_pred.GetXaxis().SetTitleOffset(0.8)

            rat_dat_pred.SetMarkerStyle(20)
            rat_dat_pred.SetMarkerSize(0.6)
            rat_dat_pred.SetMarkerColor(1)
            rat_dat_pred.SetLineColor(1)
            rat_dat_pred.Draw()

            if not doPulls:
                rat_err = hist_qcd_for_err.Clone("rat_err")
                rat_err.Divide(errorless_back)
                rat_err.Draw("same e2")

            if overlay_norw:
                if doPulls:
                    rat_norw = run_pulls(hist_dat_4tag, hist_qcd_norw)
                else: 
                    rat_norw = hist_dat_4tag.Clone("rat_norw")
                    rat_norw.Divide(hist_qcd_norw)
                    rat_norw.SetMarkerStyle(20)
                    rat_norw.SetMarkerSize(0.6)
                    rat_norw.SetMarkerColor(ROOT.kBlue)
                    rat_norw.SetLineColor(ROOT.kBlue)
                    rat_norw.Draw("same pe")

            line = ROOT.TLine(rat_dat_pred.GetXaxis().GetXmin(), mark_pos, rat_dat_pred.GetXaxis().GetXmax(), mark_pos)
            line.SetLineColor(1)
            line.SetLineWidth(1)
            line.SetLineStyle(1)
            line.Draw()

            canvas.Draw()
     
            kr_label = ""
            if kinematic_region:
                kr_label = '_'+kinematic_region
            if method == 'BDT':
                output_fname = NNT_tag+'_'+column+label+'_'+reg_title[tree]+kr_label+reg_label+overlay_label+'_BDT_'+yr_short+'.pdf'
                canvas.SaveAs(output_dir+output_fname.replace('_', '-'))
            if method == 'NN':
                output_fname = NNT_tag+'_'+column+label+'_'+reg_title[tree]+kr_label+reg_label+overlay_label+'_NN_'+yr_short+'.pdf'
                canvas.SaveAs(output_dir+output_fname.replace('_', '-'))

                if column.find("m_hh") != -1 and doPulls:
                    rat_dat_pred.SaveAs(output_dir+NNT_tag+'_'+column+'_pullhist_'+reg_title[tree]+reg_label+'_NN_'+yr_short+'.root')
                    hist_dat_4tag.SaveAs(output_dir+NNT_tag+'_'+column+'_4tag_hist_'+reg_title[tree]+reg_label+'_NN_'+yr_short+'.root')
                    hist_qcd_for_err.SaveAs(output_dir+NNT_tag+'_'+column+'_bkgd_hist_'+reg_title[tree]+reg_label+'_NN_'+yr_short+'.root')

if __name__ == '__main__':
    driver = common("make_rw_plots.py")

    parser = driver.makeParser()
    parser.add_argument("--bstrap-IQR",
                         action="store_true", dest="doIQRbstrap", default=False,
                        help="Use IQR bootstrap")
    parser.add_argument("--zoomed",
                        action="store_true", dest="zoomed", default=False,
                        help="Zoom in on ratio")
    parser.add_argument("--overlay-norw",
                        action="store_true", dest="overlay_norw", default=False,
                        help="Overlay plot before reweighting")

    args = parser.parse_args()
    inputs = driver.interpretArgs(args)
    inputs["doIQRbstrap"] = args.doIQRbstrap
    inputs["zoomed"] = args.zoomed
    inputs["overlay_norw"] = args.overlay_norw

    main(inputs)
                                        
