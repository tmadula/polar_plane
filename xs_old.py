def old_scalar_xs(mass):
    val = 1
    mass = int(mass)
    print("Getting xs for mass", mass, "GeV")

    if mass==260:  val=150.550003052*1e3*0.5824**2
    if mass==270:  val=190.000*1e3*0.5824**2
    if mass==280:  val=208.550*1e3*0.5824**2
    if mass==290:  val=217.400*1e3*0.5824**2
    if mass==300:  val=221.699996948*1e3*0.5824**2
    if mass==400:  val=229.600006104*1e3*0.5824**2
    if mass==500:  val=99.0*1e3*0.5824**2
    if mass==600:  val=39.0*1e3*0.5824**2
    if mass==700:  val=16.0*1e3*0.5824**2
    if mass==800:  val=6.90000009537*1e3*0.5824**2
    if mass==900:  val=3.20000004768*1e3*0.5824**2
    if mass==1000: val=1.60000002384*1e3*0.5824**2
    if mass==1100: val=0.800000011921*1e3*0.5824**2
    if mass==1200: val=0.423000007868*1e3*0.5824**2
    if mass==1300: val=0.232999995351*1e3*0.5824**2
    if mass==1400: val=0.131999999285*1e3*0.5824**2
    if mass==1500: val=0.0769999995828*1e3*0.5824**2
    if mass==1600: val=0.0461000017822*1e3*0.5824**2
    if mass==1800: val=0.0175000000745*1e3*0.5824**2
    if mass==2000: val=1000*1000*0.5824*0.5824* 7.1e-06
    if mass==2250: val=1000*1000*0.5824*0.5824* 2.49e-06
    if mass==2500: val=1000*1000*0.5824*0.5824* 1.0e-06
    if mass==2750: val=1000*1000*0.5824*0.5824* 0.372e-06
    if mass==3000: val=1000*1000*0.5824*0.5824* 0.155e-06

    print(val)
    return val




