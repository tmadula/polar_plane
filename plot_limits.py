#!/usr/bin/env python3

import numpy as np
from rw_utils import *
from xs_old import old_scalar_xs
from xsbook import xsLookup
import matplotlib.pyplot as plt
import os

from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument("-i", "--input_dirs", dest="input_dirs", default="", nargs="+",
                  help="Input directory or list of directories")
parser.add_argument("--comp-labels", dest="comp_labels", default="", nargs="+",
                    help ="Label(s) for comparison. List in order of input_dirs if more than one.")
parser.add_argument("-l", "--label", dest="label", default="",
                  help="Label")

args = parser.parse_args()

input_dirs = args.input_dirs
comp_labels = args.comp_labels
label = args.label

masses_str = {}
exp = {}
m_2s = {}
m_1s = {}
p_1s = {}
p_2s = {}
nom_xs = {}
masses = {}

comp_out_label = ""
for count in range(len(input_dirs)):
    in_dir = input_dirs[count]
    comp_label = comp_labels[count]
    comp_out_label+=("_"+comp_label)

    if in_dir.find("HCrescale") != -1:
        label += '_HCrescale'

    if in_dir.find("Shape") != -1:
        label+='_ShapeSyst'

    if in_dir.find("NoSyst") != -1:
        label+="_StatOnly"

    if in_dir.find("AllSyst") != -1:
        label+="_AllSyst"

    if label and label[0] != '_':
        label = '_' + label

    if in_dir[-1] != '/':
        in_dir+='/'
            
    year=''
    yr_short=''

    isg10 = False
    isScalar = False
    isHH = False

    if in_dir.find("g_hh_c10") != -1: 
        print("Comparing limits for c=1.0 graviton")
        isg10 = True
    if in_dir.find("sm_hh") != -1:
        print("Comparing limits for SM HH")
        isHH = True
    if in_dir.find("s_hh") != -1:
        print("Comparing limits for scalar")
        isScalar = True

    if in_dir.find("BDT") != -1:
        back_label = "_BDT"
        print("BDT background")
    elif in_dir.find("NN") != -1:
        back_label = "_NN"
        print("NN background")
    else:
        back_label="_spline"
        print("Spline background")

    if in_dir.find("2016") != -1: 
        print("Year is 2016")
        year = '2016'
        yr_short = '16'

    if in_dir.find("2017") != -1:
        print("Year is 2017")
        year = '2017'
        yr_short = '17'

    if in_dir.find("2018") != -1:
        print("Year is 2018")
        year = '2018'
        yr_short = '18'

    if in_dir.find("combined") != -1:
        print("Combined years")
        year = 'combined'
        yr_short = 'combined'

    if (not isg10 and not isHH and not isScalar) or (year==''):
        print("Can't plot! Check inputs")


    lims = {}
    masses_str[comp_label] = []

    import ROOT
    for file in os.listdir(in_dir):
         f = ROOT.TFile(in_dir+file)
         lim_hist = f.Get("limit")

         mass = file[:-5]
         masses_str[comp_label].append(mass)
         lims[mass] = {'obs': [], 'exp' : [], '2s_u' : [], '1s_u' : [],'1s_l' : [],'2s_l' : []}

         if mass == "-999":
             #xs = xsLookup("SMNR", 20, with_init=False)
             xs = 1.
         else:
             if isg10:
                 xs = xsLookup("g10_M"+mass, 21, with_init=False)
             elif isScalar:
                 xs = xsLookup("scalar_M"+mass, 21, with_init=False)
             else:
                 xs = 1.
         
         for i in range(len(lims[mass].keys())):
             key = list(lims[mass].keys())[i]
             lims[mass][key] = lim_hist.GetBinContent(i+1)*xs


    exp[comp_label] = []
    m_2s[comp_label] = []
    m_1s[comp_label] = []
    p_1s[comp_label] = []
    p_2s[comp_label] = []

    masses_str[comp_label].sort(key=lambda x: int(x))

    nom_xs[comp_label] = []

    masses[comp_label] = []

    for mass in masses_str[comp_label]:
        if mass == "-999":
            print("SMNR")
            print('{:^15}'.format('Exp Limit (SM xs)'), '{:^15}'.format('-2 sigma'), '{:^15}'.format('-1 sigma'), '{:^15}'.format('+1 sigma'), '{:^15}'.format('+2 sigma'))
            print('{:^15}'.format('%.2f' % lims[mass]['exp']), '{:^15}'.format('%.2f' % lims[mass]['2s_l']), '{:^15}'.format('%.2f' % lims[mass]['1s_l']),
                                 '{:^15}'.format('%.2f' % lims[mass]['1s_u']), '{:^15}'.format('%.2f' % lims[mass]['2s_u']))

            print("")
            xs = xsLookup("SMNR", 20, with_init=False)
            print("Using xs = %.2f fb" % xs)
            print('{:^15}'.format('Exp Limit (SM xs)'), '{:^15}'.format('-2 sigma'), '{:^15}'.format('-1 sigma'), '{:^15}'.format('+1 sigma'), '{:^15}'.format('+2 sigma'))
            print('{:^15}'.format('%.2f' % (lims[mass]['exp']*xs)), '{:^15}'.format('%.2f' % (lims[mass]['2s_l']*xs)), 
                                     '{:^15}'.format('%.2f' % (lims[mass]['1s_l']*xs)),
                                     '{:^15}'.format('%.2f' % (lims[mass]['1s_u']*xs)), '{:^15}'.format('%.2f' % (lims[mass]['2s_u']*xs)))

            sig_label = "SMNR" 
        else:
            if isg10:
                sig_label = "g10_M"+mass
            elif isScalar:
                sig_label = "scalar_M"+mass
            else:
                sig_label = ""

        exp[comp_label].append(lims[mass]['exp'])
        m_2s[comp_label].append(lims[mass]['2s_l'])
        m_1s[comp_label].append(lims[mass]['1s_l'])
        p_1s[comp_label].append(lims[mass]['1s_u'])
        p_2s[comp_label].append(lims[mass]['2s_u'])


        masses[comp_label].append(int(mass))

        print(sig_label)
        nom_xs[comp_label].append(xsLookup(sig_label, 21, with_init=False))

import matplotlib.font_manager as font_manager

font_dirs = ['/eos/user/s/sgasioro/SWAN_projects/freesans/', ]
font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
font_list = font_manager.createFontList(font_files)
font_manager.fontManager.ttflist.extend(font_list)

if len(masses[comp_labels[0]]) > 0:
    from rootpy.plotting.style import get_style, set_style
    from matplotlib.ticker import AutoMinorLocator, MultipleLocator

    set_style('ATLAS', mpl=True)

    import logging
    logging.getLogger('matplotlib').setLevel(logging.ERROR)

    from matplotlib import rcParams
    rcParams['mathtext.fontset'] = 'custom'

    if masses[comp_labels[0]][0] == -999 and len(masses[comp_labels[0]]) == 1:
        fig, ax = plt.subplots(figsize=(6,6))

        for b in range(len(comp_labels)):
            comp_label = comp_labels[b]
            
            if b == 0:
                ax.fill_between([-1000,-998], m_2s*2, p_2s*2, color='yellow', label='Expected $\pm$ 2$\sigma$')
                ax.fill_between([-1000,-998], m_1s*2, p_1s*2, color='chartreuse', label='Expected $\pm$ 1$\sigma$')
                ax.plot([-1000,-998], exp*2, 'k--', label='Expected Limit (95% CL)')
            else:
                ax.plot([-1000,-998], exp*2, '--', label='Expected Limit (95% CL)')

        ax.plot([-1000,-998], [1,1], 'r-', label='SM HH',
                linewidth=2)
        ax.set_xticks(masses[comp_labels[0]])
        ax.set_xticklabels(['SM HH'], fontsize=18)

        ax.legend()
        ax.set_yscale('log')
        ax.axis([-1000.5, -997.5, 0.8, 1500.])
        ax.set_ylabel(r'$\sigma(pp\rightarrow H^{*} \rightarrow HH \rightarrow b\overline{b}b\overline{b})$ [SM xs]',
                      horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')
 
        fig.tight_layout()
        fig.savefig('SMNR_limit'+label+back_label+'_'+year+'SMxs.pdf')


        #Scale by xs now
        xs = xsLookup("SMNR", 20, with_init=False)
        exp = [a*xs for a in exp]
        m_2s = [a*xs for a in m_2s]
        m_1s = [a*xs for a in m_1s]
        p_1s = [a*xs for a in p_1s]
        p_2s = [a*xs for a in p_2s]

        fig, ax = plt.subplots(figsize=(6,6))

        ax.fill_between([-1000,-998], m_2s*2, p_2s*2, color='yellow', label='Expected $\pm$ 2$\sigma$')
        ax.fill_between([-1000,-998], m_1s*2, p_1s*2, color='chartreuse', label='Expected $\pm$ 1$\sigma$')
        ax.plot([-1000,-998], exp*2, 'k--', label='Expected Limit (95% CL)')

        ax.plot([-1000,-998], nom_xs*2, 'r-', label='SM HH',
                  linewidth=2)
        ax.set_xticks(masses)
        ax.set_xticklabels(['SM HH'], fontsize=18)

        ax.legend()
        ax.set_yscale('log')
        ax.axis([-1000.5, -997.5, 0.8, 15000.])
        ax.set_ylabel(r'$\sigma(pp\rightarrow H^{*} \rightarrow HH \rightarrow b\overline{b}b\overline{b})$ [fb]',
                       horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')

       # ax.text(0.33, 0.2, label[1:]+", "+back_label[1:]+" "+year,
        #        verticalalignment='center', horizontalalignment='center',
        #        transform=ax.transAxes, fontsize=18)


        fig.tight_layout()
        fig.savefig('SMNR_limit'+label+back_label+'_'+year+'.pdf')


    else:
        if isg10:
            plot_label ='Bulk RS, k/$\overline{M}_{Pl} = 1.0$'
            x_label = 'm$_{G_{KK}^{*}}$ [GeV]'
            y_label = r'$\sigma(pp\rightarrow G_{KK}^{*} \rightarrow$ HH $\rightarrow b\overline{b}b\overline{b})$ [fb]'
            out_name='GKK_limit'+label+back_label+'_'+year+comp_out_label+'.pdf'
            y_range = [0.8, 15000.]
        if isScalar:
            x_label = 'm(Scalar) [GeV]'
            y_label = r'$\sigma(pp\rightarrow$ Scalar $\rightarrow$ HH $\rightarrow b\overline{b}b\overline{b})$ [fb]'
            out_name='scalar_limit'+label+back_label+'_'+year+comp_out_label+'.pdf'
            y_range = [0.8, 15000.]

        fig, ax = plt.subplots()

        palette = plt.get_cmap('Set1')

        for b in range(len(comp_labels)):
            comp_label = comp_labels[b]
            if b == 0:
                ax.fill_between(masses[comp_label], m_2s[comp_label], p_2s[comp_label], color='yellow', label='Expected $\pm$ 2$\sigma$')
                ax.fill_between(masses[comp_label], m_1s[comp_label], p_1s[comp_label], color='chartreuse', label='Expected $\pm$ 1$\sigma$')
                ax.plot(masses[comp_label], exp[comp_label], 'k--', label='Expected Limit (95% CL, '+comp_label+')')

                for idx in range(len(masses[comp_label])):
                    print(masses[comp_label][idx], m_2s[comp_label][idx], m_1s[comp_label][idx], exp[comp_label][idx], p_1s[comp_label][idx], p_2s[comp_label][idx])

            else:
                ax.plot(masses[comp_label], exp[comp_label], color =palette(b), linestyle= '--', label='Expected Limit ('+comp_label+')')
      
        if isg10:
            ax.plot(masses[comp_label], nom_xs[comp_label], 'r-', label=plot_label,
                    linewidth=2)
        ax.legend()
        ax.set_yscale('log')
        ax.axis([masses[comp_labels[0]][0]-50, masses[comp_labels[0]][-1]+50, y_range[0], y_range[1]])
        ax.set_xlabel(x_label, horizontalalignment='right', x=1.0)
        ax.set_ylabel(y_label,
               horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')
        ax.xaxis.set_minor_locator(AutoMinorLocator())

        #ax.text(0.33, 0.2, label[1:]+", "+back_label[1:]+" " +year,
        #        verticalalignment='center', horizontalalignment='center',
        #        transform=ax.transAxes, fontsize=18)

        fig.savefig(out_name)

