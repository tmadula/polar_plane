#!/usr/bin/env python3
import pandas as pd
import uproot
import numpy as np
from functools import reduce
import os

from scipy.stats import iqr

from tqdm import tqdm

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import TFile, TParameter, TObject

from rw_common import *

def main(inputs):
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    do_data = bool(data_file)
    do_tth = bool(tth_file)
    do_tnh = bool(tnh_file)
    doBootstrap = inputs["doBootstrap"]
    n_resamples = inputs["n_resamples"]
    n_jobs = inputs["n_jobs"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    weight_dir = inputs["weight_dir"]
    norm_dir = inputs["norm_dir"]
    comb_label = inputs["comb_label"]
    weight_label = inputs["weight_label"]
    output_dir = inputs["output_dir"]
    use_old_names = inputs["use_old_names"]
    save_all_bootstraps = inputs["save_all_bootstraps"]
    start = inputs["start"]
    end = inputs["end"]
    norm_label = inputs["norm_label"]
    nom_only = inputs["nom_only"]
    alt_only = inputs["alt_only"]
    out_tree_list = inputs["out_tree_list"]
    doVBF = inputs["doVBF"]

    doBatch = False
    batch_label = ""
    if start != -1 and end != -1:
        doBatch = True
        print("Running in batches, here from cluster %d to %d for each tree" % (start, end))
        batch_label = "_%d_to_%d" % (start, end)

    VRlabel = '_VRderiv'
    VRtree = 'validation'
    CRtree = 'control'
    if use_old_names:
        VRlabel = '_CRderiv'
        VRtree = 'control'
        CRtree = 'sideband'

    if len(data_file) > 1 or (do_data+do_tth+do_tnh) > 1:
        print("Combine one file at a time!")
        return 0

    if do_data:
        fname = data_file[0]
        prefix = "dat"
    elif do_tth:
        fname = tth_file
        prefix = "tth"
    elif do_tnh:
        fname = tnh_file
        prefix = "ttnh"
    else:
        print("No input files given!")
        return 0

    yr_idx = fname.find(yr_short)
    ext_idx = fname.find(".root")
    flabel = fname[yr_idx+2:ext_idx]

    if len(flabel) > 1:
        print("Non-standard naming. If trying to split data files, use e.g. dataXXa.root, dataXXb.root,.... Otherwise, ignore this warning")
        flabel = ""

    if flabel and not norm_label:
        print("Warning! Empty norm label for split data files. You may want to check inputs")

    #Collect the weight files
    d_file = uproot.open(fname)
    w_files = []
    w_fnames = []
    w_fnames_for_norm = []
    for job in range(n_jobs+(n_jobs==0)):
        n_job_label=""
        if n_jobs != 0:
            n_job_label= "_%d" % job 

        if not alt_only:
            w_files.append(uproot.open(weight_dir+prefix+"_NN_d24_"+yr_short+flabel+label+n_job_label+".root"))
            w_fnames.append(weight_dir+prefix+"_NN_d24_"+yr_short+flabel+label+n_job_label+".root")
            if flabel or do_tth or do_tnh:
                w_fnames_for_norm.append(norm_dir+"dat_NN_d24_"+yr_short+norm_label+label+n_job_label+".root")

        if not nom_only:
            w_files.append(uproot.open(weight_dir+prefix+"_NN_d24_"+yr_short+flabel+VRlabel+label+n_job_label+".root"))
            w_fnames.append(weight_dir+prefix+"_NN_d24_"+yr_short+flabel+VRlabel+label+n_job_label+".root")
            if flabel or do_tth or do_tnh:
                w_fnames_for_norm.append(norm_dir+"dat_NN_d24_"+yr_short+norm_label+VRlabel+label+n_job_label+".root")

    if not w_fnames_for_norm:
        w_fnames_for_norm = w_fnames.copy()

    #Grab tree names
    treeNames = d_file.allkeys(filtername=lambda name: name, filterclass=lambda cls: issubclass(cls, uproot.tree.TTreeMethods))
    check_w_treeNames = w_files[0].allkeys(filtername=lambda name: name, filterclass=lambda cls: issubclass(cls, uproot.tree.TTreeMethods))

    if treeNames != check_w_treeNames:
        print("Mismatch in input trees and weight trees. Combining the intersection of the two lists")
        treeNames = list(set(treeNames) & set(check_w_treeNames))

    #Remove cycle numbers to prevent mismatch
    for n_idx in range(len(treeNames)):
        treeNames[n_idx] = treeNames[n_idx].decode('utf-8')

        cyc_idx = treeNames[n_idx].find(';')
        if cyc_idx != -1:
            treeNames[n_idx] = treeNames[n_idx][:cyc_idx]

    if out_tree_list:
        print("Of remaining trees, only combine selected ones")
        treeNames = list(set(treeNames) & set(out_tree_list))

    print(treeNames)

    #Load in single event to get output structure (branches, dtypes)
    cols = d_file[treeNames[0]].keys()
    test = d_file[treeNames[0]].pandas.df(cols, entrystart=0,  entrystop=1)

    #Put output structure in tree dict
    tree_dict = {}
    for col in cols:
        col = col.decode('utf-8')
        tree_dict[col] = str(test[col].dtype)

    #Always save median and IQR (nominal est)
    if not alt_only:
        if doBootstrap:
            tree_dict['NN_d24_weight_bstrap_med_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight_bstrap_perc_25_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight_bstrap_perc_75_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight_bstrap_IQR_'+yr_short] = 'float64'
        else:
            tree_dict['NN_d24_weight_'+yr_short] = 'float64'

    if not nom_only:
        if doBootstrap:
            tree_dict['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight'+VRlabel+'_bstrap_perc_25_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight'+VRlabel+'_bstrap_perc_75_'+yr_short] = 'float64'
            tree_dict['NN_d24_weight'+VRlabel+'_bstrap_IQR_'+yr_short] = 'float64'
        else:
            tree_dict['NN_d24_weight'+VRlabel+'_'+yr_short] = 'float64'
   
    
    norm_dict = {}
    norm_dict_VR = {}
    for idx in range(len(w_files)):
        w_file = w_files[idx]
        cols_w = w_file[treeNames[0]].keys()
        test_w = w_file[treeNames[0]].pandas.df(cols_w, entrystart=0,  entrystop=1)
   
        f_for_norms = TFile(w_fnames_for_norm[idx], "read")
        for key in f_for_norms.GetListOfKeys():
            if key.GetName() and key.GetClassName().find('TParameter') != -1:
                name = key.GetName()
                if name.find("CR") != -1 or name.find("VR") != -1:
                    norm_dict_VR[name] = (f_for_norms.Get(name)).GetVal()
                else:
                    norm_dict[name] = (f_for_norms.Get(name)).GetVal()

        if save_all_bootstraps:
            for col in cols_w:
                col = col.decode('utf-8')
                col_new = col
                if col.find('weight') != -1:
                    col_new = col+'_'+yr_short
                tree_dict[col_new] = str(test_w[col].dtype)

    if doBootstrap and not alt_only:
        norms = np.array([norm_dict['NN_norm_resampling_%d'%bstrap] for bstrap in range(n_resamples)])
        med_columns = ["NN_d24_weight_resampling_%d" %bstrap for bstrap in range(n_resamples)]

    if doBootstrap and not nom_only:
        norms_VR = np.array([norm_dict_VR['NN_norm'+VRlabel+'_resampling_%d'%bstrap] for bstrap in range(n_resamples)])
        med_columns_VR = ["NN_d24_weight"+VRlabel+"_resampling_%d" %bstrap for bstrap in range(n_resamples)]

    if output_dir:
        out_name = output_dir + os.path.basename(fname)[:-5]+'_'+comb_label+batch_label+'.root'
    else:
        out_name = fname[:-5]+'_'+comb_label+batch_label+'.root' 

    print("Writing to", out_name)

    if doBootstrap:
        medIQR_names = []
        if not alt_only:
            medIQR_names += ['NN_d24_weight_bstrap_med', 'NN_d24_weight_bstrap_perc_25', 
                             'NN_d24_weight_bstrap_perc_75', 'NN_d24_weight_bstrap_IQR']
        if not nom_only:
            medIQR_names += ['NN_d24_weight'+VRlabel+'_bstrap_med', 'NN_d24_weight'+VRlabel+'_bstrap_perc_25',
                             'NN_d24_weight'+VRlabel+'_bstrap_perc_75', 'NN_d24_weight'+VRlabel+'_bstrap_IQR']

    with uproot.recreate(out_name) as f:
        for treeName in treeNames:
            f[treeName] = uproot.newtree(tree_dict)
            print("Merging tree", treeName)
            tree_in_d = d_file[treeName]
            vbf_exists= (b'pass_vbf_sel' in tree_in_d.keys())

            trees_in_w = [w_file[treeName] for w_file in w_files]

            clusters = list(tree_in_d.clusters())
            clusters_w = list(trees_in_w[0].clusters())

            if doBatch:
                print(len(clusters), "total clusters")
                if start > len(clusters):
                    print("Start past end of tree, skipping in this file")
                    continue
                if end > len(clusters):
                    end_here = len(clusters)
                else:
                    end_here = end
                print("Running from cluster", start, "to", end_here)
                clusters_here = clusters[start:end_here]
                clusters_here_w = clusters_w[start:end_here]
            else:
                clusters_here = clusters
                clusters_here_w = clusters_w
        

            for clust_idx in tqdm(range(len(clusters_here))):
                cluster = clusters_here[clust_idx]
                cluster_w = clusters_here_w[clust_idx]

                data_in = tree_in_d.pandas.df(entrystart=cluster[0], entrystop=cluster[1])
                if vbf_exists:
                    if doVBF:
                        #VBF background
                        data_in = data_in[data_in["pass_vbf_sel"]]
                    else:
                        #ggF background
                        data_in = data_in[~data_in["pass_vbf_sel"]]
                else:
                    print("pass_vbf_sel flag not in NNT")

                
                w_ins = [tree_in_w.pandas.df(entrystart=cluster_w[0], entrystop=cluster_w[1])
                         for tree_in_w in trees_in_w]
                full_w_in = reduce(lambda left,right: 
                                   pd.merge(left,right,how="left", 
                                            on=["event_number", "run_number"],
                                            validate="one_to_one"), w_ins)
                if doBootstrap and not alt_only:
                    full_w_in.insert(0, 'NN_d24_weight_bstrap_med', np.median(full_w_in[med_columns]*norms, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight_bstrap_perc_25', np.percentile(full_w_in[med_columns]*norms, 25, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight_bstrap_perc_75', np.percentile(full_w_in[med_columns]*norms, 75, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight_bstrap_IQR', iqr(full_w_in[med_columns]*norms, axis=1))

                if doBootstrap and not nom_only:
                    full_w_in.insert(0, 'NN_d24_weight'+VRlabel+'_bstrap_med', np.median(full_w_in[med_columns_VR]*norms_VR, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight'+VRlabel+'_bstrap_perc_25', np.percentile(full_w_in[med_columns_VR]*norms_VR, 25, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight'+VRlabel+'_bstrap_perc_75', np.percentile(full_w_in[med_columns_VR]*norms_VR, 75, axis=1))
                    full_w_in.insert(0, 'NN_d24_weight'+VRlabel+'_bstrap_IQR', iqr(full_w_in[med_columns_VR]*norms_VR, axis=1))

                if doBootstrap:
                    if save_all_bootstraps:
                        merged=pd.merge(data_in,full_w_in,how="left",on=["event_number", "run_number"],
                                        validate="one_to_one")
                    else:
                        merged=pd.merge(data_in,full_w_in[medIQR_names+["event_number", "run_number"]],how="left",on=["event_number", "run_number"],
                                        validate="one_to_one")
                else:
                    merged=pd.merge(data_in,full_w_in,how="left",on=["event_number", "run_number"],
                                    validate="one_to_one")

                rename_dict = {}
                for col in list(merged.keys()):
                    if col.find('d24_weight') != -1:
                        rename_dict[col] = col+'_'+yr_short
                merged=merged.rename(columns=rename_dict)
                if merged.isna().values.any():
                    print("Warning - NaN in dataframe!!")
                f[treeName].extend(merged.to_dict(orient='list'))


    f_out = TFile(out_name, "update")
    if (save_all_bootstraps and (not doBatch or (doBatch and start == 0))) or not doBootstrap:
        print("About to add norms")
        for w_fname in w_fnames:
            f_extra = TFile(w_fname, "read")
            for key in f_extra.GetListOfKeys():
                if key.GetName() and key.GetClassName().find('TTree') == -1:
                    extra = f_extra.Get(key.GetName())
                    extra_out = extra.Clone()
                    if key.GetClassName().find('TParameter') != -1:
                        outname = extra_out.GetName()+"_"+yr_short
                    else:
                        outname = extra_out.GetName()
                    f_out.cd()
                    extra_out.Write(outname, TObject.kOverwrite)

    if doBootstrap and not do_tth and not do_tnh:
        if not doBatch and not flabel:
            print("About to calculate med norm")
            with uproot.open(out_name) as out_file:
                if not alt_only:
                    CR = out_file[CRtree].arrays(['NN_d24_weight_bstrap_med_'+yr_short,'ntag'],
                                                 namedecode='utf-8')
                if not nom_only:
                    VR = out_file[VRtree].arrays(['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short,'ntag'],
                                                 namedecode='utf-8')

            if not alt_only:
                sf = np.sum(CR['ntag']>=4)/np.sum(CR['NN_d24_weight_bstrap_med_'+yr_short][CR['ntag']==2])
                norms_iqr = iqr(norms)
                norms_perc_25 = np.percentile(norms, 25)
                norms_perc_75 = np.percentile(norms, 75)
                print('Nom med norm:', sf, 'Perc 25:', norms_perc_25, 'Perc 75:', norms_perc_75, 'Norms IQR:', norms_iqr)
            if not nom_only:
                sf_VRderiv = np.sum(VR['ntag']>=4)/np.sum(VR['NN_d24_weight'+VRlabel+'_bstrap_med_'+yr_short][VR['ntag']==2])
                norms_VR_iqr = iqr(norms_VR)
                norms_VR_perc_25 = np.percentile(norms_VR, 25)
                norms_VR_perc_75 = np.percentile(norms_VR, 75)
                print('VR med norm:', sf_VRderiv, 'Perc 25:', norms_VR_perc_25, 'Perc 75:', norms_VR_perc_75, 'Norms IQR:', norms_VR_iqr)

            f_out.cd()
            if not alt_only:
                norm = TParameter("double")("NN_norm_bstrap_med_"+yr_short, sf)
                norm.Write("NN_norm_bstrap_med_"+yr_short, TObject.kOverwrite)

                iqr_param = TParameter("double")("NN_norm_bstrap_IQR_"+yr_short, norms_iqr)
                iqr_param.Write("NN_norm_bstrap_IQR_"+yr_short, TObject.kOverwrite)

                perc25_param = TParameter("double")("NN_norm_bstrap_perc_25_"+yr_short, norms_perc_25)
                perc25_param.Write("NN_norm_bstrap_perc_25_"+yr_short, TObject.kOverwrite)

                perc75_param = TParameter("double")("NN_norm_bstrap_perc_75_"+yr_short, norms_perc_75)
                perc75_param.Write("NN_norm_bstrap_perc_75_"+yr_short, TObject.kOverwrite)

            if not nom_only:
                norm_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_med_"+yr_short, sf_VRderiv)
                norm_VR.Write("NN_norm"+VRlabel+"_bstrap_med_"+yr_short, TObject.kOverwrite)

                iqr_param_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_IQR_"+yr_short, norms_VR_iqr)
                iqr_param_VR.Write("NN_norm"+VRlabel+"_bstrap_IQR_"+yr_short, TObject.kOverwrite)

                perc25_param_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_perc_25_"+yr_short, norms_VR_perc_25)
                perc25_param_VR.Write("NN_norm"+VRlabel+"_bstrap_perc_25_"+yr_short, TObject.kOverwrite)

                perc75_param_VR = TParameter("double")("NN_norm"+VRlabel+"_bstrap_perc_75_"+yr_short, norms_VR_perc_75)
                perc75_param_VR.Write("NN_norm"+VRlabel+"_bstrap_perc_75_"+yr_short, TObject.kOverwrite)

            f_out.Close()
        else:
            print("Use add_norms.py to calculate the median normalization. Cannot do so here due to split files")


if __name__ == '__main__':
    driver = common("combine_weights.py")

    parser = driver.makeParser()
    parser.add_argument("--save-all-bootstraps",
                        action="store_true", dest="save_all_bootstraps", default=False,
                        help="Turn on save all bootstraps, otherwise just keep med+IQR for each event.")
    parser.add_argument("--start", dest="start", default="-1",
                        help="Batch to start on")
    parser.add_argument("--end", dest="end", default="-1",
                        help="Batch to end on")
    parser.add_argument("--norm-label", dest="norm_label", default="",
                        help="For split data files, the label used for file with norms (e.g. a, b, c)")
    parser.add_argument("--nom-only",
                        action="store_true", dest="nom_only", default=False,
                        help="Only combine nominal estimate")
    parser.add_argument("--alt-only",
                        action="store_true", dest="alt_only", default=False,
                        help="Only combine alternate (VR derived) estimate")
    parser.add_argument("--norm_dir", dest="norm_dir", default="",
                        help="Directory with normalizations from data")
    parser.add_argument("--out-tree-list", dest="out_tree_list", default=[], nargs="+",
                        help="List of trees to write out. By default, does everything in input.")
    parser.add_argument("--vbf",
                        action="store_true", dest="doVBF", default= False,
                        help="Use for VBF selection. If not passed, will veto VBF events if flag in NNT")
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)
    inputs["save_all_bootstraps"] = args.save_all_bootstraps
    inputs["start"] = int(args.start)
    inputs["end"] = int(args.end)
    inputs["norm_label"] = args.norm_label
    inputs["nom_only"] = args.nom_only
    inputs["alt_only"] = args.alt_only
    inputs['out_tree_list'] = args.out_tree_list
    inputs['doVBF'] = args.doVBF
    
    norm_dir = args.norm_dir
    if norm_dir:
        if norm_dir[-1] != '/': norm_dir+='/'
    else:
        norm_dir = inputs["weight_dir"]

    inputs["norm_dir"] = norm_dir

    main(inputs)
