#!/usr/bin/env python3

import numpy as np

from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
import pickle

from pandas import DataFrame 

import sys
sys.path.insert(0, 'hep_ml')
sys.path.insert(0, '../hep_ml')
from hep_ml import reweight

from rw_utils import *
from rw_common import *

import yaml
import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import root_numpy
from ROOT import TFile, TParameter

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    output_dir = inputs["output_dir"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    rw_tree = inputs["rw_tree"]
    doBootstrap = inputs["doBootstrap"]
    doNominal = inputs["doNominal"]
    n_resamples_here = inputs["n_resamples_here"]
    start = inputs["start"]
    job = inputs["job"]
    n_jobs = inputs["n_jobs"]
    full = inputs["full"]
    all_trees = inputs["all_trees"]
    param_dict = inputs["param_dict"]
    param_label = inputs["param_label"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    save = inputs["save"]
    doLog = inputs["doLog"]
    weight_label = inputs["weight_label"]

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
                    'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt', 'pt_hh']

    columns_mc_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
                       'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'event_number', 'X_wt', 'pt_hh']

    if 'columns' in param_dict.keys():
        for column in param_dict['columns']:
            if column not in columns_load:
                columns_load+=[column]
            if column not in columns_mc_load:
                columns_mc_load+=[column] 


    print("About to load in data...")
    full_data = root_numpy.root2array(data_file,
                                      treename=rw_tree, branches=columns_load)
    if do_tth:
        full_tth = root_numpy.root2array(tth_file,
                                         treename=rw_tree, branches=columns_mc_load)

    if do_tnh:
        full_ttnh = root_numpy.root2array(tnh_file,
                                          treename=rw_tree, branches=columns_mc_load)

    rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets', 'pt_hh']
    if 'columns' in param_dict.keys():
        rw_columns = param_dict['columns']

    print("Loaded! Setting up for training...")
    full_data = append_fields(full_data, 'BDT_d24_weight'+reg_label+weight_label, np.ones(len(full_data)), usemask=False)
    if do_tth: full_tth = append_fields(full_tth, 'BDT_d24_weight'+reg_label+weight_label, np.ones(len(full_tth)), usemask=False)
    if do_tnh: full_ttnh = append_fields(full_ttnh, 'BDT_d24_weight'+reg_label+weight_label, np.ones(len(full_ttnh)), usemask=False)

    if doLog:
        print("Taking logs of", doLog)
        for col in doLog:
            full_data[col] = np.log(full_data[col])

    #Seed for reproducibility
    np.random.seed(1000)

    #Run random so that the call here corresponds to the start'th call  
    if doBootstrap:
        for run in range(start):
            _ = np.random.poisson(1, len(full_data))

        for count in range(n_resamples_here+doNominal):
            if doNominal and count == 0:
                resamp_id = ""
            else:
                resamp_id = "%d" % (start+count-doNominal)
                poisson_weight = np.random.poisson(1, len(full_data))
                full_data = append_fields(full_data, 'poissonWeight_resamp_'+resamp_id, poisson_weight, usemask=False)

    arrs = {}
    arrs['dat2'], arrs['dat4'] = driver.preprocess(full_data, year)
    if do_tth:
        arrs['th2'], arrs['th4'] = driver.preprocess(full_tth, year)
    if do_tnh:
        arrs['tnh2'], arrs['tnh4'] = driver.preprocess(full_ttnh, year)

    print(np.mean(arrs['dat2']['pT_4']), np.mean(arrs['dat4']['pT_4']))
    original = DataFrame(arrs['dat2'][rw_columns])
    target = DataFrame(arrs['dat4'][rw_columns])

    #Defaults
    param_list = ['n_estimators', 'learning_rate', 'max_depth', 'min_samples_leaf', 'gb_args']
    BDT_params = {}
    BDT_params['n_estimators'] = 50
    BDT_params['learning_rate'] = 0.1
    BDT_params['max_depth'] = 6
    BDT_params['min_samples_leaf'] = 125
    BDT_params['gb_args'] = {'subsample': 0.4}

    print("Setting up BDT with parameters:")
    for key in param_list:
        if key in param_dict.keys():
            BDT_params[key] = param_dict[key]
        print(key+':', BDT_params[key])


    print("About to train...")
    print("Training on columns:", rw_columns)

    reweighter_dict = {}
    for count in range(n_resamples_here+doNominal):
        if doNominal and count == 0:
            resamp_id = ""
        else:
            resamp_id = "%d" % (start+count-doNominal)

        reweighter_dict[resamp_id] = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'], learning_rate=BDT_params['learning_rate'], max_depth=BDT_params['max_depth'], 
                                                           min_samples_leaf=BDT_params['min_samples_leaf'], 
                                                           gb_args=BDT_params['gb_args'])

        if resamp_id:
            print("Resampling id:", resamp_id)
            original_weights = arrs['dat2']['poissonWeight_resamp_'+resamp_id]
            target_weights = arrs['dat4']['poissonWeight_resamp_'+resamp_id]
        else:
            original_weights = np.ones(len(arrs['dat2']))
            target_weights = np.ones(len(arrs['dat4']))

        reweighter_dict[resamp_id].fit(original, target, original_weight=original_weights, target_weight=target_weights)


    treeNames=[rw_tree]
    if all_trees:
        f = TFile(data_file, "read")
        treeNames= [key.GetName() for key in f.GetListOfKeys() if key.GetClassName() == "TTree"]
        f.Close()

    save_list = ['event_number', 'run_number']
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            save_list += ['BDT_d24_weight'+reg_label+weight_label+resamp_label]
        else:
            save_list += ['BDT_d24_weight'+reg_label+weight_label]
    save_list_mc = save_list.copy()

    if full:
        save_list = columns_load.copy()
        save_list_mc = columns_mc_load.copy()


    print("About to write weights for trees: ", treeNames)
    for treeCount in range(len(treeNames)):
        if treeCount == 0:
            treemode = 'recreate'
        else:
            treemode = 'update'
     
        treeName = treeNames[treeCount]    

        print("Writing tree", treeName)
        data_out = root_numpy.root2array(data_file, treename=treeName, branches=columns_load)

        if doLog:
            print("Taking logs for output")
            for col in doLog:
                data_out[col] = np.log(data_out[col])
       
        for resamp_id in reweighter_dict.keys():
            if resamp_id:
                resamp_label = "_resampling_"+resamp_id
            else:
                resamp_label = ""
            data_out = append_fields(data_out, 'BDT_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(data_out)), usemask=False)
            isdat2 = data_out['ntag'] == 2
            data_out['BDT_d24_weight'+reg_label+weight_label+resamp_label][isdat2] = reweighter_dict[resamp_id].predict_weights(DataFrame(data_out[isdat2][rw_columns]))
        root_numpy.array2root(data_out[save_list], output_dir+'dat_BDT_d24_'+yr_short+reg_label+label+'.root',
                              treename=treeName, mode=treemode)

        if do_tth:
            tth_out = root_numpy.root2array(tth_file, treename=treeName, branches=columns_mc_load)

            for resamp_id in reweighter_dict.keys():
                if resamp_id:
                    resamp_label = "_resampling_"+resamp_id
                else:
                    resamp_label = ""
                tth_out = append_fields(tth_out, 'BDT_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(tth_out)), usemask=False)
                istth2 = tth_out['ntag'] == 2
                tth_out['BDT_d24_weight'+reg_label+weight_label+resamp_label][istth2] = reweighter_dict[resamp_id].predict_weights(DataFrame(tth_out[istth2][rw_columns]))
            root_numpy.array2root(tth_out[save_list_mc], output_dir+'tth_BDT_d24_'+yr_short+reg_label+label+'.root', 
                                  treename=treeName, mode=treemode)

        if do_tnh:
            ttnh_out = root_numpy.root2array(tnh_file, treename=treeName, branches=columns_mc_load)

            for resamp_id in reweighter_dict.keys():
                if resamp_id:
                    resamp_label = "_resampling_"+resamp_id
                else:
                    resamp_label = ""
                ttnh_out = append_fields(ttnh_out, 'BDT_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(ttnh_out)), usemask=False)
                isttnh2 = ttnh_out['ntag'] == 2
                ttnh_out['BDT_d24_weight'+reg_label+weight_label+resamp_label][isttnh2] = reweighter_dict[resamp_id].predict_weights(DataFrame(ttnh_out[isttnh2][rw_columns]))
            root_numpy.array2root(ttnh_out[save_list_mc], output_dir+'ttnh_BDT_d24_'+yr_short+reg_label+label+'.root', 
                                  treename=treeName, mode=treemode)

    isdat2 = full_data['ntag']==2
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
            original_weights = full_data['poissonWeight_resamp_'+resamp_id]
            full_data = append_fields(full_data, 'BDT_d24_weight'+reg_label+weight_label+resamp_label, np.ones(len(full_data)), usemask=False)
        else:
            resamp_label = ""
            original_weights = np.ones(len(full_data))

        full_data['BDT_d24_weight'+reg_label+weight_label+resamp_label][isdat2] = reweighter_dict[resamp_id].predict_weights(DataFrame(full_data[isdat2][rw_columns]),
                                                                                                original_weight=original_weights[isdat2])
        norm_factor = 1.*np.sum(original_weights[full_data['ntag']>=4])/np.sum(full_data[full_data['ntag']==2]['BDT_d24_weight'+reg_label+weight_label+resamp_label])

        norm = TParameter("double")("BDT_norm"+reg_label+weight_label+resamp_label, norm_factor)
        f = TFile(output_dir+'dat_BDT_d24_'+yr_short+reg_label+label+'.root', "update")
        norm.Write()
        f.Close()

    if save:
        if param_label:
            param_label = '_config_'+param_label
        BDT_fname = output_dir+'BDT_d24_'+yr_short+param_label+reg_label+label+'.p'
        pickle.dump(reweighter, open( BDT_fname, "wb" ))


if __name__ == '__main__':
    driver = common("BDT_RW.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)
