#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

import os

import pandas as pd
from pandas import DataFrame

from rw_utils import *
from rw_common import *

import yaml
import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

import uproot
from ROOT import TFile, TParameter

from tqdm import tqdm

def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0-phi1) - np.pi)

def dR_hh(arr):
    return np.sqrt(dPhi(arr['phi_h1'], arr['phi_h2'])**2+(arr['eta_h1']-arr['eta_h2'])**2)

def main(inputs):
    #Unpack inputs
    year = inputs["year"]
    yr_short = inputs["yr_short"]
    do_tth = inputs["do_tth"]
    do_tnh = inputs["do_tnh"]
    output_dir = inputs["output_dir"]
    data_file = inputs["data_file"]
    tth_file = inputs["tth_file"]
    tnh_file = inputs["tnh_file"]
    rw_tree = inputs["rw_tree"]
    kinematic_region = inputs["kinematic_region"]
    doBootstrap = inputs["doBootstrap"]
    doNominal = inputs["doNominal"]
    n_resamples_here = inputs["n_resamples_here"]
    start = inputs["start"]
    job = inputs["job"]
    n_jobs = inputs["n_jobs"]
    full = inputs["full"]
    all_trees = inputs["all_trees"]
    param_dict = inputs["param_dict"]
    param_label = inputs["param_label"]
    label = inputs["label"]
    reg_label = inputs["reg_label"]
    doLog = inputs["doLog"]
    weight_label = inputs["weight_label"]
    config = inputs["config"]
    use_old_names = inputs["use_old_names"]
    out_tree_list = inputs["out_tree_list"]
    weight_dir = inputs["weight_dir"]

    flabel=[]
    for fname in data_file:
        yr_idx = fname.find(yr_short)
        ext_idx = fname.find(".root")
        flabel.append(fname[yr_idx+2:ext_idx])

    #Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
    columns_load = ['ntag', 'event_number', 'run_number']


    if kinematic_region:
        columns_load+= ['kinematic_region']

    if 'columns' in param_dict.keys():
        for column in param_dict['columns']:
            if column not in columns_load:
                columns_load+=[column]

    columns_mc_load = columns_load + ['mc_sf']

    print("About to load in data...")
    print(data_file, rw_tree, year)

    files_in = []
    w_files = []
    for fidx in range(len(data_file)):
        print(os.path.basename(data_file[fidx]))
        files_in.append(uproot.open(data_file[fidx]))
        count = 0
        for cluster in tqdm(list(files_in[fidx][rw_tree].clusters())):
            if count == 0 and fidx == 0:
                full_data = files_in[fidx][rw_tree].pandas.df(columns_load, 
                                                    entrystart=cluster[0], entrystop=cluster[1])
            else:
                full_data = full_data.append(files_in[fidx][rw_tree].pandas.df(columns_load, 
                                              entrystart=cluster[0], entrystop=cluster[1]), ignore_index=True)
            count+=1

    print(weight_dir+"dat_NN_d24_"+yr_short+reg_label+label+".root")
    w_files = uproot.open(weight_dir+"dat_NN_d24_"+yr_short+reg_label+label+".root")
    w_arr = w_files[rw_tree].pandas.df()
    full_data = pd.merge(full_data,w_arr,how="left", on=["event_number", "run_number"], validate="one_to_one")



    if kinematic_region:
        if use_old_names:
            kr_dict = {"control" : 1, "sideband" : 2}
        else:
            kr_dict = {"validation" : 1, "control" : 2}

        full_data = full_data[(full_data["kinematic_region"] == kr_dict[kinematic_region])]

    #Seed for reproducibility
    np.random.seed(1000)

    #Run random so that the call here corresponds to the start'th call  
    if doBootstrap:
        for run in range(start):
            _ = np.random.poisson(1, len(full_data))

        for count in range(n_resamples_here+doNominal):
            if doNominal and count == 0:
                resamp_id = ""
            else:
                resamp_id = "%d" % (start+count-doNominal)
                poisson_weight = np.random.poisson(1, len(full_data))
                full_data.insert(0, 'poissonWeight_resamp_'+resamp_id, poisson_weight, False)



    isdat2 = full_data['ntag']==2
    f_root = TFile(weight_dir+'dat_NN_d24_'+yr_short+flabel[0]+reg_label+label+'_norms.root', "recreate")
    for count in range(n_resamples_here+doNominal):
        if doNominal and count == 0:
            resamp_id = ""
        else:
            resamp_id = "%d" % (start+count-doNominal)
            
        resamp_label = "_resampling_"+resamp_id
        original_weights = full_data['poissonWeight_resamp_'+resamp_id]

        norm_factor = (1.*np.sum(original_weights[full_data['ntag']>=4])/
                       np.sum(original_weights[full_data['ntag']==2]*full_data[full_data['ntag']==2]['NN_d24_weight'+reg_label+weight_label+resamp_label]))

        print(resamp_id, reg_label, norm_factor)
        norm = TParameter("double")("NN_norm"+reg_label+weight_label+resamp_label, norm_factor)
        norm.Write()

    f_root.Close()

if __name__ == '__main__':
    driver = common("NN_RW.py")

    parser = driver.makeParser()
    parser.add_argument("--out-tree-list", dest="out_tree_list", default=[], nargs="+",
                        help="List of trees to write out. By default, does everything in input.")
    parser.add_argument("-w", "--weight-dir", dest="weight_dir", default="",
                        help="Weight dir")

    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    inputs['out_tree_list'] = args.out_tree_list
    inputs['weight_dir'] = args.weight_dir

    main(inputs)
