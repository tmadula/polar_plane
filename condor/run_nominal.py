#!/usr/bin/env python3
import subprocess
import os

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-i", "--input_dir", dest="input_dir", default="",
                    help="Input directory")
parser.add_argument("-d", "--data", dest="data_file", default=["dataXX.root"], nargs="+",
                  help="Data file template (e.g. dataXX.root)")
parser.add_argument("-w", "--weight-dir", dest="weight_dir", default="",
                   help="Directory with weights (for combination)")
parser.add_argument("-o", "--output-dir", dest="output_dir", default="",
                  help="Directory to send output")
parser.add_argument("-y", "--year", dest="year_in",  default="all", nargs="+",
                  help="Years to submit")
parser.add_argument("-l", "--label", dest="label", default="",
                  help="Label (pflow, e.g.)")
parser.add_argument("--doNom",
                  action="store_true", dest="doNom", default=False,
                  help="Derive nominal weights")
parser.add_argument("--doCRw",
                  action="store_true", dest="doCRw", default=False,
                  help="Derive control region weights")
parser.add_argument("--doVRw",
                    action="store_true", dest="doVRw", default=False,
                    help="Derive validaton region weights")
parser.add_argument("--combine",
                  action="store_true", dest="combine", default=False,
                  help="Combine if running weight deriv as well.")
parser.add_argument("--combine-only",
                    action="store_true", dest="combine_only", default=False,
                    help="Only run combination")
parser.add_argument("--bootstrap", dest="n_resamples", default="-1",
                  help="Turn on run bootstrap.")
parser.add_argument("--n_jobs", dest="n_jobs", default="1",
                  help="Number of jobs over which to do bootstrap runs.")
parser.add_argument("--memory", dest="memory", default="16",
                  help="Memory to request")
parser.add_argument("--submit", 
                  action="store_true",dest="submit", default=False,
                  help="Submit to Condor")
parser.add_argument("--no-med",
                    action="store_true",dest="no_med", default=False,
                    help="Turn off median calculation")
parser.add_argument("--use-old-names",
                    action="store_true", dest="use_old_names", default=False,
                    help="Use SB/CR/SR naming scheme")
parser.add_argument("--config", dest="config", default="baseline",
                    help="Choose baseline or dPhi_HC_505050.")
parser.add_argument("--out-tree-list", dest="out_tree_list", default=[], nargs="+",
                    help="List of trees to write out. By default, does everything in input.")
parser.add_argument("--Xwt-cut", dest="doXwtCut", default=False, action="store_true",
                    help="Do Xwt cut, no effect if already cut in n-tuple")

args = parser.parse_args()

input_dir = args.input_dir
data_file = args.data_file
weight_dir = args.weight_dir
year_in = args.year_in
label = args.label
output_dir = args.output_dir
submit = args.submit
doCRw = args.doCRw
doVRw = args.doVRw
doNom = args.doNom
combine = args.combine
combine_only = args.combine_only
n_resamples = args.n_resamples
doBootstrap = (n_resamples != "-1")
n_jobs = args.n_jobs
memory = args.memory
no_med = args.no_med
use_old_names = args.use_old_names
config = args.config
out_tree_list = args.out_tree_list
doXwtCut = args.doXwtCut

#Make sure uniform input and output dir format
if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if data_file:
    for fidx in range(len(data_file)):
        input_dir_here = input_dir
        if data_file[fidx][0] == '/':
            input_dir_here = ''
        data_file[fidx] = input_dir_here+data_file[fidx]
        if data_file[fidx][-4:] != 'root':
            print("Warning! Data file not a root file")
else:
    data_file = input_dir+'output_data'+yr_short+'.root'

if doCRw and not use_old_names:
    print("Naming conflict! Using new naming")
    doVRw = True


if label:
    if label[0] != '_':
        label= '_'+label
if combine or combine_only:
    label+="_comb"
if doVRw or doCRw:
    if use_old_names:
        label+="_CRW"
        alt_tree = "control"
        alt_label = "CRderiv"
    else:
        label+="_VRW"
        alt_tree = "validation"
        alt_label = "VRderiv"
if doNom:
    label+="_Nom"
if doBootstrap:
    label+="_bstrap"

old_name_str = ""
if use_old_names:
    old_name_str = " --use-old-names "

config_str = ""
if config:
    config_str = " --config "+config

out_tree_str = ""
if out_tree_list:
    out_tree_str = " --out-tree-list "
    for tree in out_tree_list:
        out_tree_str+=(tree + " ")

Xwt_str = ""
if doXwtCut:
    Xwt_str = " --Xwt-cut"

years = []
yrs_short = []
for item in year_in:
    if item == "all":
        years = ["2015", "2016", "2017", "2018"]
        yrs_short = ["15", "16", "17", "18"]
        break
    elif len(item) == 4:
        years.append(item)
        yrs_short.append(item[2:])
    elif len(item) == 2:
        years.append("20"+item)
        yrs_short.append(item)
    else:
        print("Year input not understood!")
        break

for idx in range(len(years)):
    year = years[idx]
    yr_short = yrs_short[idx]

    xrd_prefixes_in = []
    xrd_prefixes_out = []
    data_fnames = []
    data_basenames = []
    flabels = []
    xrd_inputs = True
    xrd_outputs = True
    for fidx in range(len(data_file)):
        temp_idx = data_file[fidx].find('XX')
        if temp_idx == -1:
            print("Put in template form dataXX.root!")

        data_fname = data_file[fidx][:temp_idx]+yr_short+data_file[fidx][temp_idx+2:]
        data_basename = os.path.basename(data_fname)
        xrd_prefix_in =""
        if data_fname.find("/eos/atlas/") != -1:
            xrd_prefix_in = "root://eosatlas.cern.ch/"
        elif data_fname.find("/eos/user/") != -1:
            xrd_prefix_in = "root://eosuser.cern.ch/"
        else:
            print("Not using xrdcp for input")
            xrd_inputs=False

        xrd_prefix_out =""
        if output_dir.find("/eos/atlas/") != -1:
            xrd_prefix_out = "root://eosatlas.cern.ch/"
        elif output_dir.find("/eos/user/") != -1:
            xrd_prefix_out = "root://eosuser.cern.ch/"
        else:
            print("Not using xrdcp for output")
            xrd_outputs = False

        xrd_prefixes_in.append(xrd_prefix_in)
        xrd_prefixes_out.append(xrd_prefix_out)
        data_fnames.append(data_fname)
        data_basenames.append(data_basename)
        yr_idx = data_basename.find(yr_short)
        ext_idx = data_basename.find(".root")
        flabels.append(data_basename[yr_idx+2:ext_idx])

    config_name = "submit"+yr_short+label+"_NN.sub"
    exec_name = "submit"+yr_short+label+"_NN.sh"
    job_name = "submit"+yr_short+label+"_NN"

    in_transfer_list = ["../rw_utils.py", "../combine_weights.py", "../rw_common.py", "../rw_model.py"]

    if (doNom or doCRw or doVRw) and not combine_only:
        in_transfer_list+=["../NN_RW.py"]

    out_transfer_list = []

    for fidx in range(len(data_basenames)):
        if doNom and not combine_only:
            if doBootstrap:
                if int(n_jobs) != 1:
                    out_transfer_list += ["dat_NN_d24_"+yr_short+flabels[fidx]+"_bootstrap_$(ProcId).root"]
                else:
                    out_transfer_list += ["dat_NN_d24_"+yr_short+flabels[fidx]+"_bootstrap.root"]
            else:
                out_transfer_list += ["dat_NN_d24_"+yr_short+flabels[fidx]+".root"]
        if doCRw or doVRw and not combine_only:
            if doBootstrap:
                if int(n_jobs) != 1:
                    out_transfer_list += ["dat_NN_d24_"+yr_short+flabels[fidx]+"_"+alt_label+"_bootstrap_$(ProcId).root"]
                else:
                    out_transfer_list += ["dat_NN_d24_"+yr_short+flabels[fidx]+"_"+alt_label+"_bootstrap.root"]
            else:
                out_transfer_list += ["dat_NN_d24"+"_"+alt_label+"_"+yr_short+flabels[fidx]+".root"]

        if combine or combine_only:
            if doBootstrap:
                out_transfer_list += [data_basenames[fidx][:-5]+"_NN_"+n_resamples+"_bootstraps.root"]
            else:
                out_transfer_list += [data_basenames[fidx][:-5]+"_with_weights.root"]

    if not xrd_outputs:
        out_remaps_list = []
        for name in out_transfer_list:
            out_remaps_list += [name+" = "+output_dir+name]


    in_transfer=""
    for fidx in range(len(data_fnames)):
        if (xrd_inputs and not xrd_prefixes_in[fidx]) or not xrd_inputs:
            in_transfer_list+= [data_fnames[fidx]]
    for in_idx in range(len(in_transfer_list)):
        fname = in_transfer_list[in_idx]
        if in_idx != len(in_transfer_list)-1:
            in_transfer+=(fname+",")
        else:
            in_transfer+=(fname+"\n")


    out_transfer=""
    if xrd_outputs:
        out_transfer+="\"\"\n"
    else:
        for out_idx in range(len(out_transfer_list)):
            fname = out_transfer_list[out_idx]
            if out_idx != len(out_transfer_list)-1:
                out_transfer+=(fname+",")
            else:
                out_transfer+=(fname+"\n")

        out_remaps="\""
        for out_idx in range(len(out_remaps_list)):
            fname = out_remaps_list[out_idx]
            if out_idx != len(out_remaps_list)-1:
                out_remaps+=(fname+";")
            else:
                out_remaps+=(fname+"\"\n")

    data_file_str = ""

    for fname in data_basenames:
        data_file_str+= (fname+" ")

    print("Writing config/executables", config_name, "and", exec_name, "for NN")

    with open(exec_name, "w") as fexec:
        fexec.write("#!/bin/bash\n")
        fexec.write("echo \"We are node $2\"\n")
        fexec.write("source /eos/user/s/sgasioro/public/bbbb_env/bin/activate\n")
   
        if xrd_inputs:
            for fidx in range(len(xrd_prefixes_in)):
                fexec.write("xrdcp "+xrd_prefixes_in[fidx]+data_fnames[fidx]+" .\n")
        fexec.write("ls -lth\n")
        if doNom and not combine_only:
            if doBootstrap:
                fexec.write("./NN_RW.py -d "+data_file_str+"-y "+year+" --bootstrap "+
                            n_resamples+" --job $2 --n_jobs "+n_jobs+old_name_str+config_str+Xwt_str+out_tree_str+"\n")
            else:
                fexec.write("./NN_RW.py -d "+data_file_str+"-y "+year+old_name_str+config_str+Xwt_str+out_tree_str+"\n")
        if (doCRw or doVRw) and not combine_only:
            if doBootstrap:
                fexec.write("./NN_RW.py -d "+data_file_str+"-y "+year+" --bootstrap "+
                            n_resamples+" --job $2 --n_jobs "+n_jobs+" --reweight-tree "+alt_tree+old_name_str+config_str+Xwt_str+out_tree_str+"\n")
            else:
                fexec.write("./NN_RW.py -d "+data_file_str+"-y "+year+" --reweight-tree "+alt_tree+old_name_str+config_str+Xwt_str+out_tree_str+"\n")

        if combine or combine_only:
            if doBootstrap:
                fexec.write("./combine_weights.py -d "+data_file_str+"--comb_label NN_"+n_resamples+"_bootstraps -w "+weight_dir+" --bootstrap "+
                            n_resamples+" --n_jobs "+n_jobs+" -y "+year+old_name_str+"\n")
            else:
                fexec.write("./combine_weights.py -d "+data_file_str+"--comb_label with_weights -w "+weight_dir+" -y "+year+old_name_str+"\n")

        fexec.write("ls -lth\n")

        if xrd_outputs:
            for out in out_transfer_list:
                out_swap = out.replace('$(ProcId)', '$2')
                fexec.write("xrdcp "+out_swap+" "+xrd_prefixes_out[0]+output_dir+out_swap+"\n")

        if doNom or doVRw or doCRw:
            fexec.write("mkdir networks\n")
            fexec.write("mv *.h5 networks/\n")
            fexec.write("mv *.pkl networks/\n")
            fexec.write("tar -czvf networks"+year+"_$2.tar.gz networks\n")
            if xrd_outputs:
                fexec.write("xrdcp networks"+year+"_$2.tar.gz "+xrd_prefixes_out[0]+output_dir+"networks"+year+"_$2.tar.gz\n")
            else:
                out_transfer = "networks"+year+"_$2.tar.gz,"+out_transfer
                out_remaps = ("networks"+year+"_$2.tar.gz = "+output_dir+"networks"+year+"_$2.tar.gz;"+out_remaps)


    with open(config_name, "w") as config:
        config.write("universe = vanilla\n")
        config.write("Executable = "+exec_name+"\n")
        config.write("arguments = $(ClusterID) $(ProcId)\n")
        config.write("Output = output/"+job_name+".$(ClusterId).$(ProcId).out\n")
        config.write("Error = error/"+job_name+".$(ClusterId).$(ProcId).err\n")
        config.write("Log = log/"+job_name+".$(ClusterId).$(ProcId).log\n")
        config.write("request_memory = "+memory+"GB\n")
        config.write("+JobFlavour = \"workday\"\n")
        config.write("transfer_input_files = "+in_transfer)
        config.write("transfer_output_files = "+out_transfer)
        if not xrd_outputs:
            config.write("transfer_output_remaps = "+out_remaps)
            config.write("when_to_transfer_output = ON_EXIT\n")
            config.write("max_transfer_output_mb = -1\n")

        if combine_only:
            config.write("Queue 1")
        else:
            config.write("Queue "+n_jobs)

    if submit:
        print("Running submission for year "+year)
        print(subprocess.check_output(["condor_submit", config_name]).decode("utf-8"))
