#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--limit-input", dest="limit_input", default="",
                  help="Limit input file")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")

(options, args) = parser.parse_args()

input_file = options.limit_input
label = options.label

if input_file.find('HCrescale') != -1:
    label+="_HCrescale"

if label and label[0] != '_':
    label = '_'+label

from rootpy.plotting import *
from rootpy.io import root_open
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)

methods = []
if input_file.find('BDT') != -1:
    methods.append('BDT')
if input_file.find('spline') != -1:
    methods.append('spline')

if input_file.find('16') != -1:
    year = "2016"
elif input_file.find('17') != -1:
    year = "2017"
elif input_file.find('18') != -1:
    year = "2018"
else:
    year = "2015"


if input_file.find('rel20') != -1:
    rel_label = ", rel 20"
elif input_file.find('rel21') != -1:
    rel_label = ", rel 21"
else:
    rel_label = ""

f = root_open(input_file)

#qcd_hh
#allhad_hh
#nonallhad_hh
#sm_hh
#g_hh_m400_c10
#g_hh_m500_c10
#g_hh_m600_c10
#g_hh_m700_c10
#g_hh_m800_c10
#g_hh_m900_c10
#g_hh_m1000_c10
for method in methods:
    if method == 'spline':
        hist_tth_4tag = f.Get("allhad_hh")
        hist_ttnh_4tag = f.Get("nonallhad_hh")

        hist_tth_4tag.SetFillColor(860-9)
        hist_ttnh_4tag.SetFillColor(860-4)

        hist_tth_4tag.SetFillStyle('solid')
        hist_ttnh_4tag.SetFillStyle('solid')
  
        hist_tth_4tag.SetLineColor(1)
        hist_ttnh_4tag.SetLineColor(1)

        hist_qcd = f.Get("qcd_hh")
    else:
        hist_qcd = f.Get("BDT_hh")   

    hist_qcd.SetFillColor(5)
    hist_qcd.SetFillStyle('solid')
    hist_qcd.SetLineColor(1)

    hist_smhh = f.Get("sm_hh")
    hist_smhh.Scale(100.)
    hist_smhh.SetTitle("SM HH x 100")
    hist_smhh.SetFillStyle(0)
    hist_smhh.SetLineColor(ROOT.kGreen+3)
    hist_smhh.SetLineStyle(2)
    hist_smhh.SetLineWidth(2)
   
    hist_s_hh_280 = f.Get("s_hh_m280")
    hist_s_hh_280.SetTitle("Scalar (280 GeV)")
    hist_s_hh_280.SetFillStyle(0)
    hist_s_hh_280.SetLineColor(ROOT.kRed)
    hist_s_hh_280.SetLineStyle(7)
    hist_s_hh_280.SetLineWidth(2)
 
    #hist_g10_800 = f.Get("g_hh_m800_c10")
    #hist_g10_800.SetTitle("G*_{KK} (800 GeV, k/#bar{M}_{Pl} = 1)")
    #hist_g10_800.SetFillStyle(0)
    #hist_g10_800.SetLineColor(ROOT.kMagenta+2)
    #hist_g10_800.SetLineWidth(2)

    if method == 'spline':
        stack = HistStack(hists=[hist_ttnh_4tag,hist_tth_4tag,hist_qcd])
    if method == 'BDT':
        stack = HistStack(hists=[hist_qcd])

    #stack.SetMaximum(hist_qcd.GetMaximum()*1.4)
    #stack.SetMaximum(1550)
    stack.SetMaximum(1600*(year == "2016") + 2000*(year=="2017")+ 2000*(year=="2018"))

    canvas = Canvas()
    canvas.cd()
    if method == 'spline':
        hist_tth_4tag.legendstyle = 'F'
        hist_ttnh_4tag.legendstyle = 'F'
        hist_qcd.legendstyle = 'F'
        hist_smhh.legendstyle = 'L'
        hist_s_hh_280.legendstyle = 'L'
     #   hist_g10_800.legendstyle = 'L'
        legend = Legend([hist_tth_4tag,hist_ttnh_4tag, hist_qcd, hist_smhh, hist_s_hh_280], leftmargin=0.43, topmargin=0.03, textsize=0.035)
    if method == 'BDT':
        hist_qcd.SetTitle("BDT Background")
        hist_qcd.legendstyle = 'F'
        hist_smhh.legendstyle = 'L'
        hist_s_hh_280.legendstyle = 'L'
       # hist_g10_800.legendstyle = 'L'
        legend = Legend([hist_qcd, hist_smhh, hist_s_hh_280], leftmargin=0.25, margin=0.3)

    legend.SetHeader(year+" SR"+rel_label)

    stack.Draw("hist")

    if input_file.find('HCrescale') != -1:
        stack.GetXaxis().SetTitle('m_{HH} (corrected) [GeV]')
    else:
        stack.GetXaxis().SetTitle('m_{HH} [GeV]')

    hist_smhh.Draw("hist same")
    hist_s_hh_280.Draw("hist same")
   # hist_g10_800.Draw("hist same")

    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.Draw()

    canvas.Draw()
 
    canvas.SaveAs(input_file[:-5]+label+"_mhh_histogram.pdf")

    ROOT.gPad.SetLogy()
    stack.SetMinimum(8e-3)
    stack.SetMaximum(1e5)
   # canvas.Update()
    canvas.SaveAs(input_file[:-5]+label+"_mhh_histogram_logscale.pdf")
