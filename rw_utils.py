#!/usr/bin/env python3

import numpy as np
from scipy import stats
from scipy.special import binom

def run_pulls(h_obs, h_exp):

    h_pull = h_obs.Clone("h_pull")

    for i in range(1, h_obs.GetNbinsX()+1):
        nbObs = h_obs.GetBinContent(i)
        nbExp = h_exp.GetBinContent(i)

        nbExpEr = np.sqrt(h_obs.GetBinError(i)**2 + h_exp.GetBinError(i)**2)

        if nbExp == 0 or nbObs == 0:
            h_pull.SetBinContent(i, 0)
            h_pull.SetBinError(i, 0)
            continue

        #significance calculated from https://cds.cern.ch/record/2643488
        # relabel variables to match CDS formula
        factor1 = nbObs*np.log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
        factor2 = (nbExp**2/nbExpEr**2)*np.log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )

        if nbObs < nbExp:
            pull  = -np.sqrt(2*(factor1 - factor2))
        else:
            pull  = np.sqrt(2*(factor1 - factor2))
 
        h_pull.SetBinContent(i, pull)
        h_pull.SetBinError(i, 0)
    
    return h_pull


def vanillize_name(branchname):
    if branchname.find('BDT_d24_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'BDT_d24_weight_CRderiv'
        elif branchname.find('VRderiv') != -1:
            return 'BDT_d24_weight_VRderiv'
        else:
            return 'BDT_d24_weight'

    if branchname.find('NN_d24_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'NN_d24_weight_CRderiv'
        elif branchname.find('VRderiv') != -1:
            return 'NN_d24_weight_VRderiv'
        else:
            return 'NN_d24_weight'

    if branchname.find('njet_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'njet_weight_CRderiv'
        elif branchname.find('VRderiv') != -1:
            return 'njet_weight_VRderiv'
        else:
            return 'njet_weight'
    if branchname.find('rw_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'rw_weight_CRderiv'
        elif branchname.find('VRderiv') != -1:
            return 'rw_weight_VRderiv'
        else:
            return 'rw_weight'

    return branchname

#Fit function
def f(x, p0, p1, p2, fit_counts):
    ret_arr = np.zeros(len(x))

    #Frame as a 3 binned step function [0,1) is tnh, [1,2) is th, [2,3) is qcd region.
    #Note that in tnh we force qcd counts to 0 - this is by construction with the 2 tag tnh scale factor
    #fit_counts is a dict of dicts indexed as fit_counts[data type key][fit region]
    for i in range(len(x)):
        if(0 <= x[i] < 1):
                val=p0*fit_counts['tnh4']['tnh'] + p1*fit_counts['th4']['tnh'] + p2*fit_counts['qcd']['tnh']*0
                ret_arr[i] = val
        if(1 <= x[i] < 2):
                val=p0*fit_counts['tnh4']['th'] + p1*fit_counts['th4']['th'] + p2*fit_counts['qcd']['th']
                ret_arr[i] = val
        if(2 <= x[i] < 3):
                val=p0*fit_counts['tnh4']['qcd'] + p1*fit_counts['th4']['qcd'] + p2*fit_counts['qcd']['qcd']
                ret_arr[i] = val
    return ret_arr

def makeMCweights(arrs, key, lumi, region=''):
    arr = arrs[key]
    if region:
        arr = arr[region]
    mc_weights = np.ones(len(arr))

    if (key.find('th') != -1) or (key.find('tnh') != -1):
        mc_weights = arr['mc_sf']*lumi

    return mc_weights

#Calculate weights from stored values ('njet_weight', 'rw_weight', 'mc_weight', etc)
def makeWeights(arrs, key, it, year, withnjet=True, region='', doCRw=False, doVRw=False, useUpdatedLumi=True):
    arr = arrs[key]
    if region:
        arr=arr[region]
    nj_weight=[]
    year=int(year)

    deriv_label=''
    if doCRw:
        deriv_label='_CRderiv'
    if doVRw:
        deriv_label='_VRderiv'

    #Bit of non-trivial logic - if th4, want njet weight so that it's actually 4 tag model, always
    if key == 'th4':
        nj_weight = np.ones(len(arr))
 
    #If want weight with nJet, split by iteration - for it 0, default is f=0.22
    #otherwise take it from stored column. For 4 tag, always have ones
    elif withnjet:
        if it == 0:
            if key.find('2') != -1:
                nj_weight = nJetWeight(arr, 0.22)
            else:
                nj_weight = np.ones(len(arr))
        else:
            if key.find('2') != -1:
                nj_weight = arr['njet_weight'+deriv_label].copy()
            else:
                nj_weight = np.ones(len(arr))

    #If not th4 and don't want njet, just have njet weight of one
    else:
        nj_weight = np.ones(len(arr))
        
    #Base calc weight value is njet*mc*rw - should be 1 for data, by design
    weights = nj_weight*arr['rw_weight'+deriv_label]

    #For MC, need to scale by cross section and luminosity, as well as b-tagging weights
    #Trigger scale factors need to be added as well, but this is a small effect
    if useUpdatedLumi:
        lumi15 = 3.2
        lumi16 = 24.6
        lumi17 = 43.65
        lumi18 = 58.45
    else:
        lumi15 = 3.2195
        lumi16 = 24.5556
        lumi17 = 44.3072
        lumi18 = 36.1593
    lumi = (year==2015)*lumi15 + (year==2016)*lumi16 + (year==2017)*lumi17 + (year==2018)*lumi18

    #For 15+16, overall normalization is to combined 15+16 lumi
    if year == 2015 or year == 2016:
        lumi = lumi15+lumi16
    
    weights*=makeMCweights(arrs, key, lumi, region)

    return weights


#Calculate counts in fit regions. Errors are sqrt(sum of w^2)
def getFitCounts(arrs, weights, key, region):
    arr = arrs[key]
    weight = weights[key]

    count = np.sum(weight[region])
    err = np.sqrt(np.sum(weight[region]**2))
    return count, err

#reg = SB, SR, CR
def whichRegions(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    CR_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    VR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    isSR = (Xhh < 1.6)
    isCR = (CR_hh < 45)
    isVR = (VR_hh < 30)

    return {"CR" : isCR, "SR":isSR, "VR":isVR }

#Select SB region
def CRsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    CR_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    VR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    notSR = (Xhh >= 1.6)
    isCR = (CR_hh < 45)
    notVR = (VR_hh >= 30)

    return full[(isCR) & (notVR) & (notSR)]

#Select CR
def VRsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    CR_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    VR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    notSR = (Xhh >= 1.6)
    notSB = (CR_hh >= 45)
    isVR = (VR_hh < 30)
    
    return full[(isVR) & (notSR)]

#Select SR
def SRsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)

    isSR = (Xhh < 1.6)

    return full[(isSR)]

#Calculate reweighting weight from splines
def calc_Weight(fns, arr, rw_columns, it, doCRw=False, doVRw=False):
    deriv_label=''
    if doCRw:
        deriv_label='_CRderiv'
    if doVRw:
        deriv_label='_VRderiv'

    rw_weight = arr['rw_weight'+deriv_label].copy()
    twotag_arr = (arr['ntag']==2)
    
    for var in rw_columns:
        it_weight = ((fns[var](arr[var])-1)*(1-2**(-it-1))+1)*twotag_arr + 1*(~twotag_arr)
        rw_weight *= it_weight

    return rw_weight

#Calculate nJet weight
def nJetWeight(arr, f):
    njets = np.array(arr['njets'])
    ntag = np.array(arr['ntag'])
    
    nj_weight_it = 0
    n_nontag = njets - ntag
   
    if len(njets) == 0:
        return np.array([])  
    #Optimization thing - make bank of possible values for nJet weight (only a few)
    #depends on number of nontagged jets only - max and min give the range
    max_nontag = n_nontag.max()
    min_nontag = n_nontag.min()
    
    #Initialize array of possible weights
    nj_weight_it = np.zeros(max_nontag+1 - min_nontag)
    
    #Initialize return array - default is 1 (four tag value)
    ret_weight = np.ones(len(n_nontag))
    
    #Make possible weight values for given arr
    for nontag in range(min_nontag, max_nontag+1):
        for n_choose in range(2,nontag+1):
            binomial = binom(nontag, n_choose)
            nj_weight_it[nontag-min_nontag] += binomial*(f**n_choose)*((1-f)**(nontag-n_choose))
    
    #Fill nJet weight array by grabbing appropriate values (faster than calculating each time)
    #Indexed, by design, at n_nontag - min_nontag
    for i in range(len(n_nontag)):
        if ntag[i] == 2:
            ret_weight[i] = nj_weight_it[n_nontag[i]-min_nontag]

    return ret_weight


#Calculate weighted chi^2 between hists - see later for usage
def weighted_chisquare(f_obs, f_exp, f_obs_err, f_exp_err):
    #Calculate weighted chi-square using method in arXiv:physics/0605123
    w1 = f_obs
    w2 = f_exp
    s1 = f_obs_err  # noqa
    s2 = f_exp_err  # noqa
    W1 = np.sum(w1)  # noqa
    W2 = np.sum(w2)  # noqa
    X2 = np.sum((W1*w2 - W2*w1)**2 / (W1**2 * s2**2 + W2**2 * s1**2))
    p = stats.chi2.sf(X2, np.size(w1) - 1)
    return (X2, p)

#Prevent errors with zeros by keeping things (slightly) nonzero
def makePositive(arr, arr_err):
    for i in range(len(arr)):
        if arr[i] <= 0:
            arr[i] = 0.000001
            arr_err[i] = 0.000001
    return arr, arr_err

def makePositive_hist(hist):
    for i in range(1, hist.GetNbinsX()+1):
        content = hist.GetBinContent(i)
        if content <= 0.0:
            hist.SetBinContent(i, 0.0000001)
            hist.SetBinError(i,0.0)
    return hist

#Find midpoints of fixed width bins array
def mid(bins):
    return (bins[1]-bins[0])*0.5+bins[:-1]

#Minimizer class for pseudo-tag optimization
class f_min:
    #Initialize with data/ttbar info (arrs), keys, and year. Need to also set sfs (done in it loop)
    def __init__(self, arrs, keys, year, sf_vals={}, doCRw=False, doVRw=False, useUpdatedLumi=True):
        self.arrs = arrs
        self.keys = keys
        self.sf_vals = sf_vals
        self.year = year
        self.doCRw = doCRw
        self.doVRw = doVRw
        self.useUpdatedLumi = useUpdatedLumi

    #Set scale factors from fits
    def set_sfs(self, sf_vals):
        self.sf_vals = sf_vals 

    # tth f factor function to minimize. For a given f, calculates the difference in yield between
    # weighted 2 tag and real 4 tag.
    def to_min_tth(self, f):
        arrs = self.arrs
        nj_hists = {}
        weights_it = {}
  
        n_jets_2 = arrs['th2']['njets']
        n_tag_2 = arrs['th2']['ntag']

        n_jets_4 = arrs['th4_real']['njets']
        n_tag_4 = arrs['th4_real']['ntag']

        weights_it['th2'] = makeWeights(arrs, 'th2', 1, self.year, False, doCRw=self.doCRw, doVRw=self.doVRw, useUpdatedLumi=self.useUpdatedLumi)*nJetWeight(arrs['th2'], f)
        weights_it['th4_real'] = makeWeights(arrs, 'th4_real', 1, self.year, False, doCRw=self.doCRw, doVRw=self.doVRw, useUpdatedLumi=self.useUpdatedLumi)

        return abs(np.sum(weights_it['th2']) - np.sum(weights_it['th4_real']))

    # Pseudo-tag rate function to minimize (for all 2 tag). For a given f, sets up background and 4 tag data nJet histograms.
    # Returns chi2 between them.
    def to_min(self, f):
        arrs = self.arrs
        sf = self.sf_vals
        year =int(self.year)
        nj_hists = {}
        hist_errs = {}
        weights_it = {}
   
        #Key loop is over all data/ttbar regions
        for key in self.keys:
            n_jets = arrs[key]['njets']
            n_tag = arrs[key]['ntag']

            #Make weights with nJet weight for 2 tag, given f, without nJet for 4 tag
            if key.find('2') != -1:
                weights_it[key] = makeWeights(arrs, key, 1, year, False, doCRw=self.doCRw, doVRw=self.doVRw, useUpdatedLumi=self.useUpdatedLumi)*nJetWeight(arrs[key], f)
            else:
                weights_it[key] = makeWeights(arrs, key, 1, year, False, doCRw=self.doCRw, doVRw=self.doVRw, useUpdatedLumi=self.useUpdatedLumi)

            #Set up njet histograms in array (bins are all the same)
            nj_hists[key], bins = np.histogram(arrs[key]['njets']-4, bins=10, range=[-0.5,9.5], weights=weights_it[key])

            #Make errors - sqrt(sum of squares of weights) in each bin, needed for chi2 calc
            w_arr = []
            for i in range(len(bins)-1):
                 w_arr.append(np.sqrt(np.sum(np.array(weights_it[key][((arrs[key]['njets']-4)>=bins[i]) &
                                                    ((arrs[key]['njets']-4)< bins[i+1])])**2)))
            hist_errs[key] = np.array(w_arr)

        #Construct qcd and background histograms (qcd = dat2 - th2 - tnh2, with appropriate weights, back = qcd+th4+tnh4)
        nj_hists['qcd'] = nj_hists['dat2'] - nj_hists['th2'] - nj_hists['tnh2']*sf['tnh2']
        nj_hists['back'] = nj_hists['qcd']*sf['qcd'] + nj_hists['th4']*sf['th'] + nj_hists['tnh4']*sf['tnh']

        #Get final errors for histograms with standard error propagation
        hist_errs['qcd'] = np.sqrt(hist_errs['dat2']**2 + (hist_errs['th2'])**2 + (hist_errs['tnh2']*sf['tnh2'])**2)
        hist_errs['back'] = np.sqrt((hist_errs['qcd']*sf['qcd'])**2 + (hist_errs['th4']*sf['th'])**2 + (hist_errs['tnh4']*sf['tnh'])**2)

        #Remove zeros so that chi2 doesn't toss errors
        nj_hists['back'], hist_errs['back'] = makePositive(nj_hists['back'], hist_errs['back'])
        nj_hists['dat4'], hist_errs['dat4'] = makePositive(nj_hists['dat4'], hist_errs['dat4'])

        #Calculate and return chi2
        chi2 = weighted_chisquare(nj_hists['back'], nj_hists['dat4'], hist_errs['back'], hist_errs['dat4'])[0]
        return chi2

def corr_mass(h1, h2):
    if h1.M():
        alpha1 = 125.0/h1.M()
    else:
        alpha1 = 1.0
    h1cor = h1 * alpha1

    if h2.M():
        alpha2 = 125.0/h2.M()
    else:
        alpha2 = 1.0
    h2cor = h2 * alpha2

    return (h1cor+h2cor).M()

