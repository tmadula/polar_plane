# Pythonic reweighting for resolved HH->4b

Reweighting package using Python 3 for the resolved HH->4b analysis. A root\_numpy version of the reweighting is kept
for coherency with previous versions of this framework. However, active development will be focused on a version rewritten
with [uproot](https://github.com/scikit-hep/uproot), which is more highly supported and offers improved cleanliness and
flexibility over root\_numpy.

This new version of the reweighting relies as well on [tqdm](https://github.com/tqdm/tqdm) for an improved user interface
(progress bars!)

Both packages are available via pip:
``` sh
pip install uproot  #add --user if you're on a read-only file system like lxplus
pip install tqdm    #same deal with the --user thing
```

Uproot is also available in `LCG97python3`, if that's your thing. However, the version available there is out of 
date (and has some bugs). Additionally, rootpy has been phased out for this LCG release, which we use here for histogramming 
to handle error propagation automatically. A virtual environment built on top of `LCG97python3` with all of the packages needed
(up to date uproot, rootpy, tqdm) can be entered via:
``` sh
source /eos/user/s/sgasioro/public/bbbb_env/bin/activate
```
(and exited with `deactivate`). This is read-only, so needs to be cloned to add additional packages (or reach out to Sean).

The background estimate is based around two Python scripts in the new uproot pardigm. 
The first is `NN_RW.py`, which trains the neural network reweighting and predicts weights 
for each event in the input data file. This script outputs a file with the predicted weights, event number, and run number for each event and 
tree in the input data file. 

The second script, `combine_weights.py`, combines this with the rest of the event information and takes the median over bootstrapped weights.
  
NN model and loss function are defined in `rw_model.py`.  A variety of other useful scripts are included for plotting, etc, as well. Option parsing
for (almost) all of the included scripts is centralized in `rw_common.py`, and assorted useful functions are in `rw_utils.py`.


## Important changes from previous version
1. Due to the large dataset size, loading whole trees into memory is prohibitive (and made taking the median very memory-terrible).
   To try to alleviate this (and part of the motivation for uproot), wherever possible, data is loaded in in clusters 
   (memory units that roughly correspond to baskets in ROOT). This is much less memory-intensive than previously, making it more feasible
   to run everything locally, though batching is still recommended for parallelization.
2. Naming has changed!! Code has been rewritten to conform to the region naming present in the nano n-tuples since the MAR20 production
   (control/validation/signal, or CR/VR/SR). This is reflected in the labeling as well - the alternate background model has label `VRderiv`
   (as opposed to the previous `CRderiv` label). For running with the previous SB/CR/SR naming scheme, a `--use-old-names` flag has been
   added to the relevant scripts.
3. Reweighting is now flexible for split input data files (e.g. for 2018 in the MAR20p2 production). This is handled by just passing the list 
   to `-d`, e.g. `-d dataXXa.root dataXXb.root dataXXc.root`. Important: the entire year should be used to train the reweighting. Output weights are
   given for each input file, and combination should thus be handled individually.
4. As mentioned, option parsing has been centralized in `rw_common.py`. This helps to prevent duplicate intro code. Scripts have also been
   encapsulated into a main() function.
5. Two reweighting configurations have been "baked in" to the reweighting scripts: the previous baseline and the dPhi\_HC\_505050 configuration
   (baseline variables + dPhi\_jj in each Higgs candidate + dR\_hh + log(X\_wt)). These are accessed via the `--config` flag.
6. Logging variables can be done via `--doLog` at the option parsing, but is not necessary if the reweighting column has `_log` in the name,
   as this will get picked up automatically.

## Example of reweighting

A sample reweighting sequence is nominally: 
``` sh
# Train nominal NN bootstraps (CR), save in weight_dir/ 
./NN_RW.py -d dataXX.root -y 20XX --bootstrap 100 -o weight_dir/ --config dPhi_HC_505050

# Train alternate NN bootstraps (VR), save in weight_dir/ 
./NN_RW.py -d dataXX.root -y 20XX --bootstrap 100 -o weight_dir/ --reweight-tree validation --config dPhi_HC_505050

# Combine/take median
./combine_weights.py -d dataXX.root -w weight_dir/ -y 20XX --bootstrap 100 -o output_dir/ --comb-label NN_100_bootstraps 
```

Note: `combine_weights.py` only saves the median and IQR over bootstraps by default. If you want to save everything, use the `--save-all-bootstraps` flag.
Additionally, it saves both the nominal and VR derived weights by default. If you only want one of them, use `--nom-only` (nominal) or `--alt-only` (VR derived).

In practice, due to large dataset size, this could take a very long time. The script `condor/run_nominal.py` creates config/shell scripts and submits
to the Condor batch system, e.g., as (TODO: Fix for 2018 and combination)

``` sh
# Create configs/executables and submit for a given year. dataXX is a template format.
# --doNom and --doCR runs the bootstrap training for both the nominal and VR derived weights
./run_nominal.py -d dataXX.root --bootstrap 100 --n_jobs 10 -y 20XX -o weight_dir/ --doNom --doVR --config dPhi_HC_505050 --submit

#After all of the training jobs are done, a combination job can be submitted
./run_nominal.py -d dataXX.root --bootstrap 100 --n_jobs 10 -y 20XX -w weight_dir/ --combine-only --submit
```

