#!/usr/bin/env python3
import numpy as np
import os

from rw_common import *

import argparse

import ROOT
#Prevents hijacking of help text
ROOT.PyConfig.IgnoreCommandLineOptions = True

from root_numpy import root2array, array2tree
from ROOT import TFile, TParameter, TTree, TObject

def main(inputs):
    #Unpack inputs
    yr_short = inputs["yr_short"]
    data_file = inputs["data_file"]
    reg_label = inputs["reg_label"]
    weight_dir = inputs["weight_dir"]
    n_resamples = inputs["n_resamples"]

    median = True

    NN_norms={}
    columns = []
    f=TFile(data_file, "read")
    for bstrap in range(n_resamples):
        columns+=[('NN_d24_weight'+reg_label+'_resampling_%d_'+yr_short)%bstrap]
        NN_norms[bstrap]=f.Get(('NN_norm'+reg_label+'_resampling_%d_'+yr_short)%bstrap).GetVal()
    f.Close()

    f = TFile(data_file,"update")
    treeNames = []
    objNames = []
    for key in f.GetListOfKeys():
        if key.GetName():
            if key.GetClassName() == "TTree":
                treeNames.append(key.GetName())

    for treeCount in range(len(treeNames)):
        treeName=treeNames[treeCount]
        print("Running for tree", treeName)

        T = f.Get(treeName)

        full_data = root2array(data_file,
                               treename=treeName, branches=columns)

        if not median:
            cent_label = 'ave'
            print("About to take average")
        else:
            cent_label = 'med'
            print("About to take median")

        resamp_weight_set = [] 
        for bstrap in range(n_resamples):
            resamp_weight_set.append(full_data[('NN_d24_weight'+reg_label+'_resampling_%d_'+yr_short)%bstrap]*NN_norms[bstrap])

        if median:
            nom_cent_weights = np.median(resamp_weight_set, overwrite_input = True, axis=0)
        else:
            nom_cent_weights = np.average(resamp_weight_set, axis=0)

        print(nom_cent_weights.shape)

        out_ave = np.array(nom_cent_weights,
                           dtype=[('NN_d24_weight'+reg_label+'_bstrap_'+cent_label+'_'+yr_short, np.float64)])

        array2tree(out_ave, tree=T)
        T.Print()
        f.cd()
        T.Write(treeName, TObject.kOverwrite)

    del f

if __name__ == '__main__':
    driver = common("combine_weights.py")

    parser = driver.makeParser()
    args = parser.parse_args()
    inputs = driver.interpretArgs(args)

    main(inputs)
